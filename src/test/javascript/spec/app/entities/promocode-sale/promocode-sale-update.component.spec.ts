import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { PromocodeSaleUpdateComponent } from 'app/entities/promocode-sale/promocode-sale-update.component';
import { PromocodeSaleService } from 'app/entities/promocode-sale/promocode-sale.service';
import { PromocodeSale } from 'app/shared/model/promocode-sale.model';

describe('Component Tests', () => {
  describe('PromocodeSale Management Update Component', () => {
    let comp: PromocodeSaleUpdateComponent;
    let fixture: ComponentFixture<PromocodeSaleUpdateComponent>;
    let service: PromocodeSaleService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [PromocodeSaleUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(PromocodeSaleUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(PromocodeSaleUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PromocodeSaleService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new PromocodeSale(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new PromocodeSale();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

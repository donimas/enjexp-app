import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EnjexpTestModule } from '../../../test.module';
import { PromocodeSaleDeleteDialogComponent } from 'app/entities/promocode-sale/promocode-sale-delete-dialog.component';
import { PromocodeSaleService } from 'app/entities/promocode-sale/promocode-sale.service';

describe('Component Tests', () => {
  describe('PromocodeSale Management Delete Component', () => {
    let comp: PromocodeSaleDeleteDialogComponent;
    let fixture: ComponentFixture<PromocodeSaleDeleteDialogComponent>;
    let service: PromocodeSaleService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [PromocodeSaleDeleteDialogComponent]
      })
        .overrideTemplate(PromocodeSaleDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PromocodeSaleDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PromocodeSaleService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

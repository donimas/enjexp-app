import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { PromocodeSaleDetailComponent } from 'app/entities/promocode-sale/promocode-sale-detail.component';
import { PromocodeSale } from 'app/shared/model/promocode-sale.model';

describe('Component Tests', () => {
  describe('PromocodeSale Management Detail Component', () => {
    let comp: PromocodeSaleDetailComponent;
    let fixture: ComponentFixture<PromocodeSaleDetailComponent>;
    const route = ({ data: of({ promocodeSale: new PromocodeSale(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [PromocodeSaleDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PromocodeSaleDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PromocodeSaleDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.promocodeSale).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

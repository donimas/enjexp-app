import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EnjexpTestModule } from '../../../test.module';
import { PromocodeDeleteDialogComponent } from 'app/entities/promocode/promocode-delete-dialog.component';
import { PromocodeService } from 'app/entities/promocode/promocode.service';

describe('Component Tests', () => {
  describe('Promocode Management Delete Component', () => {
    let comp: PromocodeDeleteDialogComponent;
    let fixture: ComponentFixture<PromocodeDeleteDialogComponent>;
    let service: PromocodeService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [PromocodeDeleteDialogComponent]
      })
        .overrideTemplate(PromocodeDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PromocodeDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PromocodeService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

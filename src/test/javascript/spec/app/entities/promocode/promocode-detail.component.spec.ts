import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { PromocodeDetailComponent } from 'app/entities/promocode/promocode-detail.component';
import { Promocode } from 'app/shared/model/promocode.model';

describe('Component Tests', () => {
  describe('Promocode Management Detail Component', () => {
    let comp: PromocodeDetailComponent;
    let fixture: ComponentFixture<PromocodeDetailComponent>;
    const route = ({ data: of({ promocode: new Promocode(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [PromocodeDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(PromocodeDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PromocodeDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.promocode).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

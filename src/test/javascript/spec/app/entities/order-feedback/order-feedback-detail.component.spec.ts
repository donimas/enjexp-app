import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { OrderFeedbackDetailComponent } from 'app/entities/order-feedback/order-feedback-detail.component';
import { OrderFeedback } from 'app/shared/model/order-feedback.model';

describe('Component Tests', () => {
  describe('OrderFeedback Management Detail Component', () => {
    let comp: OrderFeedbackDetailComponent;
    let fixture: ComponentFixture<OrderFeedbackDetailComponent>;
    const route = ({ data: of({ orderFeedback: new OrderFeedback(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [OrderFeedbackDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(OrderFeedbackDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(OrderFeedbackDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.orderFeedback).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

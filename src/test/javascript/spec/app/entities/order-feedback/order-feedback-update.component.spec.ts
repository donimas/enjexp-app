import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { OrderFeedbackUpdateComponent } from 'app/entities/order-feedback/order-feedback-update.component';
import { OrderFeedbackService } from 'app/entities/order-feedback/order-feedback.service';
import { OrderFeedback } from 'app/shared/model/order-feedback.model';

describe('Component Tests', () => {
  describe('OrderFeedback Management Update Component', () => {
    let comp: OrderFeedbackUpdateComponent;
    let fixture: ComponentFixture<OrderFeedbackUpdateComponent>;
    let service: OrderFeedbackService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [OrderFeedbackUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(OrderFeedbackUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(OrderFeedbackUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(OrderFeedbackService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrderFeedback(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new OrderFeedback();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

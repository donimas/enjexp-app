import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { NoticeReceiverService } from 'app/entities/notice-receiver/notice-receiver.service';
import { INoticeReceiver, NoticeReceiver } from 'app/shared/model/notice-receiver.model';

describe('Service Tests', () => {
  describe('NoticeReceiver Service', () => {
    let injector: TestBed;
    let service: NoticeReceiverService;
    let httpMock: HttpTestingController;
    let elemDefault: INoticeReceiver;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(NoticeReceiverService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new NoticeReceiver(0, 0, 'AAAAAAA', false, false, false, false, currentDate, 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            readDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a NoticeReceiver', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            readDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            readDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new NoticeReceiver(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a NoticeReceiver', () => {
        const returnedFromService = Object.assign(
          {
            toUserId: 1,
            userLang: 'BBBBBB',
            flagSmsSent: true,
            flagEmailSent: true,
            flagPushSent: true,
            flagRead: true,
            readDate: currentDate.format(DATE_TIME_FORMAT),
            receiverFullName: 'BBBBBB',
            receiverPhone: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            readDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of NoticeReceiver', () => {
        const returnedFromService = Object.assign(
          {
            toUserId: 1,
            userLang: 'BBBBBB',
            flagSmsSent: true,
            flagEmailSent: true,
            flagPushSent: true,
            flagRead: true,
            readDate: currentDate.format(DATE_TIME_FORMAT),
            receiverFullName: 'BBBBBB',
            receiverPhone: 'BBBBBB'
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            readDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a NoticeReceiver', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { NoticeReceiverUpdateComponent } from 'app/entities/notice-receiver/notice-receiver-update.component';
import { NoticeReceiverService } from 'app/entities/notice-receiver/notice-receiver.service';
import { NoticeReceiver } from 'app/shared/model/notice-receiver.model';

describe('Component Tests', () => {
  describe('NoticeReceiver Management Update Component', () => {
    let comp: NoticeReceiverUpdateComponent;
    let fixture: ComponentFixture<NoticeReceiverUpdateComponent>;
    let service: NoticeReceiverService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [NoticeReceiverUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(NoticeReceiverUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(NoticeReceiverUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(NoticeReceiverService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new NoticeReceiver(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new NoticeReceiver();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

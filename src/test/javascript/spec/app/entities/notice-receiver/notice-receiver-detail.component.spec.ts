import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { NoticeReceiverDetailComponent } from 'app/entities/notice-receiver/notice-receiver-detail.component';
import { NoticeReceiver } from 'app/shared/model/notice-receiver.model';

describe('Component Tests', () => {
  describe('NoticeReceiver Management Detail Component', () => {
    let comp: NoticeReceiverDetailComponent;
    let fixture: ComponentFixture<NoticeReceiverDetailComponent>;
    const route = ({ data: of({ noticeReceiver: new NoticeReceiver(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [NoticeReceiverDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(NoticeReceiverDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(NoticeReceiverDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.noticeReceiver).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

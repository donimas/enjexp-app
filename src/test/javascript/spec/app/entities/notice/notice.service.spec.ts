import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { NoticeService } from 'app/entities/notice/notice.service';
import { INotice, Notice } from 'app/shared/model/notice.model';
import { NoticeReceiverType } from 'app/shared/model/enumerations/notice-receiver-type.model';
import { NoticeType } from 'app/shared/model/enumerations/notice-type.model';

describe('Service Tests', () => {
  describe('Notice Service', () => {
    let injector: TestBed;
    let service: NoticeService;
    let httpMock: HttpTestingController;
    let elemDefault: INotice;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(NoticeService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Notice(
        0,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        false,
        false,
        NoticeReceiverType.ONE_USER,
        currentDate,
        currentDate,
        'AAAAAAA',
        0,
        NoticeType.INFO,
        false,
        currentDate,
        'AAAAAAA',
        0,
        false
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createDate: currentDate.format(DATE_TIME_FORMAT),
            modifyDate: currentDate.format(DATE_TIME_FORMAT),
            sentTime: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Notice', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createDate: currentDate.format(DATE_TIME_FORMAT),
            modifyDate: currentDate.format(DATE_TIME_FORMAT),
            sentTime: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createDate: currentDate,
            modifyDate: currentDate,
            sentTime: currentDate
          },
          returnedFromService
        );
        service
          .create(new Notice(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Notice', () => {
        const returnedFromService = Object.assign(
          {
            title: 'BBBBBB',
            content: 'BBBBBB',
            smsContent: 'BBBBBB',
            flagPush: true,
            flagSms: true,
            flagEmail: true,
            receiverType: 'BBBBBB',
            createDate: currentDate.format(DATE_TIME_FORMAT),
            modifyDate: currentDate.format(DATE_TIME_FORMAT),
            containerClass: 'BBBBBB',
            containerId: 1,
            noticeType: 'BBBBBB',
            flagSent: true,
            sentTime: currentDate.format(DATE_TIME_FORMAT),
            payloadObjectUrl: 'BBBBBB',
            payloadObjectId: 1,
            flagDeleted: true
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createDate: currentDate,
            modifyDate: currentDate,
            sentTime: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Notice', () => {
        const returnedFromService = Object.assign(
          {
            title: 'BBBBBB',
            content: 'BBBBBB',
            smsContent: 'BBBBBB',
            flagPush: true,
            flagSms: true,
            flagEmail: true,
            receiverType: 'BBBBBB',
            createDate: currentDate.format(DATE_TIME_FORMAT),
            modifyDate: currentDate.format(DATE_TIME_FORMAT),
            containerClass: 'BBBBBB',
            containerId: 1,
            noticeType: 'BBBBBB',
            flagSent: true,
            sentTime: currentDate.format(DATE_TIME_FORMAT),
            payloadObjectUrl: 'BBBBBB',
            payloadObjectId: 1,
            flagDeleted: true
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createDate: currentDate,
            modifyDate: currentDate,
            sentTime: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Notice', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

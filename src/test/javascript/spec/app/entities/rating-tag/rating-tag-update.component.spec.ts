import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { RatingTagUpdateComponent } from 'app/entities/rating-tag/rating-tag-update.component';
import { RatingTagService } from 'app/entities/rating-tag/rating-tag.service';
import { RatingTag } from 'app/shared/model/rating-tag.model';

describe('Component Tests', () => {
  describe('RatingTag Management Update Component', () => {
    let comp: RatingTagUpdateComponent;
    let fixture: ComponentFixture<RatingTagUpdateComponent>;
    let service: RatingTagService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [RatingTagUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(RatingTagUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RatingTagUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RatingTagService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new RatingTag(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new RatingTag();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});

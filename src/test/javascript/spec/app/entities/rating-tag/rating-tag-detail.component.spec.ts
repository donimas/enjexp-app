import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EnjexpTestModule } from '../../../test.module';
import { RatingTagDetailComponent } from 'app/entities/rating-tag/rating-tag-detail.component';
import { RatingTag } from 'app/shared/model/rating-tag.model';

describe('Component Tests', () => {
  describe('RatingTag Management Detail Component', () => {
    let comp: RatingTagDetailComponent;
    let fixture: ComponentFixture<RatingTagDetailComponent>;
    const route = ({ data: of({ ratingTag: new RatingTag(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [RatingTagDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(RatingTagDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RatingTagDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.ratingTag).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});

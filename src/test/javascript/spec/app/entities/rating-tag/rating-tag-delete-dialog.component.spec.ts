import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EnjexpTestModule } from '../../../test.module';
import { RatingTagDeleteDialogComponent } from 'app/entities/rating-tag/rating-tag-delete-dialog.component';
import { RatingTagService } from 'app/entities/rating-tag/rating-tag.service';

describe('Component Tests', () => {
  describe('RatingTag Management Delete Component', () => {
    let comp: RatingTagDeleteDialogComponent;
    let fixture: ComponentFixture<RatingTagDeleteDialogComponent>;
    let service: RatingTagService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [RatingTagDeleteDialogComponent]
      })
        .overrideTemplate(RatingTagDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(RatingTagDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RatingTagService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

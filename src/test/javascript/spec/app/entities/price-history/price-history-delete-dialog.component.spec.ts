import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { EnjexpTestModule } from '../../../test.module';
import { PriceHistoryDeleteDialogComponent } from 'app/entities/price-history/price-history-delete-dialog.component';
import { PriceHistoryService } from 'app/entities/price-history/price-history.service';

describe('Component Tests', () => {
  describe('PriceHistory Management Delete Component', () => {
    let comp: PriceHistoryDeleteDialogComponent;
    let fixture: ComponentFixture<PriceHistoryDeleteDialogComponent>;
    let service: PriceHistoryService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [EnjexpTestModule],
        declarations: [PriceHistoryDeleteDialogComponent]
      })
        .overrideTemplate(PriceHistoryDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(PriceHistoryDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(PriceHistoryService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});

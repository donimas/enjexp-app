import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { OrderService } from 'app/entities/order/order.service';
import { IOrder, Order } from 'app/shared/model/order.model';
import { PaymentType } from 'app/shared/model/enumerations/payment-type.model';
import { ServiceType } from 'app/shared/model/enumerations/service-type.model';
import { LocationType } from 'app/shared/model/enumerations/location-type.model';

describe('Service Tests', () => {
  describe('Order Service', () => {
    let injector: TestBed;
    let service: OrderService;
    let httpMock: HttpTestingController;
    let elemDefault: IOrder;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(OrderService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Order(
        0,
        'AAAAAAA',
        0,
        0,
        0,
        PaymentType.CASH,
        'AAAAAAA',
        false,
        ServiceType.PICK_UP,
        currentDate,
        currentDate,
        'AAAAAAA',
        LocationType.KAZGYU,
        0
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            serviceTime: currentDate.format(DATE_TIME_FORMAT),
            createDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Order', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            serviceTime: currentDate.format(DATE_TIME_FORMAT),
            createDate: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            serviceTime: currentDate,
            createDate: currentDate
          },
          returnedFromService
        );
        service
          .create(new Order(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Order', () => {
        const returnedFromService = Object.assign(
          {
            promocode: 'BBBBBB',
            totalPrice: 1,
            change: 1,
            paid: 1,
            paymentType: 'BBBBBB',
            paymentCode: 'BBBBBB',
            flagPaid: true,
            serviceType: 'BBBBBB',
            serviceTime: currentDate.format(DATE_TIME_FORMAT),
            createDate: currentDate.format(DATE_TIME_FORMAT),
            comment: 'BBBBBB',
            locationType: 'BBBBBB',
            deliveryDuration: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            serviceTime: currentDate,
            createDate: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Order', () => {
        const returnedFromService = Object.assign(
          {
            promocode: 'BBBBBB',
            totalPrice: 1,
            change: 1,
            paid: 1,
            paymentType: 'BBBBBB',
            paymentCode: 'BBBBBB',
            flagPaid: true,
            serviceType: 'BBBBBB',
            serviceTime: currentDate.format(DATE_TIME_FORMAT),
            createDate: currentDate.format(DATE_TIME_FORMAT),
            comment: 'BBBBBB',
            locationType: 'BBBBBB',
            deliveryDuration: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            serviceTime: currentDate,
            createDate: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Order', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});

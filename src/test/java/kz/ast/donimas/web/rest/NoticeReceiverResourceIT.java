package kz.ast.donimas.web.rest;

import kz.ast.donimas.EnjexpApp;
import kz.ast.donimas.domain.NoticeReceiver;
import kz.ast.donimas.repository.NoticeReceiverRepository;
import kz.ast.donimas.service.NoticeReceiverService;
import kz.ast.donimas.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link NoticeReceiverResource} REST controller.
 */
@SpringBootTest(classes = EnjexpApp.class)
public class NoticeReceiverResourceIT {

    private static final Long DEFAULT_TO_USER_ID = 1L;
    private static final Long UPDATED_TO_USER_ID = 2L;
    private static final Long SMALLER_TO_USER_ID = 1L - 1L;

    private static final String DEFAULT_USER_LANG = "AAAAAAAAAA";
    private static final String UPDATED_USER_LANG = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_SMS_SENT = false;
    private static final Boolean UPDATED_FLAG_SMS_SENT = true;

    private static final Boolean DEFAULT_FLAG_EMAIL_SENT = false;
    private static final Boolean UPDATED_FLAG_EMAIL_SENT = true;

    private static final Boolean DEFAULT_FLAG_PUSH_SENT = false;
    private static final Boolean UPDATED_FLAG_PUSH_SENT = true;

    private static final Boolean DEFAULT_FLAG_READ = false;
    private static final Boolean UPDATED_FLAG_READ = true;

    private static final ZonedDateTime DEFAULT_READ_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_READ_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_READ_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_RECEIVER_FULL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVER_FULL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_RECEIVER_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_RECEIVER_PHONE = "BBBBBBBBBB";

    @Autowired
    private NoticeReceiverRepository noticeReceiverRepository;

    @Autowired
    private NoticeReceiverService noticeReceiverService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNoticeReceiverMockMvc;

    private NoticeReceiver noticeReceiver;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NoticeReceiverResource noticeReceiverResource = new NoticeReceiverResource(noticeReceiverService);
        this.restNoticeReceiverMockMvc = MockMvcBuilders.standaloneSetup(noticeReceiverResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NoticeReceiver createEntity(EntityManager em) {
        NoticeReceiver noticeReceiver = new NoticeReceiver()
            .toUserId(DEFAULT_TO_USER_ID)
            .userLang(DEFAULT_USER_LANG)
            .flagSmsSent(DEFAULT_FLAG_SMS_SENT)
            .flagEmailSent(DEFAULT_FLAG_EMAIL_SENT)
            .flagPushSent(DEFAULT_FLAG_PUSH_SENT)
            .flagRead(DEFAULT_FLAG_READ)
            .readDate(DEFAULT_READ_DATE)
            .receiverFullName(DEFAULT_RECEIVER_FULL_NAME)
            .receiverPhone(DEFAULT_RECEIVER_PHONE);
        return noticeReceiver;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static NoticeReceiver createUpdatedEntity(EntityManager em) {
        NoticeReceiver noticeReceiver = new NoticeReceiver()
            .toUserId(UPDATED_TO_USER_ID)
            .userLang(UPDATED_USER_LANG)
            .flagSmsSent(UPDATED_FLAG_SMS_SENT)
            .flagEmailSent(UPDATED_FLAG_EMAIL_SENT)
            .flagPushSent(UPDATED_FLAG_PUSH_SENT)
            .flagRead(UPDATED_FLAG_READ)
            .readDate(UPDATED_READ_DATE)
            .receiverFullName(UPDATED_RECEIVER_FULL_NAME)
            .receiverPhone(UPDATED_RECEIVER_PHONE);
        return noticeReceiver;
    }

    @BeforeEach
    public void initTest() {
        noticeReceiver = createEntity(em);
    }

    @Test
    @Transactional
    public void createNoticeReceiver() throws Exception {
        int databaseSizeBeforeCreate = noticeReceiverRepository.findAll().size();

        // Create the NoticeReceiver
        restNoticeReceiverMockMvc.perform(post("/api/notice-receivers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(noticeReceiver)))
            .andExpect(status().isCreated());

        // Validate the NoticeReceiver in the database
        List<NoticeReceiver> noticeReceiverList = noticeReceiverRepository.findAll();
        assertThat(noticeReceiverList).hasSize(databaseSizeBeforeCreate + 1);
        NoticeReceiver testNoticeReceiver = noticeReceiverList.get(noticeReceiverList.size() - 1);
        assertThat(testNoticeReceiver.getToUserId()).isEqualTo(DEFAULT_TO_USER_ID);
        assertThat(testNoticeReceiver.getUserLang()).isEqualTo(DEFAULT_USER_LANG);
        assertThat(testNoticeReceiver.isFlagSmsSent()).isEqualTo(DEFAULT_FLAG_SMS_SENT);
        assertThat(testNoticeReceiver.isFlagEmailSent()).isEqualTo(DEFAULT_FLAG_EMAIL_SENT);
        assertThat(testNoticeReceiver.isFlagPushSent()).isEqualTo(DEFAULT_FLAG_PUSH_SENT);
        assertThat(testNoticeReceiver.isFlagRead()).isEqualTo(DEFAULT_FLAG_READ);
        assertThat(testNoticeReceiver.getReadDate()).isEqualTo(DEFAULT_READ_DATE);
        assertThat(testNoticeReceiver.getReceiverFullName()).isEqualTo(DEFAULT_RECEIVER_FULL_NAME);
        assertThat(testNoticeReceiver.getReceiverPhone()).isEqualTo(DEFAULT_RECEIVER_PHONE);
    }

    @Test
    @Transactional
    public void createNoticeReceiverWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = noticeReceiverRepository.findAll().size();

        // Create the NoticeReceiver with an existing ID
        noticeReceiver.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNoticeReceiverMockMvc.perform(post("/api/notice-receivers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(noticeReceiver)))
            .andExpect(status().isBadRequest());

        // Validate the NoticeReceiver in the database
        List<NoticeReceiver> noticeReceiverList = noticeReceiverRepository.findAll();
        assertThat(noticeReceiverList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllNoticeReceivers() throws Exception {
        // Initialize the database
        noticeReceiverRepository.saveAndFlush(noticeReceiver);

        // Get all the noticeReceiverList
        restNoticeReceiverMockMvc.perform(get("/api/notice-receivers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(noticeReceiver.getId().intValue())))
            .andExpect(jsonPath("$.[*].toUserId").value(hasItem(DEFAULT_TO_USER_ID.intValue())))
            .andExpect(jsonPath("$.[*].userLang").value(hasItem(DEFAULT_USER_LANG.toString())))
            .andExpect(jsonPath("$.[*].flagSmsSent").value(hasItem(DEFAULT_FLAG_SMS_SENT.booleanValue())))
            .andExpect(jsonPath("$.[*].flagEmailSent").value(hasItem(DEFAULT_FLAG_EMAIL_SENT.booleanValue())))
            .andExpect(jsonPath("$.[*].flagPushSent").value(hasItem(DEFAULT_FLAG_PUSH_SENT.booleanValue())))
            .andExpect(jsonPath("$.[*].flagRead").value(hasItem(DEFAULT_FLAG_READ.booleanValue())))
            .andExpect(jsonPath("$.[*].readDate").value(hasItem(sameInstant(DEFAULT_READ_DATE))))
            .andExpect(jsonPath("$.[*].receiverFullName").value(hasItem(DEFAULT_RECEIVER_FULL_NAME.toString())))
            .andExpect(jsonPath("$.[*].receiverPhone").value(hasItem(DEFAULT_RECEIVER_PHONE.toString())));
    }
    
    @Test
    @Transactional
    public void getNoticeReceiver() throws Exception {
        // Initialize the database
        noticeReceiverRepository.saveAndFlush(noticeReceiver);

        // Get the noticeReceiver
        restNoticeReceiverMockMvc.perform(get("/api/notice-receivers/{id}", noticeReceiver.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(noticeReceiver.getId().intValue()))
            .andExpect(jsonPath("$.toUserId").value(DEFAULT_TO_USER_ID.intValue()))
            .andExpect(jsonPath("$.userLang").value(DEFAULT_USER_LANG.toString()))
            .andExpect(jsonPath("$.flagSmsSent").value(DEFAULT_FLAG_SMS_SENT.booleanValue()))
            .andExpect(jsonPath("$.flagEmailSent").value(DEFAULT_FLAG_EMAIL_SENT.booleanValue()))
            .andExpect(jsonPath("$.flagPushSent").value(DEFAULT_FLAG_PUSH_SENT.booleanValue()))
            .andExpect(jsonPath("$.flagRead").value(DEFAULT_FLAG_READ.booleanValue()))
            .andExpect(jsonPath("$.readDate").value(sameInstant(DEFAULT_READ_DATE)))
            .andExpect(jsonPath("$.receiverFullName").value(DEFAULT_RECEIVER_FULL_NAME.toString()))
            .andExpect(jsonPath("$.receiverPhone").value(DEFAULT_RECEIVER_PHONE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingNoticeReceiver() throws Exception {
        // Get the noticeReceiver
        restNoticeReceiverMockMvc.perform(get("/api/notice-receivers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNoticeReceiver() throws Exception {
        // Initialize the database
        noticeReceiverService.save(noticeReceiver);

        int databaseSizeBeforeUpdate = noticeReceiverRepository.findAll().size();

        // Update the noticeReceiver
        NoticeReceiver updatedNoticeReceiver = noticeReceiverRepository.findById(noticeReceiver.getId()).get();
        // Disconnect from session so that the updates on updatedNoticeReceiver are not directly saved in db
        em.detach(updatedNoticeReceiver);
        updatedNoticeReceiver
            .toUserId(UPDATED_TO_USER_ID)
            .userLang(UPDATED_USER_LANG)
            .flagSmsSent(UPDATED_FLAG_SMS_SENT)
            .flagEmailSent(UPDATED_FLAG_EMAIL_SENT)
            .flagPushSent(UPDATED_FLAG_PUSH_SENT)
            .flagRead(UPDATED_FLAG_READ)
            .readDate(UPDATED_READ_DATE)
            .receiverFullName(UPDATED_RECEIVER_FULL_NAME)
            .receiverPhone(UPDATED_RECEIVER_PHONE);

        restNoticeReceiverMockMvc.perform(put("/api/notice-receivers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNoticeReceiver)))
            .andExpect(status().isOk());

        // Validate the NoticeReceiver in the database
        List<NoticeReceiver> noticeReceiverList = noticeReceiverRepository.findAll();
        assertThat(noticeReceiverList).hasSize(databaseSizeBeforeUpdate);
        NoticeReceiver testNoticeReceiver = noticeReceiverList.get(noticeReceiverList.size() - 1);
        assertThat(testNoticeReceiver.getToUserId()).isEqualTo(UPDATED_TO_USER_ID);
        assertThat(testNoticeReceiver.getUserLang()).isEqualTo(UPDATED_USER_LANG);
        assertThat(testNoticeReceiver.isFlagSmsSent()).isEqualTo(UPDATED_FLAG_SMS_SENT);
        assertThat(testNoticeReceiver.isFlagEmailSent()).isEqualTo(UPDATED_FLAG_EMAIL_SENT);
        assertThat(testNoticeReceiver.isFlagPushSent()).isEqualTo(UPDATED_FLAG_PUSH_SENT);
        assertThat(testNoticeReceiver.isFlagRead()).isEqualTo(UPDATED_FLAG_READ);
        assertThat(testNoticeReceiver.getReadDate()).isEqualTo(UPDATED_READ_DATE);
        assertThat(testNoticeReceiver.getReceiverFullName()).isEqualTo(UPDATED_RECEIVER_FULL_NAME);
        assertThat(testNoticeReceiver.getReceiverPhone()).isEqualTo(UPDATED_RECEIVER_PHONE);
    }

    @Test
    @Transactional
    public void updateNonExistingNoticeReceiver() throws Exception {
        int databaseSizeBeforeUpdate = noticeReceiverRepository.findAll().size();

        // Create the NoticeReceiver

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNoticeReceiverMockMvc.perform(put("/api/notice-receivers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(noticeReceiver)))
            .andExpect(status().isBadRequest());

        // Validate the NoticeReceiver in the database
        List<NoticeReceiver> noticeReceiverList = noticeReceiverRepository.findAll();
        assertThat(noticeReceiverList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNoticeReceiver() throws Exception {
        // Initialize the database
        noticeReceiverService.save(noticeReceiver);

        int databaseSizeBeforeDelete = noticeReceiverRepository.findAll().size();

        // Delete the noticeReceiver
        restNoticeReceiverMockMvc.perform(delete("/api/notice-receivers/{id}", noticeReceiver.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<NoticeReceiver> noticeReceiverList = noticeReceiverRepository.findAll();
        assertThat(noticeReceiverList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(NoticeReceiver.class);
        NoticeReceiver noticeReceiver1 = new NoticeReceiver();
        noticeReceiver1.setId(1L);
        NoticeReceiver noticeReceiver2 = new NoticeReceiver();
        noticeReceiver2.setId(noticeReceiver1.getId());
        assertThat(noticeReceiver1).isEqualTo(noticeReceiver2);
        noticeReceiver2.setId(2L);
        assertThat(noticeReceiver1).isNotEqualTo(noticeReceiver2);
        noticeReceiver1.setId(null);
        assertThat(noticeReceiver1).isNotEqualTo(noticeReceiver2);
    }
}

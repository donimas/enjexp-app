package kz.ast.donimas.web.rest;

import kz.ast.donimas.EnjexpApp;
import kz.ast.donimas.domain.OrderFeedback;
import kz.ast.donimas.repository.OrderFeedbackRepository;
import kz.ast.donimas.service.OrderFeedbackService;
import kz.ast.donimas.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static kz.ast.donimas.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OrderFeedbackResource} REST controller.
 */
@SpringBootTest(classes = EnjexpApp.class)
public class OrderFeedbackResourceIT {

    private static final BigDecimal DEFAULT_RATING = new BigDecimal(1);
    private static final BigDecimal UPDATED_RATING = new BigDecimal(2);
    private static final BigDecimal SMALLER_RATING = new BigDecimal(1 - 1);

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private OrderFeedbackRepository orderFeedbackRepository;

    @Mock
    private OrderFeedbackRepository orderFeedbackRepositoryMock;

    @Mock
    private OrderFeedbackService orderFeedbackServiceMock;

    @Autowired
    private OrderFeedbackService orderFeedbackService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrderFeedbackMockMvc;

    private OrderFeedback orderFeedback;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrderFeedbackResource orderFeedbackResource = new OrderFeedbackResource(orderFeedbackService);
        this.restOrderFeedbackMockMvc = MockMvcBuilders.standaloneSetup(orderFeedbackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderFeedback createEntity(EntityManager em) {
        OrderFeedback orderFeedback = new OrderFeedback()
            .rating(DEFAULT_RATING)
            .comment(DEFAULT_COMMENT)
            .createDate(DEFAULT_CREATE_DATE);
        return orderFeedback;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OrderFeedback createUpdatedEntity(EntityManager em) {
        OrderFeedback orderFeedback = new OrderFeedback()
            .rating(UPDATED_RATING)
            .comment(UPDATED_COMMENT)
            .createDate(UPDATED_CREATE_DATE);
        return orderFeedback;
    }

    @BeforeEach
    public void initTest() {
        orderFeedback = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrderFeedback() throws Exception {
        int databaseSizeBeforeCreate = orderFeedbackRepository.findAll().size();

        // Create the OrderFeedback
        restOrderFeedbackMockMvc.perform(post("/api/order-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderFeedback)))
            .andExpect(status().isCreated());

        // Validate the OrderFeedback in the database
        List<OrderFeedback> orderFeedbackList = orderFeedbackRepository.findAll();
        assertThat(orderFeedbackList).hasSize(databaseSizeBeforeCreate + 1);
        OrderFeedback testOrderFeedback = orderFeedbackList.get(orderFeedbackList.size() - 1);
        assertThat(testOrderFeedback.getRating()).isEqualTo(DEFAULT_RATING);
        assertThat(testOrderFeedback.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testOrderFeedback.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
    }

    @Test
    @Transactional
    public void createOrderFeedbackWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderFeedbackRepository.findAll().size();

        // Create the OrderFeedback with an existing ID
        orderFeedback.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderFeedbackMockMvc.perform(post("/api/order-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderFeedback)))
            .andExpect(status().isBadRequest());

        // Validate the OrderFeedback in the database
        List<OrderFeedback> orderFeedbackList = orderFeedbackRepository.findAll();
        assertThat(orderFeedbackList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrderFeedbacks() throws Exception {
        // Initialize the database
        orderFeedbackRepository.saveAndFlush(orderFeedback);

        // Get all the orderFeedbackList
        restOrderFeedbackMockMvc.perform(get("/api/order-feedbacks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(orderFeedback.getId().intValue())))
            .andExpect(jsonPath("$.[*].rating").value(hasItem(DEFAULT_RATING.intValue())))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllOrderFeedbacksWithEagerRelationshipsIsEnabled() throws Exception {
        OrderFeedbackResource orderFeedbackResource = new OrderFeedbackResource(orderFeedbackServiceMock);
        when(orderFeedbackServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restOrderFeedbackMockMvc = MockMvcBuilders.standaloneSetup(orderFeedbackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restOrderFeedbackMockMvc.perform(get("/api/order-feedbacks?eagerload=true"))
        .andExpect(status().isOk());

        verify(orderFeedbackServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllOrderFeedbacksWithEagerRelationshipsIsNotEnabled() throws Exception {
        OrderFeedbackResource orderFeedbackResource = new OrderFeedbackResource(orderFeedbackServiceMock);
            when(orderFeedbackServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restOrderFeedbackMockMvc = MockMvcBuilders.standaloneSetup(orderFeedbackResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restOrderFeedbackMockMvc.perform(get("/api/order-feedbacks?eagerload=true"))
        .andExpect(status().isOk());

            verify(orderFeedbackServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getOrderFeedback() throws Exception {
        // Initialize the database
        orderFeedbackRepository.saveAndFlush(orderFeedback);

        // Get the orderFeedback
        restOrderFeedbackMockMvc.perform(get("/api/order-feedbacks/{id}", orderFeedback.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(orderFeedback.getId().intValue()))
            .andExpect(jsonPath("$.rating").value(DEFAULT_RATING.intValue()))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingOrderFeedback() throws Exception {
        // Get the orderFeedback
        restOrderFeedbackMockMvc.perform(get("/api/order-feedbacks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderFeedback() throws Exception {
        // Initialize the database
        orderFeedbackService.save(orderFeedback);

        int databaseSizeBeforeUpdate = orderFeedbackRepository.findAll().size();

        // Update the orderFeedback
        OrderFeedback updatedOrderFeedback = orderFeedbackRepository.findById(orderFeedback.getId()).get();
        // Disconnect from session so that the updates on updatedOrderFeedback are not directly saved in db
        em.detach(updatedOrderFeedback);
        updatedOrderFeedback
            .rating(UPDATED_RATING)
            .comment(UPDATED_COMMENT)
            .createDate(UPDATED_CREATE_DATE);

        restOrderFeedbackMockMvc.perform(put("/api/order-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrderFeedback)))
            .andExpect(status().isOk());

        // Validate the OrderFeedback in the database
        List<OrderFeedback> orderFeedbackList = orderFeedbackRepository.findAll();
        assertThat(orderFeedbackList).hasSize(databaseSizeBeforeUpdate);
        OrderFeedback testOrderFeedback = orderFeedbackList.get(orderFeedbackList.size() - 1);
        assertThat(testOrderFeedback.getRating()).isEqualTo(UPDATED_RATING);
        assertThat(testOrderFeedback.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testOrderFeedback.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingOrderFeedback() throws Exception {
        int databaseSizeBeforeUpdate = orderFeedbackRepository.findAll().size();

        // Create the OrderFeedback

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderFeedbackMockMvc.perform(put("/api/order-feedbacks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(orderFeedback)))
            .andExpect(status().isBadRequest());

        // Validate the OrderFeedback in the database
        List<OrderFeedback> orderFeedbackList = orderFeedbackRepository.findAll();
        assertThat(orderFeedbackList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrderFeedback() throws Exception {
        // Initialize the database
        orderFeedbackService.save(orderFeedback);

        int databaseSizeBeforeDelete = orderFeedbackRepository.findAll().size();

        // Delete the orderFeedback
        restOrderFeedbackMockMvc.perform(delete("/api/order-feedbacks/{id}", orderFeedback.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OrderFeedback> orderFeedbackList = orderFeedbackRepository.findAll();
        assertThat(orderFeedbackList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OrderFeedback.class);
        OrderFeedback orderFeedback1 = new OrderFeedback();
        orderFeedback1.setId(1L);
        OrderFeedback orderFeedback2 = new OrderFeedback();
        orderFeedback2.setId(orderFeedback1.getId());
        assertThat(orderFeedback1).isEqualTo(orderFeedback2);
        orderFeedback2.setId(2L);
        assertThat(orderFeedback1).isNotEqualTo(orderFeedback2);
        orderFeedback1.setId(null);
        assertThat(orderFeedback1).isNotEqualTo(orderFeedback2);
    }
}

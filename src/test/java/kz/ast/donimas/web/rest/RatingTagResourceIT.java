package kz.ast.donimas.web.rest;

import kz.ast.donimas.EnjexpApp;
import kz.ast.donimas.domain.RatingTag;
import kz.ast.donimas.repository.RatingTagRepository;
import kz.ast.donimas.service.RatingTagService;
import kz.ast.donimas.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.List;

import static kz.ast.donimas.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RatingTagResource} REST controller.
 */
@SpringBootTest(classes = EnjexpApp.class)
public class RatingTagResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_EQ = new BigDecimal(1);
    private static final BigDecimal UPDATED_EQ = new BigDecimal(2);
    private static final BigDecimal SMALLER_EQ = new BigDecimal(1 - 1);

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    @Autowired
    private RatingTagRepository ratingTagRepository;

    @Autowired
    private RatingTagService ratingTagService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restRatingTagMockMvc;

    private RatingTag ratingTag;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final RatingTagResource ratingTagResource = new RatingTagResource(ratingTagService);
        this.restRatingTagMockMvc = MockMvcBuilders.standaloneSetup(ratingTagResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RatingTag createEntity(EntityManager em) {
        RatingTag ratingTag = new RatingTag()
            .name(DEFAULT_NAME)
            .eq(DEFAULT_EQ)
            .code(DEFAULT_CODE);
        return ratingTag;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RatingTag createUpdatedEntity(EntityManager em) {
        RatingTag ratingTag = new RatingTag()
            .name(UPDATED_NAME)
            .eq(UPDATED_EQ)
            .code(UPDATED_CODE);
        return ratingTag;
    }

    @BeforeEach
    public void initTest() {
        ratingTag = createEntity(em);
    }

    @Test
    @Transactional
    public void createRatingTag() throws Exception {
        int databaseSizeBeforeCreate = ratingTagRepository.findAll().size();

        // Create the RatingTag
        restRatingTagMockMvc.perform(post("/api/rating-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingTag)))
            .andExpect(status().isCreated());

        // Validate the RatingTag in the database
        List<RatingTag> ratingTagList = ratingTagRepository.findAll();
        assertThat(ratingTagList).hasSize(databaseSizeBeforeCreate + 1);
        RatingTag testRatingTag = ratingTagList.get(ratingTagList.size() - 1);
        assertThat(testRatingTag.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testRatingTag.getEq()).isEqualTo(DEFAULT_EQ);
        assertThat(testRatingTag.getCode()).isEqualTo(DEFAULT_CODE);
    }

    @Test
    @Transactional
    public void createRatingTagWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = ratingTagRepository.findAll().size();

        // Create the RatingTag with an existing ID
        ratingTag.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRatingTagMockMvc.perform(post("/api/rating-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingTag)))
            .andExpect(status().isBadRequest());

        // Validate the RatingTag in the database
        List<RatingTag> ratingTagList = ratingTagRepository.findAll();
        assertThat(ratingTagList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRatingTags() throws Exception {
        // Initialize the database
        ratingTagRepository.saveAndFlush(ratingTag);

        // Get all the ratingTagList
        restRatingTagMockMvc.perform(get("/api/rating-tags?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ratingTag.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].eq").value(hasItem(DEFAULT_EQ.intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())));
    }
    
    @Test
    @Transactional
    public void getRatingTag() throws Exception {
        // Initialize the database
        ratingTagRepository.saveAndFlush(ratingTag);

        // Get the ratingTag
        restRatingTagMockMvc.perform(get("/api/rating-tags/{id}", ratingTag.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(ratingTag.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.eq").value(DEFAULT_EQ.intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRatingTag() throws Exception {
        // Get the ratingTag
        restRatingTagMockMvc.perform(get("/api/rating-tags/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRatingTag() throws Exception {
        // Initialize the database
        ratingTagService.save(ratingTag);

        int databaseSizeBeforeUpdate = ratingTagRepository.findAll().size();

        // Update the ratingTag
        RatingTag updatedRatingTag = ratingTagRepository.findById(ratingTag.getId()).get();
        // Disconnect from session so that the updates on updatedRatingTag are not directly saved in db
        em.detach(updatedRatingTag);
        updatedRatingTag
            .name(UPDATED_NAME)
            .eq(UPDATED_EQ)
            .code(UPDATED_CODE);

        restRatingTagMockMvc.perform(put("/api/rating-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRatingTag)))
            .andExpect(status().isOk());

        // Validate the RatingTag in the database
        List<RatingTag> ratingTagList = ratingTagRepository.findAll();
        assertThat(ratingTagList).hasSize(databaseSizeBeforeUpdate);
        RatingTag testRatingTag = ratingTagList.get(ratingTagList.size() - 1);
        assertThat(testRatingTag.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testRatingTag.getEq()).isEqualTo(UPDATED_EQ);
        assertThat(testRatingTag.getCode()).isEqualTo(UPDATED_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingRatingTag() throws Exception {
        int databaseSizeBeforeUpdate = ratingTagRepository.findAll().size();

        // Create the RatingTag

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRatingTagMockMvc.perform(put("/api/rating-tags")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(ratingTag)))
            .andExpect(status().isBadRequest());

        // Validate the RatingTag in the database
        List<RatingTag> ratingTagList = ratingTagRepository.findAll();
        assertThat(ratingTagList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRatingTag() throws Exception {
        // Initialize the database
        ratingTagService.save(ratingTag);

        int databaseSizeBeforeDelete = ratingTagRepository.findAll().size();

        // Delete the ratingTag
        restRatingTagMockMvc.perform(delete("/api/rating-tags/{id}", ratingTag.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RatingTag> ratingTagList = ratingTagRepository.findAll();
        assertThat(ratingTagList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RatingTag.class);
        RatingTag ratingTag1 = new RatingTag();
        ratingTag1.setId(1L);
        RatingTag ratingTag2 = new RatingTag();
        ratingTag2.setId(ratingTag1.getId());
        assertThat(ratingTag1).isEqualTo(ratingTag2);
        ratingTag2.setId(2L);
        assertThat(ratingTag1).isNotEqualTo(ratingTag2);
        ratingTag1.setId(null);
        assertThat(ratingTag1).isNotEqualTo(ratingTag2);
    }
}

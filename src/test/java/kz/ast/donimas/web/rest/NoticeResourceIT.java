package kz.ast.donimas.web.rest;

import kz.ast.donimas.EnjexpApp;
import kz.ast.donimas.domain.Notice;
import kz.ast.donimas.repository.NoticeRepository;
import kz.ast.donimas.service.NoticeService;
import kz.ast.donimas.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.ast.donimas.domain.enumeration.NoticeReceiverType;
import kz.ast.donimas.domain.enumeration.NoticeType;
/**
 * Integration tests for the {@link NoticeResource} REST controller.
 */
@SpringBootTest(classes = EnjexpApp.class)
public class NoticeResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_CONTENT = "BBBBBBBBBB";

    private static final String DEFAULT_SMS_CONTENT = "AAAAAAAAAA";
    private static final String UPDATED_SMS_CONTENT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_PUSH = false;
    private static final Boolean UPDATED_FLAG_PUSH = true;

    private static final Boolean DEFAULT_FLAG_SMS = false;
    private static final Boolean UPDATED_FLAG_SMS = true;

    private static final Boolean DEFAULT_FLAG_EMAIL = false;
    private static final Boolean UPDATED_FLAG_EMAIL = true;

    private static final NoticeReceiverType DEFAULT_RECEIVER_TYPE = NoticeReceiverType.ONE_USER;
    private static final NoticeReceiverType UPDATED_RECEIVER_TYPE = NoticeReceiverType.GROUP;

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_MODIFY_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_MODIFY_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_MODIFY_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_CONTAINER_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_CONTAINER_CLASS = "BBBBBBBBBB";

    private static final Long DEFAULT_CONTAINER_ID = 1L;
    private static final Long UPDATED_CONTAINER_ID = 2L;
    private static final Long SMALLER_CONTAINER_ID = 1L - 1L;

    private static final NoticeType DEFAULT_NOTICE_TYPE = NoticeType.INFO;
    private static final NoticeType UPDATED_NOTICE_TYPE = NoticeType.SUCCESS;

    private static final Boolean DEFAULT_FLAG_SENT = false;
    private static final Boolean UPDATED_FLAG_SENT = true;

    private static final ZonedDateTime DEFAULT_SENT_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SENT_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_SENT_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_PAYLOAD_OBJECT_URL = "AAAAAAAAAA";
    private static final String UPDATED_PAYLOAD_OBJECT_URL = "BBBBBBBBBB";

    private static final Long DEFAULT_PAYLOAD_OBJECT_ID = 1L;
    private static final Long UPDATED_PAYLOAD_OBJECT_ID = 2L;
    private static final Long SMALLER_PAYLOAD_OBJECT_ID = 1L - 1L;

    private static final Boolean DEFAULT_FLAG_DELETED = false;
    private static final Boolean UPDATED_FLAG_DELETED = true;

    @Autowired
    private NoticeRepository noticeRepository;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restNoticeMockMvc;

    private Notice notice;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NoticeResource noticeResource = new NoticeResource(noticeService);
        this.restNoticeMockMvc = MockMvcBuilders.standaloneSetup(noticeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notice createEntity(EntityManager em) {
        Notice notice = new Notice()
            .title(DEFAULT_TITLE)
            .content(DEFAULT_CONTENT)
            .smsContent(DEFAULT_SMS_CONTENT)
            .flagPush(DEFAULT_FLAG_PUSH)
            .flagSms(DEFAULT_FLAG_SMS)
            .flagEmail(DEFAULT_FLAG_EMAIL)
            .receiverType(DEFAULT_RECEIVER_TYPE)
            .createDate(DEFAULT_CREATE_DATE)
            .modifyDate(DEFAULT_MODIFY_DATE)
            .containerClass(DEFAULT_CONTAINER_CLASS)
            .containerId(DEFAULT_CONTAINER_ID)
            .noticeType(DEFAULT_NOTICE_TYPE)
            .flagSent(DEFAULT_FLAG_SENT)
            .sentTime(DEFAULT_SENT_TIME)
            .payloadObjectUrl(DEFAULT_PAYLOAD_OBJECT_URL)
            .payloadObjectId(DEFAULT_PAYLOAD_OBJECT_ID)
            .flagDeleted(DEFAULT_FLAG_DELETED);
        return notice;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notice createUpdatedEntity(EntityManager em) {
        Notice notice = new Notice()
            .title(UPDATED_TITLE)
            .content(UPDATED_CONTENT)
            .smsContent(UPDATED_SMS_CONTENT)
            .flagPush(UPDATED_FLAG_PUSH)
            .flagSms(UPDATED_FLAG_SMS)
            .flagEmail(UPDATED_FLAG_EMAIL)
            .receiverType(UPDATED_RECEIVER_TYPE)
            .createDate(UPDATED_CREATE_DATE)
            .modifyDate(UPDATED_MODIFY_DATE)
            .containerClass(UPDATED_CONTAINER_CLASS)
            .containerId(UPDATED_CONTAINER_ID)
            .noticeType(UPDATED_NOTICE_TYPE)
            .flagSent(UPDATED_FLAG_SENT)
            .sentTime(UPDATED_SENT_TIME)
            .payloadObjectUrl(UPDATED_PAYLOAD_OBJECT_URL)
            .payloadObjectId(UPDATED_PAYLOAD_OBJECT_ID)
            .flagDeleted(UPDATED_FLAG_DELETED);
        return notice;
    }

    @BeforeEach
    public void initTest() {
        notice = createEntity(em);
    }

    @Test
    @Transactional
    public void createNotice() throws Exception {
        int databaseSizeBeforeCreate = noticeRepository.findAll().size();

        // Create the Notice
        restNoticeMockMvc.perform(post("/api/notices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notice)))
            .andExpect(status().isCreated());

        // Validate the Notice in the database
        List<Notice> noticeList = noticeRepository.findAll();
        assertThat(noticeList).hasSize(databaseSizeBeforeCreate + 1);
        Notice testNotice = noticeList.get(noticeList.size() - 1);
        assertThat(testNotice.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testNotice.getContent()).isEqualTo(DEFAULT_CONTENT);
        assertThat(testNotice.getSmsContent()).isEqualTo(DEFAULT_SMS_CONTENT);
        assertThat(testNotice.isFlagPush()).isEqualTo(DEFAULT_FLAG_PUSH);
        assertThat(testNotice.isFlagSms()).isEqualTo(DEFAULT_FLAG_SMS);
        assertThat(testNotice.isFlagEmail()).isEqualTo(DEFAULT_FLAG_EMAIL);
        assertThat(testNotice.getReceiverType()).isEqualTo(DEFAULT_RECEIVER_TYPE);
        assertThat(testNotice.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testNotice.getModifyDate()).isEqualTo(DEFAULT_MODIFY_DATE);
        assertThat(testNotice.getContainerClass()).isEqualTo(DEFAULT_CONTAINER_CLASS);
        assertThat(testNotice.getContainerId()).isEqualTo(DEFAULT_CONTAINER_ID);
        assertThat(testNotice.getNoticeType()).isEqualTo(DEFAULT_NOTICE_TYPE);
        assertThat(testNotice.isFlagSent()).isEqualTo(DEFAULT_FLAG_SENT);
        assertThat(testNotice.getSentTime()).isEqualTo(DEFAULT_SENT_TIME);
        assertThat(testNotice.getPayloadObjectUrl()).isEqualTo(DEFAULT_PAYLOAD_OBJECT_URL);
        assertThat(testNotice.getPayloadObjectId()).isEqualTo(DEFAULT_PAYLOAD_OBJECT_ID);
        assertThat(testNotice.isFlagDeleted()).isEqualTo(DEFAULT_FLAG_DELETED);
    }

    @Test
    @Transactional
    public void createNoticeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = noticeRepository.findAll().size();

        // Create the Notice with an existing ID
        notice.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNoticeMockMvc.perform(post("/api/notices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notice)))
            .andExpect(status().isBadRequest());

        // Validate the Notice in the database
        List<Notice> noticeList = noticeRepository.findAll();
        assertThat(noticeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllNotices() throws Exception {
        // Initialize the database
        noticeRepository.saveAndFlush(notice);

        // Get all the noticeList
        restNoticeMockMvc.perform(get("/api/notices?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notice.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].content").value(hasItem(DEFAULT_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].smsContent").value(hasItem(DEFAULT_SMS_CONTENT.toString())))
            .andExpect(jsonPath("$.[*].flagPush").value(hasItem(DEFAULT_FLAG_PUSH.booleanValue())))
            .andExpect(jsonPath("$.[*].flagSms").value(hasItem(DEFAULT_FLAG_SMS.booleanValue())))
            .andExpect(jsonPath("$.[*].flagEmail").value(hasItem(DEFAULT_FLAG_EMAIL.booleanValue())))
            .andExpect(jsonPath("$.[*].receiverType").value(hasItem(DEFAULT_RECEIVER_TYPE.toString())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
            .andExpect(jsonPath("$.[*].modifyDate").value(hasItem(sameInstant(DEFAULT_MODIFY_DATE))))
            .andExpect(jsonPath("$.[*].containerClass").value(hasItem(DEFAULT_CONTAINER_CLASS.toString())))
            .andExpect(jsonPath("$.[*].containerId").value(hasItem(DEFAULT_CONTAINER_ID.intValue())))
            .andExpect(jsonPath("$.[*].noticeType").value(hasItem(DEFAULT_NOTICE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].flagSent").value(hasItem(DEFAULT_FLAG_SENT.booleanValue())))
            .andExpect(jsonPath("$.[*].sentTime").value(hasItem(sameInstant(DEFAULT_SENT_TIME))))
            .andExpect(jsonPath("$.[*].payloadObjectUrl").value(hasItem(DEFAULT_PAYLOAD_OBJECT_URL.toString())))
            .andExpect(jsonPath("$.[*].payloadObjectId").value(hasItem(DEFAULT_PAYLOAD_OBJECT_ID.intValue())))
            .andExpect(jsonPath("$.[*].flagDeleted").value(hasItem(DEFAULT_FLAG_DELETED.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getNotice() throws Exception {
        // Initialize the database
        noticeRepository.saveAndFlush(notice);

        // Get the notice
        restNoticeMockMvc.perform(get("/api/notices/{id}", notice.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(notice.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.content").value(DEFAULT_CONTENT.toString()))
            .andExpect(jsonPath("$.smsContent").value(DEFAULT_SMS_CONTENT.toString()))
            .andExpect(jsonPath("$.flagPush").value(DEFAULT_FLAG_PUSH.booleanValue()))
            .andExpect(jsonPath("$.flagSms").value(DEFAULT_FLAG_SMS.booleanValue()))
            .andExpect(jsonPath("$.flagEmail").value(DEFAULT_FLAG_EMAIL.booleanValue()))
            .andExpect(jsonPath("$.receiverType").value(DEFAULT_RECEIVER_TYPE.toString()))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
            .andExpect(jsonPath("$.modifyDate").value(sameInstant(DEFAULT_MODIFY_DATE)))
            .andExpect(jsonPath("$.containerClass").value(DEFAULT_CONTAINER_CLASS.toString()))
            .andExpect(jsonPath("$.containerId").value(DEFAULT_CONTAINER_ID.intValue()))
            .andExpect(jsonPath("$.noticeType").value(DEFAULT_NOTICE_TYPE.toString()))
            .andExpect(jsonPath("$.flagSent").value(DEFAULT_FLAG_SENT.booleanValue()))
            .andExpect(jsonPath("$.sentTime").value(sameInstant(DEFAULT_SENT_TIME)))
            .andExpect(jsonPath("$.payloadObjectUrl").value(DEFAULT_PAYLOAD_OBJECT_URL.toString()))
            .andExpect(jsonPath("$.payloadObjectId").value(DEFAULT_PAYLOAD_OBJECT_ID.intValue()))
            .andExpect(jsonPath("$.flagDeleted").value(DEFAULT_FLAG_DELETED.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingNotice() throws Exception {
        // Get the notice
        restNoticeMockMvc.perform(get("/api/notices/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNotice() throws Exception {
        // Initialize the database
        noticeService.save(notice);

        int databaseSizeBeforeUpdate = noticeRepository.findAll().size();

        // Update the notice
        Notice updatedNotice = noticeRepository.findById(notice.getId()).get();
        // Disconnect from session so that the updates on updatedNotice are not directly saved in db
        em.detach(updatedNotice);
        updatedNotice
            .title(UPDATED_TITLE)
            .content(UPDATED_CONTENT)
            .smsContent(UPDATED_SMS_CONTENT)
            .flagPush(UPDATED_FLAG_PUSH)
            .flagSms(UPDATED_FLAG_SMS)
            .flagEmail(UPDATED_FLAG_EMAIL)
            .receiverType(UPDATED_RECEIVER_TYPE)
            .createDate(UPDATED_CREATE_DATE)
            .modifyDate(UPDATED_MODIFY_DATE)
            .containerClass(UPDATED_CONTAINER_CLASS)
            .containerId(UPDATED_CONTAINER_ID)
            .noticeType(UPDATED_NOTICE_TYPE)
            .flagSent(UPDATED_FLAG_SENT)
            .sentTime(UPDATED_SENT_TIME)
            .payloadObjectUrl(UPDATED_PAYLOAD_OBJECT_URL)
            .payloadObjectId(UPDATED_PAYLOAD_OBJECT_ID)
            .flagDeleted(UPDATED_FLAG_DELETED);

        restNoticeMockMvc.perform(put("/api/notices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNotice)))
            .andExpect(status().isOk());

        // Validate the Notice in the database
        List<Notice> noticeList = noticeRepository.findAll();
        assertThat(noticeList).hasSize(databaseSizeBeforeUpdate);
        Notice testNotice = noticeList.get(noticeList.size() - 1);
        assertThat(testNotice.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testNotice.getContent()).isEqualTo(UPDATED_CONTENT);
        assertThat(testNotice.getSmsContent()).isEqualTo(UPDATED_SMS_CONTENT);
        assertThat(testNotice.isFlagPush()).isEqualTo(UPDATED_FLAG_PUSH);
        assertThat(testNotice.isFlagSms()).isEqualTo(UPDATED_FLAG_SMS);
        assertThat(testNotice.isFlagEmail()).isEqualTo(UPDATED_FLAG_EMAIL);
        assertThat(testNotice.getReceiverType()).isEqualTo(UPDATED_RECEIVER_TYPE);
        assertThat(testNotice.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testNotice.getModifyDate()).isEqualTo(UPDATED_MODIFY_DATE);
        assertThat(testNotice.getContainerClass()).isEqualTo(UPDATED_CONTAINER_CLASS);
        assertThat(testNotice.getContainerId()).isEqualTo(UPDATED_CONTAINER_ID);
        assertThat(testNotice.getNoticeType()).isEqualTo(UPDATED_NOTICE_TYPE);
        assertThat(testNotice.isFlagSent()).isEqualTo(UPDATED_FLAG_SENT);
        assertThat(testNotice.getSentTime()).isEqualTo(UPDATED_SENT_TIME);
        assertThat(testNotice.getPayloadObjectUrl()).isEqualTo(UPDATED_PAYLOAD_OBJECT_URL);
        assertThat(testNotice.getPayloadObjectId()).isEqualTo(UPDATED_PAYLOAD_OBJECT_ID);
        assertThat(testNotice.isFlagDeleted()).isEqualTo(UPDATED_FLAG_DELETED);
    }

    @Test
    @Transactional
    public void updateNonExistingNotice() throws Exception {
        int databaseSizeBeforeUpdate = noticeRepository.findAll().size();

        // Create the Notice

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restNoticeMockMvc.perform(put("/api/notices")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notice)))
            .andExpect(status().isBadRequest());

        // Validate the Notice in the database
        List<Notice> noticeList = noticeRepository.findAll();
        assertThat(noticeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteNotice() throws Exception {
        // Initialize the database
        noticeService.save(notice);

        int databaseSizeBeforeDelete = noticeRepository.findAll().size();

        // Delete the notice
        restNoticeMockMvc.perform(delete("/api/notices/{id}", notice.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Notice> noticeList = noticeRepository.findAll();
        assertThat(noticeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Notice.class);
        Notice notice1 = new Notice();
        notice1.setId(1L);
        Notice notice2 = new Notice();
        notice2.setId(notice1.getId());
        assertThat(notice1).isEqualTo(notice2);
        notice2.setId(2L);
        assertThat(notice1).isNotEqualTo(notice2);
        notice1.setId(null);
        assertThat(notice1).isNotEqualTo(notice2);
    }
}

package kz.ast.donimas.web.rest;

import kz.ast.donimas.EnjexpApp;
import kz.ast.donimas.domain.Promocode;
import kz.ast.donimas.repository.PromocodeRepository;
import kz.ast.donimas.service.PromocodeService;
import kz.ast.donimas.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PromocodeResource} REST controller.
 */
@SpringBootTest(classes = EnjexpApp.class)
public class PromocodeResourceIT {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_ACTIVE = false;
    private static final Boolean UPDATED_FLAG_ACTIVE = true;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CONTAINER_CLASS = "AAAAAAAAAA";
    private static final String UPDATED_CONTAINER_CLASS = "BBBBBBBBBB";

    private static final Long DEFAULT_CONTAINER_ID = 1L;
    private static final Long UPDATED_CONTAINER_ID = 2L;
    private static final Long SMALLER_CONTAINER_ID = 1L - 1L;

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private PromocodeRepository promocodeRepository;

    @Autowired
    private PromocodeService promocodeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPromocodeMockMvc;

    private Promocode promocode;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PromocodeResource promocodeResource = new PromocodeResource(promocodeService);
        this.restPromocodeMockMvc = MockMvcBuilders.standaloneSetup(promocodeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Promocode createEntity(EntityManager em) {
        Promocode promocode = new Promocode()
            .code(DEFAULT_CODE)
            .flagActive(DEFAULT_FLAG_ACTIVE)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .containerClass(DEFAULT_CONTAINER_CLASS)
            .containerId(DEFAULT_CONTAINER_ID)
            .createDate(DEFAULT_CREATE_DATE);
        return promocode;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Promocode createUpdatedEntity(EntityManager em) {
        Promocode promocode = new Promocode()
            .code(UPDATED_CODE)
            .flagActive(UPDATED_FLAG_ACTIVE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .containerClass(UPDATED_CONTAINER_CLASS)
            .containerId(UPDATED_CONTAINER_ID)
            .createDate(UPDATED_CREATE_DATE);
        return promocode;
    }

    @BeforeEach
    public void initTest() {
        promocode = createEntity(em);
    }

    @Test
    @Transactional
    public void createPromocode() throws Exception {
        int databaseSizeBeforeCreate = promocodeRepository.findAll().size();

        // Create the Promocode
        restPromocodeMockMvc.perform(post("/api/promocodes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promocode)))
            .andExpect(status().isCreated());

        // Validate the Promocode in the database
        List<Promocode> promocodeList = promocodeRepository.findAll();
        assertThat(promocodeList).hasSize(databaseSizeBeforeCreate + 1);
        Promocode testPromocode = promocodeList.get(promocodeList.size() - 1);
        assertThat(testPromocode.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testPromocode.isFlagActive()).isEqualTo(DEFAULT_FLAG_ACTIVE);
        assertThat(testPromocode.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPromocode.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testPromocode.getContainerClass()).isEqualTo(DEFAULT_CONTAINER_CLASS);
        assertThat(testPromocode.getContainerId()).isEqualTo(DEFAULT_CONTAINER_ID);
        assertThat(testPromocode.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
    }

    @Test
    @Transactional
    public void createPromocodeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = promocodeRepository.findAll().size();

        // Create the Promocode with an existing ID
        promocode.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPromocodeMockMvc.perform(post("/api/promocodes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promocode)))
            .andExpect(status().isBadRequest());

        // Validate the Promocode in the database
        List<Promocode> promocodeList = promocodeRepository.findAll();
        assertThat(promocodeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPromocodes() throws Exception {
        // Initialize the database
        promocodeRepository.saveAndFlush(promocode);

        // Get all the promocodeList
        restPromocodeMockMvc.perform(get("/api/promocodes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(promocode.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].flagActive").value(hasItem(DEFAULT_FLAG_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].containerClass").value(hasItem(DEFAULT_CONTAINER_CLASS.toString())))
            .andExpect(jsonPath("$.[*].containerId").value(hasItem(DEFAULT_CONTAINER_ID.intValue())))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))));
    }
    
    @Test
    @Transactional
    public void getPromocode() throws Exception {
        // Initialize the database
        promocodeRepository.saveAndFlush(promocode);

        // Get the promocode
        restPromocodeMockMvc.perform(get("/api/promocodes/{id}", promocode.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(promocode.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.flagActive").value(DEFAULT_FLAG_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.containerClass").value(DEFAULT_CONTAINER_CLASS.toString()))
            .andExpect(jsonPath("$.containerId").value(DEFAULT_CONTAINER_ID.intValue()))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingPromocode() throws Exception {
        // Get the promocode
        restPromocodeMockMvc.perform(get("/api/promocodes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePromocode() throws Exception {
        // Initialize the database
        promocodeService.save(promocode);

        int databaseSizeBeforeUpdate = promocodeRepository.findAll().size();

        // Update the promocode
        Promocode updatedPromocode = promocodeRepository.findById(promocode.getId()).get();
        // Disconnect from session so that the updates on updatedPromocode are not directly saved in db
        em.detach(updatedPromocode);
        updatedPromocode
            .code(UPDATED_CODE)
            .flagActive(UPDATED_FLAG_ACTIVE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .containerClass(UPDATED_CONTAINER_CLASS)
            .containerId(UPDATED_CONTAINER_ID)
            .createDate(UPDATED_CREATE_DATE);

        restPromocodeMockMvc.perform(put("/api/promocodes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPromocode)))
            .andExpect(status().isOk());

        // Validate the Promocode in the database
        List<Promocode> promocodeList = promocodeRepository.findAll();
        assertThat(promocodeList).hasSize(databaseSizeBeforeUpdate);
        Promocode testPromocode = promocodeList.get(promocodeList.size() - 1);
        assertThat(testPromocode.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testPromocode.isFlagActive()).isEqualTo(UPDATED_FLAG_ACTIVE);
        assertThat(testPromocode.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPromocode.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testPromocode.getContainerClass()).isEqualTo(UPDATED_CONTAINER_CLASS);
        assertThat(testPromocode.getContainerId()).isEqualTo(UPDATED_CONTAINER_ID);
        assertThat(testPromocode.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPromocode() throws Exception {
        int databaseSizeBeforeUpdate = promocodeRepository.findAll().size();

        // Create the Promocode

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPromocodeMockMvc.perform(put("/api/promocodes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promocode)))
            .andExpect(status().isBadRequest());

        // Validate the Promocode in the database
        List<Promocode> promocodeList = promocodeRepository.findAll();
        assertThat(promocodeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePromocode() throws Exception {
        // Initialize the database
        promocodeService.save(promocode);

        int databaseSizeBeforeDelete = promocodeRepository.findAll().size();

        // Delete the promocode
        restPromocodeMockMvc.perform(delete("/api/promocodes/{id}", promocode.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Promocode> promocodeList = promocodeRepository.findAll();
        assertThat(promocodeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Promocode.class);
        Promocode promocode1 = new Promocode();
        promocode1.setId(1L);
        Promocode promocode2 = new Promocode();
        promocode2.setId(promocode1.getId());
        assertThat(promocode1).isEqualTo(promocode2);
        promocode2.setId(2L);
        assertThat(promocode1).isNotEqualTo(promocode2);
        promocode1.setId(null);
        assertThat(promocode1).isNotEqualTo(promocode2);
    }
}

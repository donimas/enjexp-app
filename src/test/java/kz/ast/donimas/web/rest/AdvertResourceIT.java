package kz.ast.donimas.web.rest;

import kz.ast.donimas.EnjexpApp;
import kz.ast.donimas.domain.Advert;
import kz.ast.donimas.repository.AdvertRepository;
import kz.ast.donimas.service.AdvertService;
import kz.ast.donimas.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static kz.ast.donimas.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AdvertResource} REST controller.
 */
@SpringBootTest(classes = EnjexpApp.class)
public class AdvertResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final byte[] DEFAULT_IMAGE = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_IMAGE = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_IMAGE_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_IMAGE_CONTENT_TYPE = "image/png";

    @Autowired
    private AdvertRepository advertRepository;

    @Autowired
    private AdvertService advertService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restAdvertMockMvc;

    private Advert advert;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final AdvertResource advertResource = new AdvertResource(advertService);
        this.restAdvertMockMvc = MockMvcBuilders.standaloneSetup(advertResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Advert createEntity(EntityManager em) {
        Advert advert = new Advert()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION)
            .image(DEFAULT_IMAGE)
            .imageContentType(DEFAULT_IMAGE_CONTENT_TYPE);
        return advert;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Advert createUpdatedEntity(EntityManager em) {
        Advert advert = new Advert()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);
        return advert;
    }

    @BeforeEach
    public void initTest() {
        advert = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdvert() throws Exception {
        int databaseSizeBeforeCreate = advertRepository.findAll().size();

        // Create the Advert
        restAdvertMockMvc.perform(post("/api/adverts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advert)))
            .andExpect(status().isCreated());

        // Validate the Advert in the database
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeCreate + 1);
        Advert testAdvert = advertList.get(advertList.size() - 1);
        assertThat(testAdvert.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testAdvert.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testAdvert.getImage()).isEqualTo(DEFAULT_IMAGE);
        assertThat(testAdvert.getImageContentType()).isEqualTo(DEFAULT_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void createAdvertWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = advertRepository.findAll().size();

        // Create the Advert with an existing ID
        advert.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdvertMockMvc.perform(post("/api/adverts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advert)))
            .andExpect(status().isBadRequest());

        // Validate the Advert in the database
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAdverts() throws Exception {
        // Initialize the database
        advertRepository.saveAndFlush(advert);

        // Get all the advertList
        restAdvertMockMvc.perform(get("/api/adverts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(advert.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].imageContentType").value(hasItem(DEFAULT_IMAGE_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].image").value(hasItem(Base64Utils.encodeToString(DEFAULT_IMAGE))));
    }
    
    @Test
    @Transactional
    public void getAdvert() throws Exception {
        // Initialize the database
        advertRepository.saveAndFlush(advert);

        // Get the advert
        restAdvertMockMvc.perform(get("/api/adverts/{id}", advert.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(advert.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.imageContentType").value(DEFAULT_IMAGE_CONTENT_TYPE))
            .andExpect(jsonPath("$.image").value(Base64Utils.encodeToString(DEFAULT_IMAGE)));
    }

    @Test
    @Transactional
    public void getNonExistingAdvert() throws Exception {
        // Get the advert
        restAdvertMockMvc.perform(get("/api/adverts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdvert() throws Exception {
        // Initialize the database
        advertService.save(advert);

        int databaseSizeBeforeUpdate = advertRepository.findAll().size();

        // Update the advert
        Advert updatedAdvert = advertRepository.findById(advert.getId()).get();
        // Disconnect from session so that the updates on updatedAdvert are not directly saved in db
        em.detach(updatedAdvert);
        updatedAdvert
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION)
            .image(UPDATED_IMAGE)
            .imageContentType(UPDATED_IMAGE_CONTENT_TYPE);

        restAdvertMockMvc.perform(put("/api/adverts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedAdvert)))
            .andExpect(status().isOk());

        // Validate the Advert in the database
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeUpdate);
        Advert testAdvert = advertList.get(advertList.size() - 1);
        assertThat(testAdvert.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testAdvert.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testAdvert.getImage()).isEqualTo(UPDATED_IMAGE);
        assertThat(testAdvert.getImageContentType()).isEqualTo(UPDATED_IMAGE_CONTENT_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingAdvert() throws Exception {
        int databaseSizeBeforeUpdate = advertRepository.findAll().size();

        // Create the Advert

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdvertMockMvc.perform(put("/api/adverts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(advert)))
            .andExpect(status().isBadRequest());

        // Validate the Advert in the database
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdvert() throws Exception {
        // Initialize the database
        advertService.save(advert);

        int databaseSizeBeforeDelete = advertRepository.findAll().size();

        // Delete the advert
        restAdvertMockMvc.perform(delete("/api/adverts/{id}", advert.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Advert> advertList = advertRepository.findAll();
        assertThat(advertList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Advert.class);
        Advert advert1 = new Advert();
        advert1.setId(1L);
        Advert advert2 = new Advert();
        advert2.setId(advert1.getId());
        assertThat(advert1).isEqualTo(advert2);
        advert2.setId(2L);
        assertThat(advert1).isNotEqualTo(advert2);
        advert1.setId(null);
        assertThat(advert1).isNotEqualTo(advert2);
    }
}

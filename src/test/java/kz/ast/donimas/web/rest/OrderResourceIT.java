package kz.ast.donimas.web.rest;

import kz.ast.donimas.EnjexpApp;
import kz.ast.donimas.domain.Order;
import kz.ast.donimas.repository.OrderRepository;
import kz.ast.donimas.service.OrderService;
import kz.ast.donimas.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import kz.ast.donimas.domain.enumeration.PaymentType;
import kz.ast.donimas.domain.enumeration.ServiceType;
import kz.ast.donimas.domain.enumeration.LocationType;
/**
 * Integration tests for the {@link OrderResource} REST controller.
 */
@SpringBootTest(classes = EnjexpApp.class)
public class OrderResourceIT {

    private static final String DEFAULT_PROMOCODE = "AAAAAAAAAA";
    private static final String UPDATED_PROMOCODE = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_TOTAL_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_TOTAL_PRICE = new BigDecimal(2);
    private static final BigDecimal SMALLER_TOTAL_PRICE = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_CHANGE = new BigDecimal(1);
    private static final BigDecimal UPDATED_CHANGE = new BigDecimal(2);
    private static final BigDecimal SMALLER_CHANGE = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_PAID = new BigDecimal(1);
    private static final BigDecimal UPDATED_PAID = new BigDecimal(2);
    private static final BigDecimal SMALLER_PAID = new BigDecimal(1 - 1);

    private static final PaymentType DEFAULT_PAYMENT_TYPE = PaymentType.CASH;
    private static final PaymentType UPDATED_PAYMENT_TYPE = PaymentType.EPAY;

    private static final String DEFAULT_PAYMENT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PAYMENT_CODE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FLAG_PAID = false;
    private static final Boolean UPDATED_FLAG_PAID = true;

    private static final ServiceType DEFAULT_SERVICE_TYPE = ServiceType.PICK_UP;
    private static final ServiceType UPDATED_SERVICE_TYPE = ServiceType.DELIVERY;

    private static final ZonedDateTime DEFAULT_SERVICE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_SERVICE_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_SERVICE_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_CREATE_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final LocationType DEFAULT_LOCATION_TYPE = LocationType.KAZGYU;
    private static final LocationType UPDATED_LOCATION_TYPE = LocationType.COMMUNITY;

    private static final BigDecimal DEFAULT_DELIVERY_DURATION = new BigDecimal(1);
    private static final BigDecimal UPDATED_DELIVERY_DURATION = new BigDecimal(2);
    private static final BigDecimal SMALLER_DELIVERY_DURATION = new BigDecimal(1 - 1);

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restOrderMockMvc;

    private Order order;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final OrderResource orderResource = new OrderResource(orderService);
        this.restOrderMockMvc = MockMvcBuilders.standaloneSetup(orderResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Order createEntity(EntityManager em) {
        Order order = new Order()
            .promocode(DEFAULT_PROMOCODE)
            .totalPrice(DEFAULT_TOTAL_PRICE)
            .change(DEFAULT_CHANGE)
            .paid(DEFAULT_PAID)
            .paymentType(DEFAULT_PAYMENT_TYPE)
            .paymentCode(DEFAULT_PAYMENT_CODE)
            .flagPaid(DEFAULT_FLAG_PAID)
            .serviceType(DEFAULT_SERVICE_TYPE)
            .serviceTime(DEFAULT_SERVICE_TIME)
            .createDate(DEFAULT_CREATE_DATE)
            .comment(DEFAULT_COMMENT)
            .locationType(DEFAULT_LOCATION_TYPE)
            .deliveryDuration(DEFAULT_DELIVERY_DURATION);
        return order;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Order createUpdatedEntity(EntityManager em) {
        Order order = new Order()
            .promocode(UPDATED_PROMOCODE)
            .totalPrice(UPDATED_TOTAL_PRICE)
            .change(UPDATED_CHANGE)
            .paid(UPDATED_PAID)
            .paymentType(UPDATED_PAYMENT_TYPE)
            .paymentCode(UPDATED_PAYMENT_CODE)
            .flagPaid(UPDATED_FLAG_PAID)
            .serviceType(UPDATED_SERVICE_TYPE)
            .serviceTime(UPDATED_SERVICE_TIME)
            .createDate(UPDATED_CREATE_DATE)
            .comment(UPDATED_COMMENT)
            .locationType(UPDATED_LOCATION_TYPE)
            .deliveryDuration(UPDATED_DELIVERY_DURATION);
        return order;
    }

    @BeforeEach
    public void initTest() {
        order = createEntity(em);
    }

    @Test
    @Transactional
    public void createOrder() throws Exception {
        int databaseSizeBeforeCreate = orderRepository.findAll().size();

        // Create the Order
        restOrderMockMvc.perform(post("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(order)))
            .andExpect(status().isCreated());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(databaseSizeBeforeCreate + 1);
        Order testOrder = orderList.get(orderList.size() - 1);
        assertThat(testOrder.getPromocode()).isEqualTo(DEFAULT_PROMOCODE);
        assertThat(testOrder.getTotalPrice()).isEqualTo(DEFAULT_TOTAL_PRICE);
        assertThat(testOrder.getChange()).isEqualTo(DEFAULT_CHANGE);
        assertThat(testOrder.getPaid()).isEqualTo(DEFAULT_PAID);
        assertThat(testOrder.getPaymentType()).isEqualTo(DEFAULT_PAYMENT_TYPE);
        assertThat(testOrder.getPaymentCode()).isEqualTo(DEFAULT_PAYMENT_CODE);
        assertThat(testOrder.isFlagPaid()).isEqualTo(DEFAULT_FLAG_PAID);
        assertThat(testOrder.getServiceType()).isEqualTo(DEFAULT_SERVICE_TYPE);
        assertThat(testOrder.getServiceTime()).isEqualTo(DEFAULT_SERVICE_TIME);
        assertThat(testOrder.getCreateDate()).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testOrder.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testOrder.getLocationType()).isEqualTo(DEFAULT_LOCATION_TYPE);
        assertThat(testOrder.getDeliveryDuration()).isEqualTo(DEFAULT_DELIVERY_DURATION);
    }

    @Test
    @Transactional
    public void createOrderWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = orderRepository.findAll().size();

        // Create the Order with an existing ID
        order.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOrderMockMvc.perform(post("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(order)))
            .andExpect(status().isBadRequest());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOrders() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get all the orderList
        restOrderMockMvc.perform(get("/api/orders?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(order.getId().intValue())))
            .andExpect(jsonPath("$.[*].promocode").value(hasItem(DEFAULT_PROMOCODE.toString())))
            .andExpect(jsonPath("$.[*].totalPrice").value(hasItem(DEFAULT_TOTAL_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].change").value(hasItem(DEFAULT_CHANGE.intValue())))
            .andExpect(jsonPath("$.[*].paid").value(hasItem(DEFAULT_PAID.intValue())))
            .andExpect(jsonPath("$.[*].paymentType").value(hasItem(DEFAULT_PAYMENT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].paymentCode").value(hasItem(DEFAULT_PAYMENT_CODE.toString())))
            .andExpect(jsonPath("$.[*].flagPaid").value(hasItem(DEFAULT_FLAG_PAID.booleanValue())))
            .andExpect(jsonPath("$.[*].serviceType").value(hasItem(DEFAULT_SERVICE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].serviceTime").value(hasItem(sameInstant(DEFAULT_SERVICE_TIME))))
            .andExpect(jsonPath("$.[*].createDate").value(hasItem(sameInstant(DEFAULT_CREATE_DATE))))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT.toString())))
            .andExpect(jsonPath("$.[*].locationType").value(hasItem(DEFAULT_LOCATION_TYPE.toString())))
            .andExpect(jsonPath("$.[*].deliveryDuration").value(hasItem(DEFAULT_DELIVERY_DURATION.intValue())));
    }
    
    @Test
    @Transactional
    public void getOrder() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get the order
        restOrderMockMvc.perform(get("/api/orders/{id}", order.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(order.getId().intValue()))
            .andExpect(jsonPath("$.promocode").value(DEFAULT_PROMOCODE.toString()))
            .andExpect(jsonPath("$.totalPrice").value(DEFAULT_TOTAL_PRICE.intValue()))
            .andExpect(jsonPath("$.change").value(DEFAULT_CHANGE.intValue()))
            .andExpect(jsonPath("$.paid").value(DEFAULT_PAID.intValue()))
            .andExpect(jsonPath("$.paymentType").value(DEFAULT_PAYMENT_TYPE.toString()))
            .andExpect(jsonPath("$.paymentCode").value(DEFAULT_PAYMENT_CODE.toString()))
            .andExpect(jsonPath("$.flagPaid").value(DEFAULT_FLAG_PAID.booleanValue()))
            .andExpect(jsonPath("$.serviceType").value(DEFAULT_SERVICE_TYPE.toString()))
            .andExpect(jsonPath("$.serviceTime").value(sameInstant(DEFAULT_SERVICE_TIME)))
            .andExpect(jsonPath("$.createDate").value(sameInstant(DEFAULT_CREATE_DATE)))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT.toString()))
            .andExpect(jsonPath("$.locationType").value(DEFAULT_LOCATION_TYPE.toString()))
            .andExpect(jsonPath("$.deliveryDuration").value(DEFAULT_DELIVERY_DURATION.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingOrder() throws Exception {
        // Get the order
        restOrderMockMvc.perform(get("/api/orders/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrder() throws Exception {
        // Initialize the database
        orderService.save(order);

        int databaseSizeBeforeUpdate = orderRepository.findAll().size();

        // Update the order
        Order updatedOrder = orderRepository.findById(order.getId()).get();
        // Disconnect from session so that the updates on updatedOrder are not directly saved in db
        em.detach(updatedOrder);
        updatedOrder
            .promocode(UPDATED_PROMOCODE)
            .totalPrice(UPDATED_TOTAL_PRICE)
            .change(UPDATED_CHANGE)
            .paid(UPDATED_PAID)
            .paymentType(UPDATED_PAYMENT_TYPE)
            .paymentCode(UPDATED_PAYMENT_CODE)
            .flagPaid(UPDATED_FLAG_PAID)
            .serviceType(UPDATED_SERVICE_TYPE)
            .serviceTime(UPDATED_SERVICE_TIME)
            .createDate(UPDATED_CREATE_DATE)
            .comment(UPDATED_COMMENT)
            .locationType(UPDATED_LOCATION_TYPE)
            .deliveryDuration(UPDATED_DELIVERY_DURATION);

        restOrderMockMvc.perform(put("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedOrder)))
            .andExpect(status().isOk());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(databaseSizeBeforeUpdate);
        Order testOrder = orderList.get(orderList.size() - 1);
        assertThat(testOrder.getPromocode()).isEqualTo(UPDATED_PROMOCODE);
        assertThat(testOrder.getTotalPrice()).isEqualTo(UPDATED_TOTAL_PRICE);
        assertThat(testOrder.getChange()).isEqualTo(UPDATED_CHANGE);
        assertThat(testOrder.getPaid()).isEqualTo(UPDATED_PAID);
        assertThat(testOrder.getPaymentType()).isEqualTo(UPDATED_PAYMENT_TYPE);
        assertThat(testOrder.getPaymentCode()).isEqualTo(UPDATED_PAYMENT_CODE);
        assertThat(testOrder.isFlagPaid()).isEqualTo(UPDATED_FLAG_PAID);
        assertThat(testOrder.getServiceType()).isEqualTo(UPDATED_SERVICE_TYPE);
        assertThat(testOrder.getServiceTime()).isEqualTo(UPDATED_SERVICE_TIME);
        assertThat(testOrder.getCreateDate()).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testOrder.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testOrder.getLocationType()).isEqualTo(UPDATED_LOCATION_TYPE);
        assertThat(testOrder.getDeliveryDuration()).isEqualTo(UPDATED_DELIVERY_DURATION);
    }

    @Test
    @Transactional
    public void updateNonExistingOrder() throws Exception {
        int databaseSizeBeforeUpdate = orderRepository.findAll().size();

        // Create the Order

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOrderMockMvc.perform(put("/api/orders")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(order)))
            .andExpect(status().isBadRequest());

        // Validate the Order in the database
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOrder() throws Exception {
        // Initialize the database
        orderService.save(order);

        int databaseSizeBeforeDelete = orderRepository.findAll().size();

        // Delete the order
        restOrderMockMvc.perform(delete("/api/orders/{id}", order.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Order> orderList = orderRepository.findAll();
        assertThat(orderList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Order.class);
        Order order1 = new Order();
        order1.setId(1L);
        Order order2 = new Order();
        order2.setId(order1.getId());
        assertThat(order1).isEqualTo(order2);
        order2.setId(2L);
        assertThat(order1).isNotEqualTo(order2);
        order1.setId(null);
        assertThat(order1).isNotEqualTo(order2);
    }
}

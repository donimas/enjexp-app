package kz.ast.donimas.web.rest;

import kz.ast.donimas.EnjexpApp;
import kz.ast.donimas.domain.PromocodeSale;
import kz.ast.donimas.repository.PromocodeSaleRepository;
import kz.ast.donimas.service.PromocodeSaleService;
import kz.ast.donimas.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static kz.ast.donimas.web.rest.TestUtil.sameInstant;
import static kz.ast.donimas.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link PromocodeSaleResource} REST controller.
 */
@SpringBootTest(classes = EnjexpApp.class)
public class PromocodeSaleResourceIT {

    private static final BigDecimal DEFAULT_SALE = new BigDecimal(1);
    private static final BigDecimal UPDATED_SALE = new BigDecimal(2);
    private static final BigDecimal SMALLER_SALE = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_MIN_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MIN_PRICE = new BigDecimal(2);
    private static final BigDecimal SMALLER_MIN_PRICE = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_MAX_PRICE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_PRICE = new BigDecimal(2);
    private static final BigDecimal SMALLER_MAX_PRICE = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_MIN_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MIN_AMOUNT = new BigDecimal(2);
    private static final BigDecimal SMALLER_MIN_AMOUNT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_MAX_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_MAX_AMOUNT = new BigDecimal(2);
    private static final BigDecimal SMALLER_MAX_AMOUNT = new BigDecimal(1 - 1);

    private static final BigDecimal DEFAULT_MIN_SHARE = new BigDecimal(1);
    private static final BigDecimal UPDATED_MIN_SHARE = new BigDecimal(2);
    private static final BigDecimal SMALLER_MIN_SHARE = new BigDecimal(1 - 1);

    private static final ZonedDateTime DEFAULT_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_START_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final ZonedDateTime DEFAULT_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END_DATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final ZonedDateTime SMALLER_END_DATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    @Autowired
    private PromocodeSaleRepository promocodeSaleRepository;

    @Autowired
    private PromocodeSaleService promocodeSaleService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPromocodeSaleMockMvc;

    private PromocodeSale promocodeSale;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PromocodeSaleResource promocodeSaleResource = new PromocodeSaleResource(promocodeSaleService);
        this.restPromocodeSaleMockMvc = MockMvcBuilders.standaloneSetup(promocodeSaleResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PromocodeSale createEntity(EntityManager em) {
        PromocodeSale promocodeSale = new PromocodeSale()
            .sale(DEFAULT_SALE)
            .minPrice(DEFAULT_MIN_PRICE)
            .maxPrice(DEFAULT_MAX_PRICE)
            .minAmount(DEFAULT_MIN_AMOUNT)
            .maxAmount(DEFAULT_MAX_AMOUNT)
            .minShare(DEFAULT_MIN_SHARE)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE);
        return promocodeSale;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PromocodeSale createUpdatedEntity(EntityManager em) {
        PromocodeSale promocodeSale = new PromocodeSale()
            .sale(UPDATED_SALE)
            .minPrice(UPDATED_MIN_PRICE)
            .maxPrice(UPDATED_MAX_PRICE)
            .minAmount(UPDATED_MIN_AMOUNT)
            .maxAmount(UPDATED_MAX_AMOUNT)
            .minShare(UPDATED_MIN_SHARE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE);
        return promocodeSale;
    }

    @BeforeEach
    public void initTest() {
        promocodeSale = createEntity(em);
    }

    @Test
    @Transactional
    public void createPromocodeSale() throws Exception {
        int databaseSizeBeforeCreate = promocodeSaleRepository.findAll().size();

        // Create the PromocodeSale
        restPromocodeSaleMockMvc.perform(post("/api/promocode-sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promocodeSale)))
            .andExpect(status().isCreated());

        // Validate the PromocodeSale in the database
        List<PromocodeSale> promocodeSaleList = promocodeSaleRepository.findAll();
        assertThat(promocodeSaleList).hasSize(databaseSizeBeforeCreate + 1);
        PromocodeSale testPromocodeSale = promocodeSaleList.get(promocodeSaleList.size() - 1);
        assertThat(testPromocodeSale.getSale()).isEqualTo(DEFAULT_SALE);
        assertThat(testPromocodeSale.getMinPrice()).isEqualTo(DEFAULT_MIN_PRICE);
        assertThat(testPromocodeSale.getMaxPrice()).isEqualTo(DEFAULT_MAX_PRICE);
        assertThat(testPromocodeSale.getMinAmount()).isEqualTo(DEFAULT_MIN_AMOUNT);
        assertThat(testPromocodeSale.getMaxAmount()).isEqualTo(DEFAULT_MAX_AMOUNT);
        assertThat(testPromocodeSale.getMinShare()).isEqualTo(DEFAULT_MIN_SHARE);
        assertThat(testPromocodeSale.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testPromocodeSale.getEndDate()).isEqualTo(DEFAULT_END_DATE);
    }

    @Test
    @Transactional
    public void createPromocodeSaleWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = promocodeSaleRepository.findAll().size();

        // Create the PromocodeSale with an existing ID
        promocodeSale.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPromocodeSaleMockMvc.perform(post("/api/promocode-sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promocodeSale)))
            .andExpect(status().isBadRequest());

        // Validate the PromocodeSale in the database
        List<PromocodeSale> promocodeSaleList = promocodeSaleRepository.findAll();
        assertThat(promocodeSaleList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPromocodeSales() throws Exception {
        // Initialize the database
        promocodeSaleRepository.saveAndFlush(promocodeSale);

        // Get all the promocodeSaleList
        restPromocodeSaleMockMvc.perform(get("/api/promocode-sales?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(promocodeSale.getId().intValue())))
            .andExpect(jsonPath("$.[*].sale").value(hasItem(DEFAULT_SALE.intValue())))
            .andExpect(jsonPath("$.[*].minPrice").value(hasItem(DEFAULT_MIN_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].maxPrice").value(hasItem(DEFAULT_MAX_PRICE.intValue())))
            .andExpect(jsonPath("$.[*].minAmount").value(hasItem(DEFAULT_MIN_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].maxAmount").value(hasItem(DEFAULT_MAX_AMOUNT.intValue())))
            .andExpect(jsonPath("$.[*].minShare").value(hasItem(DEFAULT_MIN_SHARE.intValue())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(sameInstant(DEFAULT_START_DATE))))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(sameInstant(DEFAULT_END_DATE))));
    }
    
    @Test
    @Transactional
    public void getPromocodeSale() throws Exception {
        // Initialize the database
        promocodeSaleRepository.saveAndFlush(promocodeSale);

        // Get the promocodeSale
        restPromocodeSaleMockMvc.perform(get("/api/promocode-sales/{id}", promocodeSale.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(promocodeSale.getId().intValue()))
            .andExpect(jsonPath("$.sale").value(DEFAULT_SALE.intValue()))
            .andExpect(jsonPath("$.minPrice").value(DEFAULT_MIN_PRICE.intValue()))
            .andExpect(jsonPath("$.maxPrice").value(DEFAULT_MAX_PRICE.intValue()))
            .andExpect(jsonPath("$.minAmount").value(DEFAULT_MIN_AMOUNT.intValue()))
            .andExpect(jsonPath("$.maxAmount").value(DEFAULT_MAX_AMOUNT.intValue()))
            .andExpect(jsonPath("$.minShare").value(DEFAULT_MIN_SHARE.intValue()))
            .andExpect(jsonPath("$.startDate").value(sameInstant(DEFAULT_START_DATE)))
            .andExpect(jsonPath("$.endDate").value(sameInstant(DEFAULT_END_DATE)));
    }

    @Test
    @Transactional
    public void getNonExistingPromocodeSale() throws Exception {
        // Get the promocodeSale
        restPromocodeSaleMockMvc.perform(get("/api/promocode-sales/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePromocodeSale() throws Exception {
        // Initialize the database
        promocodeSaleService.save(promocodeSale);

        int databaseSizeBeforeUpdate = promocodeSaleRepository.findAll().size();

        // Update the promocodeSale
        PromocodeSale updatedPromocodeSale = promocodeSaleRepository.findById(promocodeSale.getId()).get();
        // Disconnect from session so that the updates on updatedPromocodeSale are not directly saved in db
        em.detach(updatedPromocodeSale);
        updatedPromocodeSale
            .sale(UPDATED_SALE)
            .minPrice(UPDATED_MIN_PRICE)
            .maxPrice(UPDATED_MAX_PRICE)
            .minAmount(UPDATED_MIN_AMOUNT)
            .maxAmount(UPDATED_MAX_AMOUNT)
            .minShare(UPDATED_MIN_SHARE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE);

        restPromocodeSaleMockMvc.perform(put("/api/promocode-sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPromocodeSale)))
            .andExpect(status().isOk());

        // Validate the PromocodeSale in the database
        List<PromocodeSale> promocodeSaleList = promocodeSaleRepository.findAll();
        assertThat(promocodeSaleList).hasSize(databaseSizeBeforeUpdate);
        PromocodeSale testPromocodeSale = promocodeSaleList.get(promocodeSaleList.size() - 1);
        assertThat(testPromocodeSale.getSale()).isEqualTo(UPDATED_SALE);
        assertThat(testPromocodeSale.getMinPrice()).isEqualTo(UPDATED_MIN_PRICE);
        assertThat(testPromocodeSale.getMaxPrice()).isEqualTo(UPDATED_MAX_PRICE);
        assertThat(testPromocodeSale.getMinAmount()).isEqualTo(UPDATED_MIN_AMOUNT);
        assertThat(testPromocodeSale.getMaxAmount()).isEqualTo(UPDATED_MAX_AMOUNT);
        assertThat(testPromocodeSale.getMinShare()).isEqualTo(UPDATED_MIN_SHARE);
        assertThat(testPromocodeSale.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testPromocodeSale.getEndDate()).isEqualTo(UPDATED_END_DATE);
    }

    @Test
    @Transactional
    public void updateNonExistingPromocodeSale() throws Exception {
        int databaseSizeBeforeUpdate = promocodeSaleRepository.findAll().size();

        // Create the PromocodeSale

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPromocodeSaleMockMvc.perform(put("/api/promocode-sales")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(promocodeSale)))
            .andExpect(status().isBadRequest());

        // Validate the PromocodeSale in the database
        List<PromocodeSale> promocodeSaleList = promocodeSaleRepository.findAll();
        assertThat(promocodeSaleList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePromocodeSale() throws Exception {
        // Initialize the database
        promocodeSaleService.save(promocodeSale);

        int databaseSizeBeforeDelete = promocodeSaleRepository.findAll().size();

        // Delete the promocodeSale
        restPromocodeSaleMockMvc.perform(delete("/api/promocode-sales/{id}", promocodeSale.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PromocodeSale> promocodeSaleList = promocodeSaleRepository.findAll();
        assertThat(promocodeSaleList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PromocodeSale.class);
        PromocodeSale promocodeSale1 = new PromocodeSale();
        promocodeSale1.setId(1L);
        PromocodeSale promocodeSale2 = new PromocodeSale();
        promocodeSale2.setId(promocodeSale1.getId());
        assertThat(promocodeSale1).isEqualTo(promocodeSale2);
        promocodeSale2.setId(2L);
        assertThat(promocodeSale1).isNotEqualTo(promocodeSale2);
        promocodeSale1.setId(null);
        assertThat(promocodeSale1).isNotEqualTo(promocodeSale2);
    }
}

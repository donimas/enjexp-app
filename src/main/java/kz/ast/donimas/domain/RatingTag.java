package kz.ast.donimas.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * A RatingTag.
 */
@Entity
@Table(name = "rating_tag")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RatingTag implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "eq", precision = 21, scale = 2)
    private BigDecimal eq;

    @Column(name = "code")
    private String code;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public RatingTag name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getEq() {
        return eq;
    }

    public RatingTag eq(BigDecimal eq) {
        this.eq = eq;
        return this;
    }

    public void setEq(BigDecimal eq) {
        this.eq = eq;
    }

    public String getCode() {
        return code;
    }

    public RatingTag code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RatingTag)) {
            return false;
        }
        return id != null && id.equals(((RatingTag) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "RatingTag{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", eq=" + getEq() +
            ", code='" + getCode() + "'" +
            "}";
    }
}

package kz.ast.donimas.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

import kz.ast.donimas.domain.enumeration.PaymentType;

import kz.ast.donimas.domain.enumeration.ServiceType;

import kz.ast.donimas.domain.enumeration.LocationType;

/**
 * A Order.
 */
@Entity
@Table(name = "jhi_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "promocode")
    private String promocode;

    @Column(name = "total_price", precision = 21, scale = 2)
    private BigDecimal totalPrice;

    @Column(name = "subtotal", precision = 21, scale = 2)
    private BigDecimal subtotalPrice;

    @Column(name = "discount", precision = 21, scale = 2)
    private BigDecimal discount;

    @Enumerated(EnumType.STRING)
    @Column(name = "payment_type")
    private PaymentType paymentType;

    @Column(name = "payment_code")
    private String paymentCode;

    @Column(name = "flag_paid")
    private Boolean flagPaid;

    @Enumerated(EnumType.STRING)
    @Column(name = "service_type")
    private ServiceType serviceType;

    @Column(name = "service_time")
    private ZonedDateTime serviceTime;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "jhi_comment")
    private String comment;

    @Enumerated(EnumType.STRING)
    @Column(name = "location_type")
    private LocationType locationType;

    @Column(name = "delivery_duration", precision = 21, scale = 2)
    private BigDecimal deliveryDuration;

    @ManyToOne
    @JsonIgnoreProperties("orders")
    private User user;

    @ManyToOne
    @JsonIgnoreProperties("orders")
    private OrderStatusHistory status;

    @ManyToOne
    @JsonIgnoreProperties("orders")
    private Location location;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPromocode() {
        return promocode;
    }

    public Order promocode(String promocode) {
        this.promocode = promocode;
        return this;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public Order totalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
        return this;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public Order paymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
        return this;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getPaymentCode() {
        return paymentCode;
    }

    public Order paymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
        return this;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    public Boolean isFlagPaid() {
        return flagPaid;
    }

    public Order flagPaid(Boolean flagPaid) {
        this.flagPaid = flagPaid;
        return this;
    }

    public void setFlagPaid(Boolean flagPaid) {
        this.flagPaid = flagPaid;
    }

    public ServiceType getServiceType() {
        return serviceType;
    }

    public Order serviceType(ServiceType serviceType) {
        this.serviceType = serviceType;
        return this;
    }

    public void setServiceType(ServiceType serviceType) {
        this.serviceType = serviceType;
    }

    public ZonedDateTime getServiceTime() {
        return serviceTime;
    }

    public Order serviceTime(ZonedDateTime serviceTime) {
        this.serviceTime = serviceTime;
        return this;
    }

    public void setServiceTime(ZonedDateTime serviceTime) {
        this.serviceTime = serviceTime;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public Order createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public String getComment() {
        return comment;
    }

    public Order comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public LocationType getLocationType() {
        return locationType;
    }

    public Order locationType(LocationType locationType) {
        this.locationType = locationType;
        return this;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }

    public BigDecimal getDeliveryDuration() {
        return deliveryDuration;
    }

    public Order deliveryDuration(BigDecimal deliveryDuration) {
        this.deliveryDuration = deliveryDuration;
        return this;
    }

    public void setDeliveryDuration(BigDecimal deliveryDuration) {
        this.deliveryDuration = deliveryDuration;
    }

    public User getUser() {
        return user;
    }

    public Order user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderStatusHistory getStatus() {
        return status;
    }

    public Order status(OrderStatusHistory orderStatusHistory) {
        this.status = orderStatusHistory;
        return this;
    }

    public void setStatus(OrderStatusHistory orderStatusHistory) {
        this.status = orderStatusHistory;
    }

    public Location getLocation() {
        return location;
    }

    public Order location(Location location) {
        this.location = location;
        return this;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public BigDecimal getSubtotalPrice() {
        return subtotalPrice;
    }

    public void setSubtotalPrice(BigDecimal subtotalPrice) {
        this.subtotalPrice = subtotalPrice;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Boolean getFlagDeleted() {
        return flagDeleted;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }
// jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Order)) {
            return false;
        }
        return id != null && id.equals(((Order) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Order{" +
            "id=" + getId() +
            ", promocode='" + getPromocode() + "'" +
            ", totalPrice=" + getTotalPrice() +
            ", paymentType='" + getPaymentType() + "'" +
            ", paymentCode='" + getPaymentCode() + "'" +
            ", flagPaid='" + isFlagPaid() + "'" +
            ", serviceType='" + getServiceType() + "'" +
            ", serviceTime='" + getServiceTime() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", comment='" + getComment() + "'" +
            ", locationType='" + getLocationType() + "'" +
            ", deliveryDuration=" + getDeliveryDuration() +
            ", flagDeleted=" + getFlagDeleted() +
            "}";
    }
}

package kz.ast.donimas.domain.enumeration;

/**
 * The ServiceType enumeration.
 */
public enum ServiceType {
    PICK_UP, DELIVERY
}

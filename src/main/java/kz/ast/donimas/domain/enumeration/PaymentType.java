package kz.ast.donimas.domain.enumeration;

/**
 * The PaymentType enumeration.
 */
public enum PaymentType {
    CASH, EPAY, KASPI
}

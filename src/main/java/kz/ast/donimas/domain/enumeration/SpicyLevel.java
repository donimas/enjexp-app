package kz.ast.donimas.domain.enumeration;

public enum SpicyLevel {
    NONE, LOW, MIDDLE, HIGH
}

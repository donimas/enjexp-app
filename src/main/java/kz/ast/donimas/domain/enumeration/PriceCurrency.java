package kz.ast.donimas.domain.enumeration;

/**
 * The PriceCurrency enumeration.
 */
public enum PriceCurrency {
    KZT, RU, US
}

package kz.ast.donimas.domain.enumeration;

/**
 * The OrderItemType enumeration.
 */
public enum OrderItemType {
    PAY, GIFT
}

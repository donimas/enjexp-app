package kz.ast.donimas.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    NEW, CONFIRMED, IN_PROCESS, FINISHED, DELIVERING, DELIVERED, CANCELED, REJECTED
}

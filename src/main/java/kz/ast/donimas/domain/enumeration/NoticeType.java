package kz.ast.donimas.domain.enumeration;

/**
 * The NoticeType enumeration.
 */
public enum NoticeType {
    INFO, SUCCESS, WARNING, DANGER
}

package kz.ast.donimas.domain.enumeration;

/**
 * The NoticeReceiverType enumeration.
 */
public enum NoticeReceiverType {
    ONE_USER, GROUP, ALL_USERS, ORDER, STOCK
}

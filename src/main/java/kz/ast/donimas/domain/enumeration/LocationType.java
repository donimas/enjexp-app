package kz.ast.donimas.domain.enumeration;

/**
 * The LocationType enumeration.
 */
public enum LocationType {
    KAZGYU, COMMUNITY, CITY
}

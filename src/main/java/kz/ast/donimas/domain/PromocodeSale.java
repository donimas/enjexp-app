package kz.ast.donimas.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A PromocodeSale.
 */
@Entity
@Table(name = "promocode_sale")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PromocodeSale implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "sale", precision = 21, scale = 2)
    private BigDecimal sale;

    @Column(name = "min_price", precision = 21, scale = 2)
    private BigDecimal minPrice;

    @Column(name = "max_price", precision = 21, scale = 2)
    private BigDecimal maxPrice;

    @Column(name = "min_amount", precision = 21, scale = 2)
    private BigDecimal minAmount;

    @Column(name = "max_amount", precision = 21, scale = 2)
    private BigDecimal maxAmount;

    @Column(name = "min_share", precision = 21, scale = 2)
    private BigDecimal minShare;

    @Column(name = "start_date")
    private ZonedDateTime startDate;

    @Column(name = "end_date")
    private ZonedDateTime endDate;

    @ManyToOne
    @JsonIgnoreProperties("promocodeSales")
    private Promocode promocode;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getSale() {
        return sale;
    }

    public PromocodeSale sale(BigDecimal sale) {
        this.sale = sale;
        return this;
    }

    public void setSale(BigDecimal sale) {
        this.sale = sale;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public PromocodeSale minPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
        return this;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public PromocodeSale maxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
        return this;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public PromocodeSale minAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
        return this;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public PromocodeSale maxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
        return this;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public BigDecimal getMinShare() {
        return minShare;
    }

    public PromocodeSale minShare(BigDecimal minShare) {
        this.minShare = minShare;
        return this;
    }

    public void setMinShare(BigDecimal minShare) {
        this.minShare = minShare;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public PromocodeSale startDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
    }

    public ZonedDateTime getEndDate() {
        return endDate;
    }

    public PromocodeSale endDate(ZonedDateTime endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(ZonedDateTime endDate) {
        this.endDate = endDate;
    }

    public Promocode getPromocode() {
        return promocode;
    }

    public PromocodeSale promocode(Promocode promocode) {
        this.promocode = promocode;
        return this;
    }

    public void setPromocode(Promocode promocode) {
        this.promocode = promocode;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PromocodeSale)) {
            return false;
        }
        return id != null && id.equals(((PromocodeSale) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "PromocodeSale{" +
            "id=" + getId() +
            ", sale=" + getSale() +
            ", minPrice=" + getMinPrice() +
            ", maxPrice=" + getMaxPrice() +
            ", minAmount=" + getMinAmount() +
            ", maxAmount=" + getMaxAmount() +
            ", minShare=" + getMinShare() +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }
}

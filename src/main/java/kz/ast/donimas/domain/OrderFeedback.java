package kz.ast.donimas.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * A OrderFeedback.
 */
@Entity
@Table(name = "order_feedback")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class OrderFeedback implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "rating", precision = 21, scale = 2)
    private BigDecimal rating;

    @Column(name = "jhi_comment", columnDefinition = "TEXT")
    private String comment;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties("orderFeedbacks")
    private Order order;

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "order_feedback_rating_tag",
               joinColumns = @JoinColumn(name = "order_feedback_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "rating_tag_id", referencedColumnName = "id"))
    private Set<RatingTag> ratingTags = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getRating() {
        return rating;
    }

    public OrderFeedback rating(BigDecimal rating) {
        this.rating = rating;
        return this;
    }

    public void setRating(BigDecimal rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public OrderFeedback comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public OrderFeedback createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public Order getOrder() {
        return order;
    }

    public OrderFeedback order(Order order) {
        this.order = order;
        return this;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Set<RatingTag> getRatingTags() {
        return ratingTags;
    }

    public OrderFeedback ratingTags(Set<RatingTag> ratingTags) {
        this.ratingTags = ratingTags;
        return this;
    }

    public OrderFeedback addRatingTag(RatingTag ratingTag) {
        this.ratingTags.add(ratingTag);
        //ratingTag.getOrderFeedbacks().add(this);
        return this;
    }

    public OrderFeedback removeRatingTag(RatingTag ratingTag) {
        this.ratingTags.remove(ratingTag);
        //ratingTag.getOrderFeedbacks().remove(this);
        return this;
    }

    public void setRatingTags(Set<RatingTag> ratingTags) {
        this.ratingTags = ratingTags;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OrderFeedback)) {
            return false;
        }
        return id != null && id.equals(((OrderFeedback) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "OrderFeedback{" +
            "id=" + getId() +
            ", rating=" + getRating() +
            ", comment='" + getComment() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            "}";
    }
}

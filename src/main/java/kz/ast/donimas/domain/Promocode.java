package kz.ast.donimas.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A Promocode.
 */
@Entity
@Table(name = "promocode")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Promocode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "code")
    private String code;

    @Column(name = "flag_active")
    private Boolean flagActive;

    @Column(name = "name")
    private String name;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "container_class")
    private String containerClass;

    @Column(name = "container_id")
    private Long containerId;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @ManyToOne
    @JsonIgnoreProperties("promocodes")
    private User user;

    @Column(name = "owner_count")
    private BigDecimal usedByOwnerCount;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Promocode code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean isFlagActive() {
        return flagActive;
    }

    public Promocode flagActive(Boolean flagActive) {
        this.flagActive = flagActive;
        return this;
    }

    public void setFlagActive(Boolean flagActive) {
        this.flagActive = flagActive;
    }

    public String getName() {
        return name;
    }

    public Promocode name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Promocode description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContainerClass() {
        return containerClass;
    }

    public Promocode containerClass(String containerClass) {
        this.containerClass = containerClass;
        return this;
    }

    public void setContainerClass(String containerClass) {
        this.containerClass = containerClass;
    }

    public Long getContainerId() {
        return containerId;
    }

    public Promocode containerId(Long containerId) {
        this.containerId = containerId;
        return this;
    }

    public void setContainerId(Long containerId) {
        this.containerId = containerId;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public Promocode createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public User getUser() {
        return user;
    }

    public Promocode user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public BigDecimal getUsedByOwnerCount() {
        return usedByOwnerCount;
    }

    public void setUsedByOwnerCount(BigDecimal usedByOwnerCount) {
        this.usedByOwnerCount = usedByOwnerCount;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Promocode)) {
            return false;
        }
        return id != null && id.equals(((Promocode) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Promocode{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", flagActive='" + isFlagActive() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", containerClass='" + getContainerClass() + "'" +
            ", containerId=" + getContainerId() +
            ", createDate='" + getCreateDate() + "'" +
            ", userByOwnerCount='" + getUsedByOwnerCount() + "'" +
            "}";
    }
}

package kz.ast.donimas.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

import kz.ast.donimas.domain.enumeration.NoticeReceiverType;

import kz.ast.donimas.domain.enumeration.NoticeType;

/**
 * A Notice.
 */
@Entity
@Table(name = "notice")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Notice implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "content", columnDefinition = "TEXT")
    private String content;

    @Column(name = "sms_content")
    private String smsContent;

    @Column(name = "flag_push")
    private Boolean flagPush;

    @Column(name = "flag_sms")
    private Boolean flagSms;

    @Column(name = "flag_email")
    private Boolean flagEmail;

    @Enumerated(EnumType.STRING)
    @Column(name = "receiver_type")
    private NoticeReceiverType receiverType;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "modify_date")
    private ZonedDateTime modifyDate;

    @Column(name = "container_class")
    private String containerClass;

    @Column(name = "container_id")
    private Long containerId;

    @Enumerated(EnumType.STRING)
    @Column(name = "notice_type")
    private NoticeType noticeType;

    @Column(name = "flag_sent")
    private Boolean flagSent;

    @Column(name = "sent_time")
    private ZonedDateTime sentTime;

    @Column(name = "payload_object_url")
    private String payloadObjectUrl;

    @Column(name = "payload_object_id")
    private Long payloadObjectId;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Notice title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public Notice content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSmsContent() {
        return smsContent;
    }

    public Notice smsContent(String smsContent) {
        this.smsContent = smsContent;
        return this;
    }

    public void setSmsContent(String smsContent) {
        this.smsContent = smsContent;
    }

    public Boolean isFlagPush() {
        return flagPush;
    }

    public Notice flagPush(Boolean flagPush) {
        this.flagPush = flagPush;
        return this;
    }

    public void setFlagPush(Boolean flagPush) {
        this.flagPush = flagPush;
    }

    public Boolean isFlagSms() {
        return flagSms;
    }

    public Notice flagSms(Boolean flagSms) {
        this.flagSms = flagSms;
        return this;
    }

    public void setFlagSms(Boolean flagSms) {
        this.flagSms = flagSms;
    }

    public Boolean isFlagEmail() {
        return flagEmail;
    }

    public Notice flagEmail(Boolean flagEmail) {
        this.flagEmail = flagEmail;
        return this;
    }

    public void setFlagEmail(Boolean flagEmail) {
        this.flagEmail = flagEmail;
    }

    public NoticeReceiverType getReceiverType() {
        return receiverType;
    }

    public Notice receiverType(NoticeReceiverType receiverType) {
        this.receiverType = receiverType;
        return this;
    }

    public void setReceiverType(NoticeReceiverType receiverType) {
        this.receiverType = receiverType;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public Notice createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getModifyDate() {
        return modifyDate;
    }

    public Notice modifyDate(ZonedDateTime modifyDate) {
        this.modifyDate = modifyDate;
        return this;
    }

    public void setModifyDate(ZonedDateTime modifyDate) {
        this.modifyDate = modifyDate;
    }

    public String getContainerClass() {
        return containerClass;
    }

    public Notice containerClass(String containerClass) {
        this.containerClass = containerClass;
        return this;
    }

    public void setContainerClass(String containerClass) {
        this.containerClass = containerClass;
    }

    public Long getContainerId() {
        return containerId;
    }

    public Notice containerId(Long containerId) {
        this.containerId = containerId;
        return this;
    }

    public void setContainerId(Long containerId) {
        this.containerId = containerId;
    }

    public NoticeType getNoticeType() {
        return noticeType;
    }

    public Notice noticeType(NoticeType noticeType) {
        this.noticeType = noticeType;
        return this;
    }

    public void setNoticeType(NoticeType noticeType) {
        this.noticeType = noticeType;
    }

    public Boolean isFlagSent() {
        return flagSent;
    }

    public Notice flagSent(Boolean flagSent) {
        this.flagSent = flagSent;
        return this;
    }

    public void setFlagSent(Boolean flagSent) {
        this.flagSent = flagSent;
    }

    public ZonedDateTime getSentTime() {
        return sentTime;
    }

    public Notice sentTime(ZonedDateTime sentTime) {
        this.sentTime = sentTime;
        return this;
    }

    public void setSentTime(ZonedDateTime sentTime) {
        this.sentTime = sentTime;
    }

    public String getPayloadObjectUrl() {
        return payloadObjectUrl;
    }

    public Notice payloadObjectUrl(String payloadObjectUrl) {
        this.payloadObjectUrl = payloadObjectUrl;
        return this;
    }

    public void setPayloadObjectUrl(String payloadObjectUrl) {
        this.payloadObjectUrl = payloadObjectUrl;
    }

    public Long getPayloadObjectId() {
        return payloadObjectId;
    }

    public Notice payloadObjectId(Long payloadObjectId) {
        this.payloadObjectId = payloadObjectId;
        return this;
    }

    public void setPayloadObjectId(Long payloadObjectId) {
        this.payloadObjectId = payloadObjectId;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public Notice flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Notice)) {
            return false;
        }
        return id != null && id.equals(((Notice) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Notice{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", content='" + getContent() + "'" +
            ", smsContent='" + getSmsContent() + "'" +
            ", flagPush='" + isFlagPush() + "'" +
            ", flagSms='" + isFlagSms() + "'" +
            ", flagEmail='" + isFlagEmail() + "'" +
            ", receiverType='" + getReceiverType() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", modifyDate='" + getModifyDate() + "'" +
            ", containerClass='" + getContainerClass() + "'" +
            ", containerId=" + getContainerId() +
            ", noticeType='" + getNoticeType() + "'" +
            ", flagSent='" + isFlagSent() + "'" +
            ", sentTime='" + getSentTime() + "'" +
            ", payloadObjectUrl='" + getPayloadObjectUrl() + "'" +
            ", payloadObjectId=" + getPayloadObjectId() +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            "}";
    }
}

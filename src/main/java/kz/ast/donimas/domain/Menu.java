package kz.ast.donimas.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import kz.ast.donimas.domain.enumeration.SpicyLevel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.ZonedDateTime;

/**
 * A Menu.
 */
@Entity
@Table(name = "menu")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Lob
    @Column(name = "image")
    private byte[] image;

    @Column(name = "image_content_type")
    private String imageContentType;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "flag_deleted")
    private Boolean flagDeleted;

    @Column(name = "content", columnDefinition = "TEXT")
    private String content;

    @Column(name = "prep_duration", precision = 21, scale = 2)
    private BigDecimal prepDuration;

    @ManyToOne
    @JsonIgnoreProperties("menus")
    private Category category;

    @ManyToOne
    @JsonIgnoreProperties("menus")
    private PriceHistory price;

    @ManyToOne
    @JsonIgnoreProperties("menus")
    private Menu combo;

    @Column(name = "on_stop")
    private Boolean onStop;

    @Column(name = "spicy")
    private Boolean spicy;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Menu name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Menu description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte[] getImage() {
        return image;
    }

    public Menu image(byte[] image) {
        this.image = image;
        return this;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getImageContentType() {
        return imageContentType;
    }

    public Menu imageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
        return this;
    }

    public void setImageContentType(String imageContentType) {
        this.imageContentType = imageContentType;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public Menu createDate(ZonedDateTime createDate) {
        this.createDate = createDate;
        return this;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public Boolean isFlagDeleted() {
        return flagDeleted;
    }

    public Menu flagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
        return this;
    }

    public void setFlagDeleted(Boolean flagDeleted) {
        this.flagDeleted = flagDeleted;
    }

    public String getContent() {
        return content;
    }

    public Menu content(String content) {
        this.content = content;
        return this;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public BigDecimal getPrepDuration() {
        return prepDuration;
    }

    public Menu prepDuration(BigDecimal prepDuration) {
        this.prepDuration = prepDuration;
        return this;
    }

    public void setPrepDuration(BigDecimal prepDuration) {
        this.prepDuration = prepDuration;
    }

    public Category getCategory() {
        return category;
    }

    public Menu category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public PriceHistory getPrice() {
        return price;
    }

    public Menu price(PriceHistory priceHistory) {
        this.price = priceHistory;
        return this;
    }

    public void setPrice(PriceHistory priceHistory) {
        this.price = priceHistory;
    }

    public Menu getCombo() {
        return combo;
    }

    public Menu combo(Menu menu) {
        this.combo = menu;
        return this;
    }

    public void setCombo(Menu menu) {
        this.combo = menu;
    }

    public Boolean getOnStop() {
        return onStop;
    }

    public void setOnStop(Boolean onStop) {
        this.onStop = onStop;
    }

    public Boolean getSpicy() {
        return spicy;
    }

    public void setSpicy(Boolean spicy) {
        this.spicy = spicy;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Menu)) {
            return false;
        }
        return id != null && id.equals(((Menu) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Menu{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            ", imageContentType='" + getImageContentType() + "'" +
            ", createDate='" + getCreateDate() + "'" +
            ", flagDeleted='" + isFlagDeleted() + "'" +
            ", content='" + getContent() + "'" +
            ", prepDuration=" + getPrepDuration() +
            ", onStop=" + getOnStop() +
            ", spicy=" + getSpicy() +
            "}";
    }
}

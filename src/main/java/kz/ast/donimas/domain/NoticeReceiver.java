package kz.ast.donimas.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A NoticeReceiver.
 */
@Entity
@Table(name = "notice_receiver")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class NoticeReceiver implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "to_user_id")
    private Long toUserId;

    @Column(name = "user_lang")
    private String userLang;

    @Column(name = "flag_sms_sent")
    private Boolean flagSmsSent;

    @Column(name = "flag_email_sent")
    private Boolean flagEmailSent;

    @Column(name = "flag_push_sent")
    private Boolean flagPushSent;

    @Column(name = "flag_read")
    private Boolean flagRead;

    @Column(name = "read_date")
    private ZonedDateTime readDate;

    @Column(name = "receiver_full_name")
    private String receiverFullName;

    @Column(name = "receiver_phone")
    private String receiverPhone;

    @ManyToOne
    @JsonIgnoreProperties("noticeReceivers")
    private Notice notice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public NoticeReceiver toUserId(Long toUserId) {
        this.toUserId = toUserId;
        return this;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    public String getUserLang() {
        return userLang;
    }

    public NoticeReceiver userLang(String userLang) {
        this.userLang = userLang;
        return this;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public Boolean isFlagSmsSent() {
        return flagSmsSent;
    }

    public NoticeReceiver flagSmsSent(Boolean flagSmsSent) {
        this.flagSmsSent = flagSmsSent;
        return this;
    }

    public void setFlagSmsSent(Boolean flagSmsSent) {
        this.flagSmsSent = flagSmsSent;
    }

    public Boolean isFlagEmailSent() {
        return flagEmailSent;
    }

    public NoticeReceiver flagEmailSent(Boolean flagEmailSent) {
        this.flagEmailSent = flagEmailSent;
        return this;
    }

    public void setFlagEmailSent(Boolean flagEmailSent) {
        this.flagEmailSent = flagEmailSent;
    }

    public Boolean isFlagPushSent() {
        return flagPushSent;
    }

    public NoticeReceiver flagPushSent(Boolean flagPushSent) {
        this.flagPushSent = flagPushSent;
        return this;
    }

    public void setFlagPushSent(Boolean flagPushSent) {
        this.flagPushSent = flagPushSent;
    }

    public Boolean isFlagRead() {
        return flagRead;
    }

    public NoticeReceiver flagRead(Boolean flagRead) {
        this.flagRead = flagRead;
        return this;
    }

    public void setFlagRead(Boolean flagRead) {
        this.flagRead = flagRead;
    }

    public ZonedDateTime getReadDate() {
        return readDate;
    }

    public NoticeReceiver readDate(ZonedDateTime readDate) {
        this.readDate = readDate;
        return this;
    }

    public void setReadDate(ZonedDateTime readDate) {
        this.readDate = readDate;
    }

    public String getReceiverFullName() {
        return receiverFullName;
    }

    public NoticeReceiver receiverFullName(String receiverFullName) {
        this.receiverFullName = receiverFullName;
        return this;
    }

    public void setReceiverFullName(String receiverFullName) {
        this.receiverFullName = receiverFullName;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public NoticeReceiver receiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
        return this;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public Notice getNotice() {
        return notice;
    }

    public NoticeReceiver notice(Notice notice) {
        this.notice = notice;
        return this;
    }

    public void setNotice(Notice notice) {
        this.notice = notice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof NoticeReceiver)) {
            return false;
        }
        return id != null && id.equals(((NoticeReceiver) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "NoticeReceiver{" +
            "id=" + getId() +
            ", toUserId=" + getToUserId() +
            ", userLang='" + getUserLang() + "'" +
            ", flagSmsSent='" + isFlagSmsSent() + "'" +
            ", flagEmailSent='" + isFlagEmailSent() + "'" +
            ", flagPushSent='" + isFlagPushSent() + "'" +
            ", flagRead='" + isFlagRead() + "'" +
            ", readDate='" + getReadDate() + "'" +
            ", receiverFullName='" + getReceiverFullName() + "'" +
            ", receiverPhone='" + getReceiverPhone() + "'" +
            "}";
    }
}

package kz.ast.donimas.client.sms;

import kz.ast.donimas.service.dto.SmsRequestDto;
import kz.ast.donimas.service.dto.SmsResponseDto;
import kz.ast.donimas.service.util.HttpUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.util.UriEncoder;

import java.util.regex.Pattern;

@Component
public class SmsClient {

    private final Logger log = LoggerFactory.getLogger(SmsClient.class);
    private Pattern PHONE = Pattern.compile("\\d{11}");
    @Value("${mobizon.apiKey}")
    private String apiKey;

    public SmsResponseDto send(SmsRequestDto dto) {
        return send(dto.apiKey, dto.phone, dto.message);
    }

    public SmsResponseDto send(String apiKey, String phone, String message) {
        SmsResponseDto dto = new SmsResponseDto();
        try {
            if (!PHONE.matcher(phone).matches()) {
                throw new RuntimeException("Incorrect phone: '" + phone + "'");
            }
            if (apiKey == null || apiKey.trim().isEmpty()) {
                apiKey = this.apiKey;
            }
            String requestUrl = "https://api.mobizon.com/service/message/sendsmsmessage" +
                    "?apiKey=" + apiKey +
                    "&recipient=" + phone +
                    "&text=" + UriEncoder.encode(message);
            log.debug("request url: " + requestUrl);
            String response = HttpUtils.requestGet(requestUrl);
            log.debug("response: " + response);
//            String response = "{\"code\":0,\"data\":{\"campaignId\":\"1670807\",\"status\":1,\"messageId\":\"11768597\"},\"message\":\"\"}";
//            String response = "{\"code\":1,\"data\":{\"campaignId\":\"1670807\",\"status\":1,\"messageId\":\"11768597\"},\"message\":\"\"}";
            JSONObject obj = new JSONObject(response);
            dto.code = obj.getInt("code");
            dto.responseText = StringEscapeUtils.unescapeJava(response);
            return dto;
        } catch (Exception e) {
            dto.code = -1;
            dto.responseText = e.getMessage();
            return dto;
        }
    }

}

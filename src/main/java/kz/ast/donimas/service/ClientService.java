package kz.ast.donimas.service;

import kz.ast.donimas.config.Constants;
import kz.ast.donimas.domain.User;
import kz.ast.donimas.service.dto.PromocodeConfirmDto;
import kz.ast.donimas.web.rest.errors.FLCException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

@Service
@Transactional
public class ClientService {

    private final Logger log = LoggerFactory.getLogger(ClientService.class);

    private final UserService userService;
    private final OrderService orderService;
    private final PromocodeService promocodeService;

    public ClientService(UserService userService, OrderService orderService, PromocodeService promocodeService) {
        this.userService = userService;
        this.orderService = orderService;
        this.promocodeService = promocodeService;
    }

    public PromocodeConfirmDto checkPromoCode(String code) {
        log.debug("Request to check promo code: {}", code);
        Optional<User> optUser = this.userService.getUserWithAuthorities();
        if(!optUser.isPresent()) {
            throw new FLCException("User not found");
        }
        User currentUser = optUser.get();
        Long count = orderService.countUsedPromocodeForCurrentUser(code, currentUser);
        if(count > 0) {
            throw new FLCException("Вы уже использовали Приветственный Промо Код");
        }
        if(promocodeService.countByCode(code) == 0) {
            throw new FLCException("Промо акция не найдена с таким кодом");
        }

        PromocodeConfirmDto result = new PromocodeConfirmDto();
        result.code = code;
        result.login = currentUser.getLogin();
        result.confirmed = true;
        result.discount = new BigDecimal(Constants.HELLO_PROMOCODE_DISCOUNT);

        return result;
    }

}

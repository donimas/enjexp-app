package kz.ast.donimas.service;

import kz.ast.donimas.domain.NoticeReceiver;
import kz.ast.donimas.repository.NoticeReceiverRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link NoticeReceiver}.
 */
@Service
@Transactional
public class NoticeReceiverService {

    private final Logger log = LoggerFactory.getLogger(NoticeReceiverService.class);

    private final NoticeReceiverRepository noticeReceiverRepository;

    public NoticeReceiverService(NoticeReceiverRepository noticeReceiverRepository) {
        this.noticeReceiverRepository = noticeReceiverRepository;
    }

    /**
     * Save a noticeReceiver.
     *
     * @param noticeReceiver the entity to save.
     * @return the persisted entity.
     */
    public NoticeReceiver save(NoticeReceiver noticeReceiver) {
        log.debug("Request to save NoticeReceiver : {}", noticeReceiver);
        return noticeReceiverRepository.save(noticeReceiver);
    }

    /**
     * Get all the noticeReceivers.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<NoticeReceiver> findAll(Pageable pageable) {
        log.debug("Request to get all NoticeReceivers");
        return noticeReceiverRepository.findAll(pageable);
    }


    /**
     * Get one noticeReceiver by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<NoticeReceiver> findOne(Long id) {
        log.debug("Request to get NoticeReceiver : {}", id);
        return noticeReceiverRepository.findById(id);
    }

    /**
     * Delete the noticeReceiver by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete NoticeReceiver : {}", id);
        noticeReceiverRepository.deleteById(id);
    }
}

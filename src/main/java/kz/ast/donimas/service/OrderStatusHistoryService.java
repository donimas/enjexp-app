package kz.ast.donimas.service;

import kz.ast.donimas.domain.Order;
import kz.ast.donimas.domain.OrderStatusHistory;
import kz.ast.donimas.domain.enumeration.OrderStatus;
import kz.ast.donimas.repository.OrderStatusHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderStatusHistory}.
 */
@Service
@Transactional
public class OrderStatusHistoryService {

    private final Logger log = LoggerFactory.getLogger(OrderStatusHistoryService.class);

    private final OrderStatusHistoryRepository orderStatusHistoryRepository;

    public OrderStatusHistoryService(OrderStatusHistoryRepository orderStatusHistoryRepository) {
        this.orderStatusHistoryRepository = orderStatusHistoryRepository;
    }

    /**
     * Save a orderStatusHistory.
     *
     * @param orderStatusHistory the entity to save.
     * @return the persisted entity.
     */
    public OrderStatusHistory save(OrderStatusHistory orderStatusHistory) {
        log.debug("Request to save OrderStatusHistory : {}", orderStatusHistory);
        return orderStatusHistoryRepository.save(orderStatusHistory);
    }

    /**
     * Get all the orderStatusHistories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OrderStatusHistory> findAll(Pageable pageable) {
        log.debug("Request to get all OrderStatusHistories");
        return orderStatusHistoryRepository.findAll(pageable);
    }


    /**
     * Get one orderStatusHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OrderStatusHistory> findOne(Long id) {
        log.debug("Request to get OrderStatusHistory : {}", id);
        return orderStatusHistoryRepository.findById(id);
    }

    /**
     * Delete the orderStatusHistory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OrderStatusHistory : {}", id);
        orderStatusHistoryRepository.deleteById(id);
    }

    public OrderStatusHistory create(Order order, OrderStatus status, String comment) {
        log.debug("Request to create order status -> {} -> {} -> {}", order.getId(), status, comment);
        OrderStatusHistory result = new OrderStatusHistory();
        result.setCreateDate(ZonedDateTime.now(ZoneId.systemDefault()));
        result.setOrder(order);
        result.setOrderStatus(status);
        result.setComment(comment);

        return save(result);
    }
}

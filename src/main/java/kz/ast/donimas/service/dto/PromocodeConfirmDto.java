package kz.ast.donimas.service.dto;

import java.math.BigDecimal;

public class PromocodeConfirmDto {

    public String login;
    public String code;
    public boolean confirmed;
    public String note;
    public BigDecimal discount;
    public BigDecimal usedByOwnerCount;
    public BigDecimal totalUsedCount;

    public PromocodeConfirmDto() {

    }
    public PromocodeConfirmDto(int count, String code) {
        this.usedByOwnerCount = new BigDecimal(count);
        this.code = code;
    }

}

package kz.ast.donimas.service.dto;

public class SmsResponseDto {
    public int code;
    public String responseText;

    @Override
    public String toString() {
        return "SmsResponseDto{" +
                "code=" + code +
                ", responseText='" + responseText + '\'' +
                '}';
    }
}

package kz.ast.donimas.service.dto;

public class SmsRequestDto {
    public String apiKey;
    public String phone;
    public String message;
}

package kz.ast.donimas.service.dto;

import kz.ast.donimas.domain.Category;
import kz.ast.donimas.domain.Menu;
import kz.ast.donimas.domain.PriceHistory;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public class MenuDto {

    public Long id;
    public String name;
    public String description;
    public byte[] image;
    public String imageContentType;
    public ZonedDateTime createDate;
    public Boolean flagDeleted;
    public String content;
    public BigDecimal prepDuration;
    public Category category;
    public PriceHistory price;
    public MenuDto combo;
    public boolean spicy;

    public MenuDto() {}

    public MenuDto(Menu menu) {
        if(menu != null) {
            this.id = menu.getId();
            this.name = menu.getName();
            this.description = menu.getDescription();
            this.image = menu.getImage();
            this.imageContentType = menu.getImageContentType();
            this.createDate = menu.getCreateDate();
            this.flagDeleted = menu.isFlagDeleted();
            this.content = menu.getContent();
            this.prepDuration = menu.getPrepDuration();
            this.category = menu.getCategory();
            this.price = menu.getPrice();
            this.combo = new MenuDto(menu.getCombo());
            if(menu.getSpicy() != null) {
                this.spicy = menu.getSpicy();
            }
        }
    }

    @Override
    public String toString() {
        return "MenuDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}

package kz.ast.donimas.service.dto;

import kz.ast.donimas.domain.OrderItem;
import kz.ast.donimas.domain.enumeration.SpicyLevel;

import java.math.BigDecimal;

public class CheckoutItemDto {

    public Long id;
    public BigDecimal amount;
    public MenuDto menu;
    public SpicyLevel spicyLevel;

    public CheckoutItemDto() {}

    public CheckoutItemDto(OrderItem orderItem) {
        id = orderItem.getId();
        amount = orderItem.getAmount();
        spicyLevel = orderItem.getSpicyLevel();
    }

    @Override
    public String toString() {
        return "CheckoutItemDto{" +
                "amount=" + amount +
                ", menu=" + menu +
                ", spicyLevel=" + spicyLevel +
                '}';
    }
}

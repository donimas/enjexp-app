package kz.ast.donimas.service.dto;

import kz.ast.donimas.domain.Order;
import kz.ast.donimas.domain.OrderStatusHistory;
import kz.ast.donimas.domain.enumeration.PaymentType;
import kz.ast.donimas.domain.enumeration.ServiceType;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class CheckoutDto {

    public Long id;
    public ZonedDateTime pickupTime;
    public String login;
    public String promocode;
    public PaymentType paymentType;
    public BigDecimal subtotal;
    public BigDecimal discount;
    public BigDecimal total;
    public List<CheckoutItemDto> items = new ArrayList<>();
    public String comment;
    public ServiceType serviceType;
    public OrderStatusHistory statusHistory;

    public CheckoutDto() {}

    public CheckoutDto(Order order) {
        id = order.getId();
        pickupTime = order.getServiceTime();
        login = order.getUser().getLogin();
        promocode = order.getPromocode();
        paymentType = order.getPaymentType();
        subtotal = order.getSubtotalPrice();
        total = order.getTotalPrice();
        discount = order.getDiscount();
        comment = order.getComment();
        serviceType = order.getServiceType();
        statusHistory = order.getStatus();
    }

    @Override
    public String toString() {
        return "CheckoutDto{" +
                "pickupTime=" + pickupTime +
                ", login='" + login + '\'' +
                ", promocode='" + promocode + '\'' +
                ", paymentType=" + paymentType +
                ", subtotal=" + subtotal +
                ", discount=" + discount +
                ", total=" + total +
                ", items=" + items +
                '}';
    }
}

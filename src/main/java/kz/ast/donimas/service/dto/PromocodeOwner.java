package kz.ast.donimas.service.dto;

import java.math.BigDecimal;

public class PromocodeOwner {

    public PromocodeOwner() {}

    public PromocodeOwner(int conditionalAmount, String code) {
        this.conditionalAmount = new BigDecimal(conditionalAmount);
        this.code = code;
    }

    public BigDecimal conditionalAmount;
    public BigDecimal factAmount;
    public boolean filled;
    public String login;
    public String note;
    public String code;

}

package kz.ast.donimas.service;

import kz.ast.donimas.domain.Order;
import kz.ast.donimas.domain.OrderItem;
import kz.ast.donimas.domain.OrderStatusHistory;
import kz.ast.donimas.domain.User;
import kz.ast.donimas.domain.enumeration.OrderStatus;
import kz.ast.donimas.repository.OrderRepository;
import kz.ast.donimas.security.SecurityUtils;
import kz.ast.donimas.service.dto.CheckoutDto;
import kz.ast.donimas.service.dto.CheckoutItemDto;
import kz.ast.donimas.service.dto.PromocodeConfirmDto;
import kz.ast.donimas.web.rest.errors.FLCException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Order}.
 */
@Service
@Transactional
public class OrderService {

    private final Logger log = LoggerFactory.getLogger(OrderService.class);

    private final OrderRepository orderRepository;
    private final UserService userService;
    private final OrderItemService orderItemService;
    private final OrderStatusHistoryService orderStatusHistoryService;

    public OrderService(OrderRepository orderRepository, UserService userService, OrderItemService orderItemService, OrderStatusHistoryService orderStatusHistoryService) {
        this.orderRepository = orderRepository;
        this.userService = userService;
        this.orderItemService = orderItemService;
        this.orderStatusHistoryService = orderStatusHistoryService;
    }

    /**
     * Save a order.
     *
     * @param order the entity to save.
     * @return the persisted entity.
     */
    public Order save(Order order) {
        log.debug("Request to save Order : {}", order);
        return orderRepository.save(order);
    }

    /**
     * Get all the orders.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CheckoutDto> findAll(Pageable pageable) {
        log.debug("Request to get all Orders");
        List<OrderStatus> orderStatuses = new ArrayList<>();
        orderStatuses.add(OrderStatus.NEW);
        orderStatuses.add(OrderStatus.CONFIRMED);
        orderStatuses.add(OrderStatus.IN_PROCESS);
        orderStatuses.add(OrderStatus.FINISHED);
        orderStatuses.add(OrderStatus.DELIVERING);
        orderStatuses.add(OrderStatus.CANCELED);
        //return orderRepository.findAll(pageable);
        List<Order> orders = orderRepository.findAllByFlagDeletedFalseAndStatus_OrderStatusIn(orderStatuses);
        if(orders == null || orders.isEmpty()) {
            return null;
        }

        return orders.stream().map(this::toCheckoutDto).collect(Collectors.toList());
    }


    /**
     * Get one order by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Order> findOne(Long id) {
        log.debug("Request to get Order : {}", id);
        return orderRepository.findById(id);
    }

    /**
     * Delete the order by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Order : {}", id);
        Optional<Order> orderOpt = findOne(id);
        if(orderOpt.isPresent()) {
            Order order = orderOpt.get();
            order.setFlagDeleted(true);
            save(order);
        }
    }

    public Long countUsedPromocode(String promocode, String currentLogin) {
        log.debug("Request to count user promos: {} -> {}", promocode, currentLogin);
        return orderRepository.countAllByPromocodeAndUser_LoginNotAndStatus_OrderStatus(promocode, currentLogin, OrderStatus.DELIVERED);
    }

    public Long countUsedPromocodeForCurrentUser(String promocode, User user) {
        log.debug("Request to count user promocode for current user: {} - {}", promocode, user.getLogin());
        return orderRepository.countAllByPromocodeIsNotNullAndUserAndStatus_OrderStatus(user, OrderStatus.DELIVERED);
    }

    public Order create(CheckoutDto checkoutDto, PromocodeConfirmDto promo) {
        log.debug("Request to create checkout client: {}", checkoutDto);
        Optional<String> optLogin = SecurityUtils.getCurrentUserLogin();
        if(!optLogin.isPresent()) {
            throw new FLCException("Пользователь не найден");
        }
        Optional<User> optUser = userService.getUserWithAuthoritiesByLogin(optLogin.get());
        if(!optUser.isPresent()) {
            throw new FLCException("Пользователь не найден");
        }
        Order order = new Order();
        order.setCreateDate(ZonedDateTime.now(ZoneId.systemDefault()));
        order.setUser(optUser.get());
        order.setComment(checkoutDto.comment);
        order.setDiscount(checkoutDto.discount);
        order.setSubtotalPrice(checkoutDto.subtotal);
        order.setTotalPrice(checkoutDto.total);
        order.setFlagPaid(false);
        order.setFlagDeleted(false);
        order.setPaymentType(checkoutDto.paymentType);
        order.setServiceType(checkoutDto.serviceType);
        order.setServiceTime(checkoutDto.pickupTime);
        if(promo != null) {
            order.setPromocode(promo.code);
            order.setDiscount(promo.discount);
        }

        save(order);
        log.debug("Order saved: {}", order);

        // Creating order item
        checkoutDto.items.forEach((checkoutItemDto) -> {
            log.debug("item -> {}", checkoutItemDto);
            OrderItem orderItem = orderItemService.create(checkoutItemDto, order);
            log.debug("Order item created -> {}", orderItem);
        });

        // creating order status
        OrderStatusHistory status = orderStatusHistoryService.create(order, OrderStatus.CONFIRMED, null);
        log.debug("Status created -> {}", status);
        order.setStatus(status);

        return save(order);
    }

    public List<Order> findAllOfUser(User user) {
        log.debug("Request to find all of user: {}", user.getLogin());
        List<OrderStatus> statuses = new ArrayList<>();
        statuses.add(OrderStatus.NEW);
        statuses.add(OrderStatus.CONFIRMED);
        statuses.add(OrderStatus.IN_PROCESS);
        statuses.add(OrderStatus.FINISHED);
        statuses.add(OrderStatus.DELIVERING);
        statuses.add(OrderStatus.REJECTED);

        return orderRepository.findAllOrderOfUser(user, statuses);
    }

    public CheckoutDto toCheckoutDto(Order order) {
        log.debug("Request to convert to checkout dto -> {}", order);
        if(order == null) {
            throw new FLCException("Order is null");
        }
        CheckoutDto result = new CheckoutDto(order);
        List<OrderItem> orderItems = orderItemService.findAllByOrder(order);
        result.items =
                orderItems.stream().map((orderItemService::toCheckoutItemDto)).collect(Collectors.toList());


        return result;
    }

    public Order cancelOrder(Order order) {
        log.debug("Request to cancel order: {}", order);
        OrderStatusHistory statusHistory = orderStatusHistoryService.create(order, OrderStatus.CANCELED, null);
        order.setStatus(statusHistory);
        return save(order);
    }
}

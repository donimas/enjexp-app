package kz.ast.donimas.service;

import kz.ast.donimas.domain.Promocode;
import kz.ast.donimas.domain.User;
import kz.ast.donimas.repository.PromocodeRepository;
import kz.ast.donimas.service.util.RandomUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Promocode}.
 */
@Service
@Transactional
public class PromocodeService {

    private final Logger log = LoggerFactory.getLogger(PromocodeService.class);

    private final PromocodeRepository promocodeRepository;

    public PromocodeService(PromocodeRepository promocodeRepository) {
        this.promocodeRepository = promocodeRepository;
    }

    public Promocode create(User user) {
        log.debug("Request to create PromoCode for user: {}", user.getLogin());
        Promocode promocode = new Promocode();
        String code = RandomUtil.generateSmsCode(4);
        log.debug("Promo code randomly generated: {}", code);
        promocode.setCode(code);
        promocode.setCreateDate(ZonedDateTime.now(ZoneId.systemDefault()));
        promocode.setFlagActive(true);
        promocode.setUser(user);
        promocode.setName("Приветственный Промо Код");
        promocode.setDescription("Поделись промо кодом с 5 друзьями! " +
                "После того как они совершат покупку с этим промо кодом через приложение, получи скидку в 2000 тг!");

        return save(promocode);
    }

    /**
     * Save a promocode.
     *
     * @param promocode the entity to save.
     * @return the persisted entity.
     */
    public Promocode save(Promocode promocode) {
        log.debug("Request to save Promocode : {}", promocode);
        return promocodeRepository.save(promocode);
    }

    /**
     * Get all the promocodes.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Promocode> findAll(Pageable pageable) {
        log.debug("Request to get all Promocodes");
        return promocodeRepository.findAll(pageable);
    }


    /**
     * Get one promocode by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Promocode> findOne(Long id) {
        log.debug("Request to get Promocode : {}", id);
        return promocodeRepository.findById(id);
    }

    /**
     * Delete the promocode by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Promocode : {}", id);
        promocodeRepository.deleteById(id);
    }

    public List<Promocode> findActiveByUser() {
        log.debug("Request to find promo code by login");
        return promocodeRepository.findActiveByUserIsCurrentUser();
    }

    public Long countByCode(String code) {
        log.debug("Request to count by code: {}", code);
        return promocodeRepository.countAllByCode(code);
    }
}

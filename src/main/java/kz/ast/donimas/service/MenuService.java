package kz.ast.donimas.service;

import kz.ast.donimas.domain.Menu;
import kz.ast.donimas.domain.PriceHistory;
import kz.ast.donimas.domain.enumeration.PriceCurrency;
import kz.ast.donimas.repository.MenuRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link Menu}.
 */
@Service
@Transactional
public class MenuService {

    private final Logger log = LoggerFactory.getLogger(MenuService.class);

    private final MenuRepository menuRepository;
    private final PriceHistoryService priceHistoryService;

    public MenuService(MenuRepository menuRepository, PriceHistoryService priceHistoryService) {
        this.menuRepository = menuRepository;
        this.priceHistoryService = priceHistoryService;
    }

    /**
     * Save a menu.
     *
     * @param menu the entity to save.
     * @return the persisted entity.
     */
    public Menu save(Menu menu) {
        log.debug("Request to save Menu : {}", menu);
        //PriceHistory priceHistory = menu.getPrice();
        BigDecimal price = null;
        if(menu.getPrice() != null) {
            price = menu.getPrice().getPrice();
        }
        if(menu.getId() == null) {
            menu.setPrice(null);
            menu.setFlagDeleted(false);
            menu.setOnStop(false);
            menuRepository.save(menu);
        }
        if(menu.getPrice() != null && menu.getPrice().getId() == null) {
            PriceHistory priceHistory = priceHistoryService.create(price, menu);
            menu.setPrice(priceHistory);
        }

        return menuRepository.save(menu);
    }

    /**
     * Get all the menus.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Menu> findAll(Pageable pageable) {
        log.debug("Request to get all Menus");
        return menuRepository.findAll(pageable);
    }

    @Transactional(readOnly = true)
    public List<Menu> findAll() {
        log.debug("Request to get all Menus without page");
        return menuRepository.findAllByFlagDeletedFalse();
    }


    /**
     * Get one menu by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Menu> findOne(Long id) {
        log.debug("Request to get Menu : {}", id);
        return menuRepository.findById(id);
    }

    /**
     * Delete the menu by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Menu : {}", id);
        Optional<Menu> menuOpt = findOne(id);
        if(!menuOpt.isPresent()) {
            return;
        }

        Menu menu = menuOpt.get();
        menu.setFlagDeleted(true);
        save(menu);

        //menuRepository.deleteById(id);
    }

    public List<Menu> findAllByIds(List<Long> ids) {
        log.debug("Request to get all menus by ids: {}", ids);
        return this.menuRepository.findAllByFlagDeletedFalseAndIdIn(ids);
    }
}

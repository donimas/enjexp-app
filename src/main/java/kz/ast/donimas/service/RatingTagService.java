package kz.ast.donimas.service;

import kz.ast.donimas.domain.RatingTag;
import kz.ast.donimas.repository.RatingTagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link RatingTag}.
 */
@Service
@Transactional
public class RatingTagService {

    private final Logger log = LoggerFactory.getLogger(RatingTagService.class);

    private final RatingTagRepository ratingTagRepository;

    public RatingTagService(RatingTagRepository ratingTagRepository) {
        this.ratingTagRepository = ratingTagRepository;
    }

    /**
     * Save a ratingTag.
     *
     * @param ratingTag the entity to save.
     * @return the persisted entity.
     */
    public RatingTag save(RatingTag ratingTag) {
        log.debug("Request to save RatingTag : {}", ratingTag);
        return ratingTagRepository.save(ratingTag);
    }

    /**
     * Get all the ratingTags.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<RatingTag> findAll(Pageable pageable) {
        log.debug("Request to get all RatingTags");
        return ratingTagRepository.findAll(pageable);
    }


    /**
     * Get one ratingTag by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<RatingTag> findOne(Long id) {
        log.debug("Request to get RatingTag : {}", id);
        return ratingTagRepository.findById(id);
    }

    /**
     * Delete the ratingTag by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete RatingTag : {}", id);
        ratingTagRepository.deleteById(id);
    }
}

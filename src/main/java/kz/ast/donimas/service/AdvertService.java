package kz.ast.donimas.service;

import kz.ast.donimas.domain.Advert;
import kz.ast.donimas.repository.AdvertRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link Advert}.
 */
@Service
@Transactional
public class AdvertService {

    private final Logger log = LoggerFactory.getLogger(AdvertService.class);

    private final AdvertRepository advertRepository;

    public AdvertService(AdvertRepository advertRepository) {
        this.advertRepository = advertRepository;
    }

    /**
     * Save a advert.
     *
     * @param advert the entity to save.
     * @return the persisted entity.
     */
    public Advert save(Advert advert) {
        log.debug("Request to save Advert : {}", advert);
        return advertRepository.save(advert);
    }

    /**
     * Get all the adverts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Advert> findAll(Pageable pageable) {
        log.debug("Request to get all Adverts");
        return advertRepository.findAll(pageable);
    }


    /**
     * Get one advert by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Advert> findOne(Long id) {
        log.debug("Request to get Advert : {}", id);
        return advertRepository.findById(id);
    }

    /**
     * Delete the advert by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Advert : {}", id);
        advertRepository.deleteById(id);
    }
}

package kz.ast.donimas.service;

import kz.ast.donimas.domain.PromocodeSale;
import kz.ast.donimas.repository.PromocodeSaleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link PromocodeSale}.
 */
@Service
@Transactional
public class PromocodeSaleService {

    private final Logger log = LoggerFactory.getLogger(PromocodeSaleService.class);

    private final PromocodeSaleRepository promocodeSaleRepository;

    public PromocodeSaleService(PromocodeSaleRepository promocodeSaleRepository) {
        this.promocodeSaleRepository = promocodeSaleRepository;
    }

    /**
     * Save a promocodeSale.
     *
     * @param promocodeSale the entity to save.
     * @return the persisted entity.
     */
    public PromocodeSale save(PromocodeSale promocodeSale) {
        log.debug("Request to save PromocodeSale : {}", promocodeSale);
        return promocodeSaleRepository.save(promocodeSale);
    }

    /**
     * Get all the promocodeSales.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PromocodeSale> findAll(Pageable pageable) {
        log.debug("Request to get all PromocodeSales");
        return promocodeSaleRepository.findAll(pageable);
    }


    /**
     * Get one promocodeSale by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PromocodeSale> findOne(Long id) {
        log.debug("Request to get PromocodeSale : {}", id);
        return promocodeSaleRepository.findById(id);
    }

    /**
     * Delete the promocodeSale by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PromocodeSale : {}", id);
        promocodeSaleRepository.deleteById(id);
    }
}

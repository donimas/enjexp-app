package kz.ast.donimas.service.util;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpUtils {

    private static final Logger log = Logger.getLogger(HttpUtils.class.getName());

    public static String requestGet(String requestUrl) throws IOException {
        BufferedReader in = null;
        try {
            log.log(Level.FINE, "requestUrl: {}", requestUrl);
            URL url = new URL(requestUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            int responseCode = con.getResponseCode();
            log.log(Level.FINE, "responseCode: {}", responseCode);

            in = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "UTF-8"));
            String inputLine;
            StringBuilder sb = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }
            return sb.toString();
        }finally {
            if(in != null) try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String requestGetViaHttps(String requestUrl) throws IOException {
        BufferedReader in = null;
        try {
            log.log(Level.FINE, "requestUrl https: {}", requestUrl);
            URL url = new URL(requestUrl);
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Mozilla/5.0");
            con.setHostnameVerifier((s, sslSession) -> true);
            int responseCode = con.getResponseCode();
            log.log(Level.FINE, "responseCode: {}", responseCode);

            in = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), "UTF-8"));
            String inputLine;
            StringBuilder sb = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                sb.append(inputLine);
            }
            return sb.toString();
        }finally {
            if(in != null) try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void writeStream(HttpURLConnection con, String content) throws IOException {
        con.setDoOutput(true);
        BufferedWriter wr = null;
        try {
            wr = new BufferedWriter(
                    new OutputStreamWriter(con.getOutputStream(), "UTF-8"));
            wr.write(content);
            wr.flush();
        } finally {
            if(wr != null){
                wr.close();
            }
        }
    }

    public static String readStream(HttpURLConnection con) throws IOException {
        BufferedReader br = null;
        try {
            br = new BufferedReader(
                new InputStreamReader(con.getInputStream(), "UTF-8"));
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");//без этого не пройдет проверка ЭЦПs
            }
            if(sb.length() > 0 && sb.charAt(0) == '\uFEFF'){
                sb.deleteCharAt(0);
            }
            return sb.toString();
        } finally {
            if(br != null){
                br.close();
            }
        }
    }
}

package kz.ast.donimas.service;

import kz.ast.donimas.domain.OrderFeedback;
import kz.ast.donimas.repository.OrderFeedbackRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link OrderFeedback}.
 */
@Service
@Transactional
public class OrderFeedbackService {

    private final Logger log = LoggerFactory.getLogger(OrderFeedbackService.class);

    private final OrderFeedbackRepository orderFeedbackRepository;

    public OrderFeedbackService(OrderFeedbackRepository orderFeedbackRepository) {
        this.orderFeedbackRepository = orderFeedbackRepository;
    }

    /**
     * Save a orderFeedback.
     *
     * @param orderFeedback the entity to save.
     * @return the persisted entity.
     */
    public OrderFeedback save(OrderFeedback orderFeedback) {
        log.debug("Request to save OrderFeedback : {}", orderFeedback);
        return orderFeedbackRepository.save(orderFeedback);
    }

    /**
     * Get all the orderFeedbacks.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<OrderFeedback> findAll(Pageable pageable) {
        log.debug("Request to get all OrderFeedbacks");
        return orderFeedbackRepository.findAll(pageable);
    }

    /**
     * Get all the orderFeedbacks with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<OrderFeedback> findAllWithEagerRelationships(Pageable pageable) {
        return orderFeedbackRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one orderFeedback by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<OrderFeedback> findOne(Long id) {
        log.debug("Request to get OrderFeedback : {}", id);
        return orderFeedbackRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the orderFeedback by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete OrderFeedback : {}", id);
        orderFeedbackRepository.deleteById(id);
    }
}

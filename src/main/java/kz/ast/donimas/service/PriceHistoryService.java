package kz.ast.donimas.service;

import kz.ast.donimas.domain.Menu;
import kz.ast.donimas.domain.PriceHistory;
import kz.ast.donimas.domain.enumeration.PriceCurrency;
import kz.ast.donimas.repository.PriceHistoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;

/**
 * Service Implementation for managing {@link PriceHistory}.
 */
@Service
@Transactional
public class PriceHistoryService {

    private final Logger log = LoggerFactory.getLogger(PriceHistoryService.class);

    private final PriceHistoryRepository priceHistoryRepository;

    public PriceHistoryService(PriceHistoryRepository priceHistoryRepository) {
        this.priceHistoryRepository = priceHistoryRepository;
    }

    public PriceHistory create(BigDecimal price, Menu menu) {
        if(price == null) {
            return null;
        }
        PriceHistory result = new PriceHistory();
        result.setPrice(price);
        result.setMenu(menu);
        result.setCurrency(PriceCurrency.KZT);

        return save(result);
    }

    /**
     * Save a priceHistory.
     *
     * @param priceHistory the entity to save.
     * @return the persisted entity.
     */
    public PriceHistory save(PriceHistory priceHistory) {
        log.debug("Request to save PriceHistory : {}", priceHistory);
        return priceHistoryRepository.save(priceHistory);
    }

    /**
     * Get all the priceHistories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<PriceHistory> findAll(Pageable pageable) {
        log.debug("Request to get all PriceHistories");
        return priceHistoryRepository.findAll(pageable);
    }


    /**
     * Get one priceHistory by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<PriceHistory> findOne(Long id) {
        log.debug("Request to get PriceHistory : {}", id);
        return priceHistoryRepository.findById(id);
    }

    /**
     * Delete the priceHistory by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete PriceHistory : {}", id);
        priceHistoryRepository.deleteById(id);
    }
}

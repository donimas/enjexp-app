package kz.ast.donimas.repository;
import kz.ast.donimas.domain.Promocode;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Promocode entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PromocodeRepository extends JpaRepository<Promocode, Long> {

    @Query("select promocode from Promocode promocode where promocode.user.login = ?#{principal.username}")
    List<Promocode> findByUserIsCurrentUser();

    @Query("select promocode from Promocode promocode where promocode.flagActive = true and promocode.user.login = ?#{principal.username}")
    List<Promocode> findActiveByUserIsCurrentUser();

    Long countAllByCode(String code);

}

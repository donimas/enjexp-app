package kz.ast.donimas.repository;
import kz.ast.donimas.domain.PromocodeSale;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the PromocodeSale entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PromocodeSaleRepository extends JpaRepository<PromocodeSale, Long> {

}

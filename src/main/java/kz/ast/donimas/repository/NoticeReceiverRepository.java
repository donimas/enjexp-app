package kz.ast.donimas.repository;
import kz.ast.donimas.domain.NoticeReceiver;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the NoticeReceiver entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NoticeReceiverRepository extends JpaRepository<NoticeReceiver, Long> {

}

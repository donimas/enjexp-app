package kz.ast.donimas.repository;
import kz.ast.donimas.domain.Advert;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Advert entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AdvertRepository extends JpaRepository<Advert, Long> {

}

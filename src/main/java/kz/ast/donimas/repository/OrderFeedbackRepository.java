package kz.ast.donimas.repository;
import kz.ast.donimas.domain.OrderFeedback;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the OrderFeedback entity.
 */
@Repository
public interface OrderFeedbackRepository extends JpaRepository<OrderFeedback, Long> {

    @Query(value = "select distinct orderFeedback from OrderFeedback orderFeedback left join fetch orderFeedback.ratingTags",
        countQuery = "select count(distinct orderFeedback) from OrderFeedback orderFeedback")
    Page<OrderFeedback> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct orderFeedback from OrderFeedback orderFeedback left join fetch orderFeedback.ratingTags")
    List<OrderFeedback> findAllWithEagerRelationships();

    @Query("select orderFeedback from OrderFeedback orderFeedback left join fetch orderFeedback.ratingTags where orderFeedback.id =:id")
    Optional<OrderFeedback> findOneWithEagerRelationships(@Param("id") Long id);

}

package kz.ast.donimas.repository;
import kz.ast.donimas.domain.Order;
import kz.ast.donimas.domain.User;
import kz.ast.donimas.domain.enumeration.OrderStatus;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the Order entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    @Query("select order from Order order where order.user.login = ?#{principal.username}")
    List<Order> findByUserIsCurrentUser();

    Long countAllByPromocodeAndUser_LoginNotAndStatus_OrderStatus(String promocode, String login, OrderStatus orderStatus);

    Long countAllByPromocodeIsNotNullAndUserAndStatus_OrderStatus(User user, OrderStatus orderStatus);

    @Query("select order from Order order where order.user = ?1 and order.status.orderStatus in (?2)")
    List<Order> findAllOrderOfUser(User user, List<OrderStatus> statuses);

    List<Order> findAllByFlagDeletedFalseAndStatus_OrderStatusIn(List<OrderStatus> orderStatuses);

}

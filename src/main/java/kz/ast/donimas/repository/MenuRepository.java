package kz.ast.donimas.repository;
import kz.ast.donimas.domain.Menu;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Menu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {

    List<Menu> findAllByFlagDeletedFalse();

    List<Menu> findAllByFlagDeletedFalseAndIdIn(List<Long> ids);

}

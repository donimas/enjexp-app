package kz.ast.donimas.repository;
import kz.ast.donimas.domain.RatingTag;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the RatingTag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RatingTagRepository extends JpaRepository<RatingTag, Long> {

}

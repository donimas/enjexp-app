package kz.ast.donimas.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import kz.ast.donimas.domain.Advert;
import kz.ast.donimas.domain.Category;
import kz.ast.donimas.domain.Menu;
import kz.ast.donimas.service.AdvertService;
import kz.ast.donimas.service.CategoryService;
import kz.ast.donimas.service.MenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link kz.ast.donimas.domain.Menu}.
 */
@RestController
@RequestMapping("/open-api")
public class MenuExtResource {

    private final Logger log = LoggerFactory.getLogger(MenuExtResource.class);

    private static final String ENTITY_NAME = "menu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MenuService menuService;
    private final CategoryService categoryService;
    private final AdvertService advertService;

    public MenuExtResource(
            MenuService menuService,
            CategoryService categoryService, AdvertService advertService) {
        this.menuService = menuService;
        this.categoryService = categoryService;
        this.advertService = advertService;
    }

    @GetMapping("/menus")
    public ResponseEntity<List<Menu>> getAllMenus(Pageable pageable) {
        log.debug("REST request to get a page of Menus");
        List<Menu> menu = menuService.findAll();
        //HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().body(menu);
    }

    @GetMapping("/menus/{id}")
    public ResponseEntity<Menu> getMenu(@PathVariable Long id) {
        log.debug("REST request to get Menu : {}", id);
        Optional<Menu> menu = menuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(menu);
    }

    @GetMapping("/categories")
    public ResponseEntity<List<Category>> getAllCategories(Pageable pageable) {
        log.debug("REST request to get a page of Categories");
        Page<Category> page = categoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /adverts} : get all the adverts.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adverts in body.
     */
    @GetMapping("/adverts")
    public ResponseEntity<List<Advert>> getAllAdverts(Pageable pageable) {
        log.debug("REST request to get a page of Adverts");
        Page<Advert> page = advertService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    @GetMapping("/favourites")
    public ResponseEntity<List<Menu>> findAllFavourites(@RequestParam List<Long> id) throws URISyntaxException {
        log.debug("REST request to get favs : {}", id);

        List<Menu> result = menuService.findAllByIds(id);

        return ResponseEntity.ok(result);
    }
}

package kz.ast.donimas.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import kz.ast.donimas.config.Constants;
import kz.ast.donimas.domain.Order;
import kz.ast.donimas.domain.Promocode;
import kz.ast.donimas.domain.User;
import kz.ast.donimas.security.SecurityUtils;
import kz.ast.donimas.service.ClientService;
import kz.ast.donimas.service.OrderService;
import kz.ast.donimas.service.PromocodeService;
import kz.ast.donimas.service.UserService;
import kz.ast.donimas.service.dto.CheckoutDto;
import kz.ast.donimas.service.dto.PromocodeConfirmDto;
import kz.ast.donimas.service.dto.PromocodeOwner;
import kz.ast.donimas.web.rest.errors.FLCException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.util.ListUtils;

import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api")
public class ClientResource {

    private final Logger log = LoggerFactory.getLogger(ClientResource.class);

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final UserService userService;
    private final PromocodeService promocodeService;
    private final OrderService orderService;
    private final ClientService clientService;

    public ClientResource(UserService userService, PromocodeService promocodeService, OrderService orderService, ClientService clientService) {
        this.userService = userService;
        this.promocodeService = promocodeService;
        this.orderService = orderService;
        this.clientService = clientService;
    }

    @GetMapping("/create/promocode")
    public ResponseEntity<Promocode> createPromoCodeForUser() throws URISyntaxException {
        User currentUser = this.userService.checkClientAccess();
        Promocode result = promocodeService.create(currentUser);

        return ResponseEntity.created(new URI("/client-api/create/promocode/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, "promocode", result.getId().toString()))
                .body(result);
    }

    @GetMapping("/promocode/filled")
    public ResponseEntity<PromocodeConfirmDto> checkPromocodeOwner() throws URISyntaxException {
        log.debug("Rest request to check promo code filled");
        User currentUser = this.userService.checkClientAccess();
        List<Promocode> userPromos = promocodeService.findActiveByUser();
        if(ListUtils.isEmpty(userPromos)) {
            throw new FLCException("Empty promos");
        }

        // check if promocode owner's order less than used promocode by others
        Optional<PromocodeConfirmDto> optPromoOwner = userPromos.stream()
                .filter((p) -> orderService.countUsedPromocode(
                        p.getCode(),
                        currentUser.getLogin()) > p.getUsedByOwnerCount().longValue())
                .map((p) -> new PromocodeConfirmDto(p.getUsedByOwnerCount().intValue(), p.getCode()))
                .findFirst();

        PromocodeConfirmDto result = null;
        if(optPromoOwner.isPresent()) {
            result = optPromoOwner.get();
            log.debug("Found Promocode owner: {}", result);
            result.confirmed = true;
            result.login = currentUser.getLogin();
            result.discount = new BigDecimal(Constants.HELLO_PROMOCODE_DISCOUNT);
        }

        return ResponseEntity.created(new URI("/client-api/promocode/filled"))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, "promocode", "owner"))
                .body(result);
    }

    @GetMapping("/promocode/check/{code}")
    public ResponseEntity<PromocodeConfirmDto> checkPromocode(@PathVariable String code) throws URISyntaxException {
        log.debug("Rest request to check promo code: {}", code);
        userService.checkClientAccess();

        PromocodeConfirmDto result = clientService.checkPromoCode(code);

        return ResponseEntity.created(new URI("/client-api/getpro"))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, "pro", code))
                .body(result);
    }

    @PostMapping("/client/checkout")
    public ResponseEntity<Void> clientCheckout(@RequestBody CheckoutDto checkoutDto) {
        log.debug("REST request to client checkout : {}", checkoutDto);
        userService.checkClientAccess();

        PromocodeConfirmDto promocodeConfirmDto = null;
        if(!StringUtils.isEmpty(checkoutDto.promocode)) {
            try {
                promocodeConfirmDto = clientService.checkPromoCode(checkoutDto.promocode);
            } catch (FLCException e) {
                log.error(e.getMessage(), e);
            }
        }

        Order result = orderService.create(checkoutDto, promocodeConfirmDto);
        return ResponseEntity.noContent().headers(HeaderUtil.createAlert(applicationName, "Успешно приняли ваш заказ", null)).build();
    }

    @GetMapping("/client/orders")
    public List<CheckoutDto> findAllOrderOfClient() {
        log.debug("Rest request to find all orders");
        User user = userService.checkClientAccess();
        List<Order> orders = orderService.findAllOfUser(user);
        return orders.stream().map(orderService::toCheckoutDto).collect(Collectors.toList());
    }

    @GetMapping("/client/order/{id}")
    public CheckoutDto findOrder(@PathVariable Long id) {
        log.debug("Rest request to find order -> {}", id);
        userService.checkClientAccess();
        Optional<Order> optOrder = orderService.findOne(id);
        if(!optOrder.isPresent()) {
            throw new FLCException("Order not found");
        }
        return orderService.toCheckoutDto(optOrder.get());
    }

    @GetMapping("/client/cancel/order/{id}")
    public ResponseEntity<Void> cancelOrder(@PathVariable Long id) {
        log.debug("Rest request to cancel order -> {}", id);
        User user = userService.checkClientAccess();
        Optional<Order> optOrder = orderService.findOne(id);
        if(!optOrder.isPresent()) {
            throw new FLCException("Order not found");
        }
        Order order = optOrder.get();
        if(order.getUser() == null || !order.getUser().getLogin().equals(user.getLogin())) {
            throw new FLCException("Incorrect access");
        }
        orderService.cancelOrder(order);

        return ResponseEntity.ok().build();
    }

}

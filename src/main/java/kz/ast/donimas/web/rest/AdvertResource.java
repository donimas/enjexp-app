package kz.ast.donimas.web.rest;

import kz.ast.donimas.domain.Advert;
import kz.ast.donimas.service.AdvertService;
import kz.ast.donimas.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link kz.ast.donimas.domain.Advert}.
 */
@RestController
@RequestMapping("/api")
public class AdvertResource {

    private final Logger log = LoggerFactory.getLogger(AdvertResource.class);

    private static final String ENTITY_NAME = "advert";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AdvertService advertService;

    public AdvertResource(AdvertService advertService) {
        this.advertService = advertService;
    }

    /**
     * {@code POST  /adverts} : Create a new advert.
     *
     * @param advert the advert to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new advert, or with status {@code 400 (Bad Request)} if the advert has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/adverts")
    public ResponseEntity<Advert> createAdvert(@RequestBody Advert advert) throws URISyntaxException {
        log.debug("REST request to save Advert : {}", advert);
        if (advert.getId() != null) {
            throw new BadRequestAlertException("A new advert cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Advert result = advertService.save(advert);
        return ResponseEntity.created(new URI("/api/adverts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /adverts} : Updates an existing advert.
     *
     * @param advert the advert to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated advert,
     * or with status {@code 400 (Bad Request)} if the advert is not valid,
     * or with status {@code 500 (Internal Server Error)} if the advert couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/adverts")
    public ResponseEntity<Advert> updateAdvert(@RequestBody Advert advert) throws URISyntaxException {
        log.debug("REST request to update Advert : {}", advert);
        if (advert.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Advert result = advertService.save(advert);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, advert.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /adverts} : get all the adverts.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of adverts in body.
     */
    @GetMapping("/adverts")
    public ResponseEntity<List<Advert>> getAllAdverts(Pageable pageable) {
        log.debug("REST request to get a page of Adverts");
        Page<Advert> page = advertService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /adverts/:id} : get the "id" advert.
     *
     * @param id the id of the advert to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the advert, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/adverts/{id}")
    public ResponseEntity<Advert> getAdvert(@PathVariable Long id) {
        log.debug("REST request to get Advert : {}", id);
        Optional<Advert> advert = advertService.findOne(id);
        return ResponseUtil.wrapOrNotFound(advert);
    }

    /**
     * {@code DELETE  /adverts/:id} : delete the "id" advert.
     *
     * @param id the id of the advert to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/adverts/{id}")
    public ResponseEntity<Void> deleteAdvert(@PathVariable Long id) {
        log.debug("REST request to delete Advert : {}", id);
        advertService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

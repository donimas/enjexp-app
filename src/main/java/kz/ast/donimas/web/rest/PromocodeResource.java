package kz.ast.donimas.web.rest;

import kz.ast.donimas.domain.Promocode;
import kz.ast.donimas.service.PromocodeService;
import kz.ast.donimas.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link kz.ast.donimas.domain.Promocode}.
 */
@RestController
@RequestMapping("/api")
public class PromocodeResource {

    private final Logger log = LoggerFactory.getLogger(PromocodeResource.class);

    private static final String ENTITY_NAME = "promocode";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PromocodeService promocodeService;

    public PromocodeResource(PromocodeService promocodeService) {
        this.promocodeService = promocodeService;
    }

    /**
     * {@code POST  /promocodes} : Create a new promocode.
     *
     * @param promocode the promocode to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new promocode, or with status {@code 400 (Bad Request)} if the promocode has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/promocodes")
    public ResponseEntity<Promocode> createPromocode(@RequestBody Promocode promocode) throws URISyntaxException {
        log.debug("REST request to save Promocode : {}", promocode);
        if (promocode.getId() != null) {
            throw new BadRequestAlertException("A new promocode cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Promocode result = promocodeService.save(promocode);
        return ResponseEntity.created(new URI("/api/promocodes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /promocodes} : Updates an existing promocode.
     *
     * @param promocode the promocode to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated promocode,
     * or with status {@code 400 (Bad Request)} if the promocode is not valid,
     * or with status {@code 500 (Internal Server Error)} if the promocode couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/promocodes")
    public ResponseEntity<Promocode> updatePromocode(@RequestBody Promocode promocode) throws URISyntaxException {
        log.debug("REST request to update Promocode : {}", promocode);
        if (promocode.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Promocode result = promocodeService.save(promocode);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, promocode.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /promocodes} : get all the promocodes.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of promocodes in body.
     */
    @GetMapping("/promocodes")
    public ResponseEntity<List<Promocode>> getAllPromocodes(Pageable pageable) {
        log.debug("REST request to get a page of Promocodes");
        Page<Promocode> page = promocodeService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /promocodes/:id} : get the "id" promocode.
     *
     * @param id the id of the promocode to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the promocode, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/promocodes/{id}")
    public ResponseEntity<Promocode> getPromocode(@PathVariable Long id) {
        log.debug("REST request to get Promocode : {}", id);
        Optional<Promocode> promocode = promocodeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(promocode);
    }

    /**
     * {@code DELETE  /promocodes/:id} : delete the "id" promocode.
     *
     * @param id the id of the promocode to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/promocodes/{id}")
    public ResponseEntity<Void> deletePromocode(@PathVariable Long id) {
        log.debug("REST request to delete Promocode : {}", id);
        promocodeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

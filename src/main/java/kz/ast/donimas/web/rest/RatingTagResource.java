package kz.ast.donimas.web.rest;

import kz.ast.donimas.domain.RatingTag;
import kz.ast.donimas.service.RatingTagService;
import kz.ast.donimas.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link kz.ast.donimas.domain.RatingTag}.
 */
@RestController
@RequestMapping("/api")
public class RatingTagResource {

    private final Logger log = LoggerFactory.getLogger(RatingTagResource.class);

    private static final String ENTITY_NAME = "ratingTag";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RatingTagService ratingTagService;

    public RatingTagResource(RatingTagService ratingTagService) {
        this.ratingTagService = ratingTagService;
    }

    /**
     * {@code POST  /rating-tags} : Create a new ratingTag.
     *
     * @param ratingTag the ratingTag to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new ratingTag, or with status {@code 400 (Bad Request)} if the ratingTag has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/rating-tags")
    public ResponseEntity<RatingTag> createRatingTag(@RequestBody RatingTag ratingTag) throws URISyntaxException {
        log.debug("REST request to save RatingTag : {}", ratingTag);
        if (ratingTag.getId() != null) {
            throw new BadRequestAlertException("A new ratingTag cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RatingTag result = ratingTagService.save(ratingTag);
        return ResponseEntity.created(new URI("/api/rating-tags/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /rating-tags} : Updates an existing ratingTag.
     *
     * @param ratingTag the ratingTag to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated ratingTag,
     * or with status {@code 400 (Bad Request)} if the ratingTag is not valid,
     * or with status {@code 500 (Internal Server Error)} if the ratingTag couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/rating-tags")
    public ResponseEntity<RatingTag> updateRatingTag(@RequestBody RatingTag ratingTag) throws URISyntaxException {
        log.debug("REST request to update RatingTag : {}", ratingTag);
        if (ratingTag.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RatingTag result = ratingTagService.save(ratingTag);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, ratingTag.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /rating-tags} : get all the ratingTags.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of ratingTags in body.
     */
    @GetMapping("/rating-tags")
    public ResponseEntity<List<RatingTag>> getAllRatingTags(Pageable pageable) {
        log.debug("REST request to get a page of RatingTags");
        Page<RatingTag> page = ratingTagService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /rating-tags/:id} : get the "id" ratingTag.
     *
     * @param id the id of the ratingTag to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the ratingTag, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/rating-tags/{id}")
    public ResponseEntity<RatingTag> getRatingTag(@PathVariable Long id) {
        log.debug("REST request to get RatingTag : {}", id);
        Optional<RatingTag> ratingTag = ratingTagService.findOne(id);
        return ResponseUtil.wrapOrNotFound(ratingTag);
    }

    /**
     * {@code DELETE  /rating-tags/:id} : delete the "id" ratingTag.
     *
     * @param id the id of the ratingTag to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/rating-tags/{id}")
    public ResponseEntity<Void> deleteRatingTag(@PathVariable Long id) {
        log.debug("REST request to delete RatingTag : {}", id);
        ratingTagService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

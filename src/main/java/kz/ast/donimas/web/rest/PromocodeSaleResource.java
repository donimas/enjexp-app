package kz.ast.donimas.web.rest;

import kz.ast.donimas.domain.PromocodeSale;
import kz.ast.donimas.service.PromocodeSaleService;
import kz.ast.donimas.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link kz.ast.donimas.domain.PromocodeSale}.
 */
@RestController
@RequestMapping("/api")
public class PromocodeSaleResource {

    private final Logger log = LoggerFactory.getLogger(PromocodeSaleResource.class);

    private static final String ENTITY_NAME = "promocodeSale";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final PromocodeSaleService promocodeSaleService;

    public PromocodeSaleResource(PromocodeSaleService promocodeSaleService) {
        this.promocodeSaleService = promocodeSaleService;
    }

    /**
     * {@code POST  /promocode-sales} : Create a new promocodeSale.
     *
     * @param promocodeSale the promocodeSale to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new promocodeSale, or with status {@code 400 (Bad Request)} if the promocodeSale has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/promocode-sales")
    public ResponseEntity<PromocodeSale> createPromocodeSale(@RequestBody PromocodeSale promocodeSale) throws URISyntaxException {
        log.debug("REST request to save PromocodeSale : {}", promocodeSale);
        if (promocodeSale.getId() != null) {
            throw new BadRequestAlertException("A new promocodeSale cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PromocodeSale result = promocodeSaleService.save(promocodeSale);
        return ResponseEntity.created(new URI("/api/promocode-sales/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /promocode-sales} : Updates an existing promocodeSale.
     *
     * @param promocodeSale the promocodeSale to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated promocodeSale,
     * or with status {@code 400 (Bad Request)} if the promocodeSale is not valid,
     * or with status {@code 500 (Internal Server Error)} if the promocodeSale couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/promocode-sales")
    public ResponseEntity<PromocodeSale> updatePromocodeSale(@RequestBody PromocodeSale promocodeSale) throws URISyntaxException {
        log.debug("REST request to update PromocodeSale : {}", promocodeSale);
        if (promocodeSale.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        PromocodeSale result = promocodeSaleService.save(promocodeSale);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, promocodeSale.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /promocode-sales} : get all the promocodeSales.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of promocodeSales in body.
     */
    @GetMapping("/promocode-sales")
    public ResponseEntity<List<PromocodeSale>> getAllPromocodeSales(Pageable pageable) {
        log.debug("REST request to get a page of PromocodeSales");
        Page<PromocodeSale> page = promocodeSaleService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /promocode-sales/:id} : get the "id" promocodeSale.
     *
     * @param id the id of the promocodeSale to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the promocodeSale, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/promocode-sales/{id}")
    public ResponseEntity<PromocodeSale> getPromocodeSale(@PathVariable Long id) {
        log.debug("REST request to get PromocodeSale : {}", id);
        Optional<PromocodeSale> promocodeSale = promocodeSaleService.findOne(id);
        return ResponseUtil.wrapOrNotFound(promocodeSale);
    }

    /**
     * {@code DELETE  /promocode-sales/:id} : delete the "id" promocodeSale.
     *
     * @param id the id of the promocodeSale to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/promocode-sales/{id}")
    public ResponseEntity<Void> deletePromocodeSale(@PathVariable Long id) {
        log.debug("REST request to delete PromocodeSale : {}", id);
        promocodeSaleService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

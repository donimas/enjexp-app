package kz.ast.donimas.web.rest;

import kz.ast.donimas.domain.NoticeReceiver;
import kz.ast.donimas.service.NoticeReceiverService;
import kz.ast.donimas.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link kz.ast.donimas.domain.NoticeReceiver}.
 */
@RestController
@RequestMapping("/api")
public class NoticeReceiverResource {

    private final Logger log = LoggerFactory.getLogger(NoticeReceiverResource.class);

    private static final String ENTITY_NAME = "noticeReceiver";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final NoticeReceiverService noticeReceiverService;

    public NoticeReceiverResource(NoticeReceiverService noticeReceiverService) {
        this.noticeReceiverService = noticeReceiverService;
    }

    /**
     * {@code POST  /notice-receivers} : Create a new noticeReceiver.
     *
     * @param noticeReceiver the noticeReceiver to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new noticeReceiver, or with status {@code 400 (Bad Request)} if the noticeReceiver has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/notice-receivers")
    public ResponseEntity<NoticeReceiver> createNoticeReceiver(@RequestBody NoticeReceiver noticeReceiver) throws URISyntaxException {
        log.debug("REST request to save NoticeReceiver : {}", noticeReceiver);
        if (noticeReceiver.getId() != null) {
            throw new BadRequestAlertException("A new noticeReceiver cannot already have an ID", ENTITY_NAME, "idexists");
        }
        NoticeReceiver result = noticeReceiverService.save(noticeReceiver);
        return ResponseEntity.created(new URI("/api/notice-receivers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /notice-receivers} : Updates an existing noticeReceiver.
     *
     * @param noticeReceiver the noticeReceiver to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated noticeReceiver,
     * or with status {@code 400 (Bad Request)} if the noticeReceiver is not valid,
     * or with status {@code 500 (Internal Server Error)} if the noticeReceiver couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/notice-receivers")
    public ResponseEntity<NoticeReceiver> updateNoticeReceiver(@RequestBody NoticeReceiver noticeReceiver) throws URISyntaxException {
        log.debug("REST request to update NoticeReceiver : {}", noticeReceiver);
        if (noticeReceiver.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        NoticeReceiver result = noticeReceiverService.save(noticeReceiver);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, noticeReceiver.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /notice-receivers} : get all the noticeReceivers.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of noticeReceivers in body.
     */
    @GetMapping("/notice-receivers")
    public ResponseEntity<List<NoticeReceiver>> getAllNoticeReceivers(Pageable pageable) {
        log.debug("REST request to get a page of NoticeReceivers");
        Page<NoticeReceiver> page = noticeReceiverService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /notice-receivers/:id} : get the "id" noticeReceiver.
     *
     * @param id the id of the noticeReceiver to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the noticeReceiver, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/notice-receivers/{id}")
    public ResponseEntity<NoticeReceiver> getNoticeReceiver(@PathVariable Long id) {
        log.debug("REST request to get NoticeReceiver : {}", id);
        Optional<NoticeReceiver> noticeReceiver = noticeReceiverService.findOne(id);
        return ResponseUtil.wrapOrNotFound(noticeReceiver);
    }

    /**
     * {@code DELETE  /notice-receivers/:id} : delete the "id" noticeReceiver.
     *
     * @param id the id of the noticeReceiver to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/notice-receivers/{id}")
    public ResponseEntity<Void> deleteNoticeReceiver(@PathVariable Long id) {
        log.debug("REST request to delete NoticeReceiver : {}", id);
        noticeReceiverService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

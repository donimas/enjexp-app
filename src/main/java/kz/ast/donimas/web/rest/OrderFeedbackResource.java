package kz.ast.donimas.web.rest;

import kz.ast.donimas.domain.OrderFeedback;
import kz.ast.donimas.service.OrderFeedbackService;
import kz.ast.donimas.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link kz.ast.donimas.domain.OrderFeedback}.
 */
@RestController
@RequestMapping("/api")
public class OrderFeedbackResource {

    private final Logger log = LoggerFactory.getLogger(OrderFeedbackResource.class);

    private static final String ENTITY_NAME = "orderFeedback";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderFeedbackService orderFeedbackService;

    public OrderFeedbackResource(OrderFeedbackService orderFeedbackService) {
        this.orderFeedbackService = orderFeedbackService;
    }

    /**
     * {@code POST  /order-feedbacks} : Create a new orderFeedback.
     *
     * @param orderFeedback the orderFeedback to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderFeedback, or with status {@code 400 (Bad Request)} if the orderFeedback has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/order-feedbacks")
    public ResponseEntity<OrderFeedback> createOrderFeedback(@RequestBody OrderFeedback orderFeedback) throws URISyntaxException {
        log.debug("REST request to save OrderFeedback : {}", orderFeedback);
        if (orderFeedback.getId() != null) {
            throw new BadRequestAlertException("A new orderFeedback cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderFeedback result = orderFeedbackService.save(orderFeedback);
        return ResponseEntity.created(new URI("/api/order-feedbacks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-feedbacks} : Updates an existing orderFeedback.
     *
     * @param orderFeedback the orderFeedback to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderFeedback,
     * or with status {@code 400 (Bad Request)} if the orderFeedback is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderFeedback couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/order-feedbacks")
    public ResponseEntity<OrderFeedback> updateOrderFeedback(@RequestBody OrderFeedback orderFeedback) throws URISyntaxException {
        log.debug("REST request to update OrderFeedback : {}", orderFeedback);
        if (orderFeedback.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderFeedback result = orderFeedbackService.save(orderFeedback);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderFeedback.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /order-feedbacks} : get all the orderFeedbacks.
     *

     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderFeedbacks in body.
     */
    @GetMapping("/order-feedbacks")
    public ResponseEntity<List<OrderFeedback>> getAllOrderFeedbacks(Pageable pageable, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of OrderFeedbacks");
        Page<OrderFeedback> page;
        if (eagerload) {
            page = orderFeedbackService.findAllWithEagerRelationships(pageable);
        } else {
            page = orderFeedbackService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /order-feedbacks/:id} : get the "id" orderFeedback.
     *
     * @param id the id of the orderFeedback to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderFeedback, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/order-feedbacks/{id}")
    public ResponseEntity<OrderFeedback> getOrderFeedback(@PathVariable Long id) {
        log.debug("REST request to get OrderFeedback : {}", id);
        Optional<OrderFeedback> orderFeedback = orderFeedbackService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderFeedback);
    }

    /**
     * {@code DELETE  /order-feedbacks/:id} : delete the "id" orderFeedback.
     *
     * @param id the id of the orderFeedback to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/order-feedbacks/{id}")
    public ResponseEntity<Void> deleteOrderFeedback(@PathVariable Long id) {
        log.debug("REST request to delete OrderFeedback : {}", id);
        orderFeedbackService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

/**
 * View Models used by Spring MVC REST controllers.
 */
package kz.ast.donimas.web.rest.vm;

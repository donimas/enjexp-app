package kz.ast.donimas.web.rest;

import kz.ast.donimas.domain.OrderStatusHistory;
import kz.ast.donimas.service.OrderStatusHistoryService;
import kz.ast.donimas.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link kz.ast.donimas.domain.OrderStatusHistory}.
 */
@RestController
@RequestMapping("/api")
public class OrderStatusHistoryResource {

    private final Logger log = LoggerFactory.getLogger(OrderStatusHistoryResource.class);

    private static final String ENTITY_NAME = "orderStatusHistory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OrderStatusHistoryService orderStatusHistoryService;

    public OrderStatusHistoryResource(OrderStatusHistoryService orderStatusHistoryService) {
        this.orderStatusHistoryService = orderStatusHistoryService;
    }

    /**
     * {@code POST  /order-status-histories} : Create a new orderStatusHistory.
     *
     * @param orderStatusHistory the orderStatusHistory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new orderStatusHistory, or with status {@code 400 (Bad Request)} if the orderStatusHistory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/order-status-histories")
    public ResponseEntity<OrderStatusHistory> createOrderStatusHistory(@RequestBody OrderStatusHistory orderStatusHistory) throws URISyntaxException {
        log.debug("REST request to save OrderStatusHistory : {}", orderStatusHistory);
        if (orderStatusHistory.getId() != null) {
            throw new BadRequestAlertException("A new orderStatusHistory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OrderStatusHistory result = orderStatusHistoryService.save(orderStatusHistory);
        return ResponseEntity.created(new URI("/api/order-status-histories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /order-status-histories} : Updates an existing orderStatusHistory.
     *
     * @param orderStatusHistory the orderStatusHistory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated orderStatusHistory,
     * or with status {@code 400 (Bad Request)} if the orderStatusHistory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the orderStatusHistory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/order-status-histories")
    public ResponseEntity<OrderStatusHistory> updateOrderStatusHistory(@RequestBody OrderStatusHistory orderStatusHistory) throws URISyntaxException {
        log.debug("REST request to update OrderStatusHistory : {}", orderStatusHistory);
        if (orderStatusHistory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OrderStatusHistory result = orderStatusHistoryService.save(orderStatusHistory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, orderStatusHistory.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /order-status-histories} : get all the orderStatusHistories.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of orderStatusHistories in body.
     */
    @GetMapping("/order-status-histories")
    public ResponseEntity<List<OrderStatusHistory>> getAllOrderStatusHistories(Pageable pageable) {
        log.debug("REST request to get a page of OrderStatusHistories");
        Page<OrderStatusHistory> page = orderStatusHistoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /order-status-histories/:id} : get the "id" orderStatusHistory.
     *
     * @param id the id of the orderStatusHistory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the orderStatusHistory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/order-status-histories/{id}")
    public ResponseEntity<OrderStatusHistory> getOrderStatusHistory(@PathVariable Long id) {
        log.debug("REST request to get OrderStatusHistory : {}", id);
        Optional<OrderStatusHistory> orderStatusHistory = orderStatusHistoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(orderStatusHistory);
    }

    /**
     * {@code DELETE  /order-status-histories/:id} : delete the "id" orderStatusHistory.
     *
     * @param id the id of the orderStatusHistory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/order-status-histories/{id}")
    public ResponseEntity<Void> deleteOrderStatusHistory(@PathVariable Long id) {
        log.debug("REST request to delete OrderStatusHistory : {}", id);
        orderStatusHistoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}

package kz.ast.donimas.web.rest.errors;

public class FLCException extends RuntimeException {

    public FLCException(String message) {
        super(message);
    }

    public static FLCException getErrorOccuredInstance() {
        return new FLCException("error.occurred");
    }

    public static FLCException getErrorOccuredTryAgainInstance() {
        return new FLCException("error.occurred-try-again");
    }

}

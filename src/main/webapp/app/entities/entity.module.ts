import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'menu',
        loadChildren: () => import('./menu/menu.module').then(m => m.EnjexpMenuModule)
      },
      {
        path: 'category',
        loadChildren: () => import('./category/category.module').then(m => m.EnjexpCategoryModule)
      },
      {
        path: 'price-history',
        loadChildren: () => import('./price-history/price-history.module').then(m => m.EnjexpPriceHistoryModule)
      },
      {
        path: 'order',
        loadChildren: () => import('./order/order.module').then(m => m.EnjexpOrderModule)
      },
      {
        path: 'location',
        loadChildren: () => import('./location/location.module').then(m => m.EnjexpLocationModule)
      },
      {
        path: 'order-item',
        loadChildren: () => import('./order-item/order-item.module').then(m => m.EnjexpOrderItemModule)
      },
      {
        path: 'order-status-history',
        loadChildren: () => import('./order-status-history/order-status-history.module').then(m => m.EnjexpOrderStatusHistoryModule)
      },
      {
        path: 'order-feedback',
        loadChildren: () => import('./order-feedback/order-feedback.module').then(m => m.EnjexpOrderFeedbackModule)
      },
      {
        path: 'rating-tag',
        loadChildren: () => import('./rating-tag/rating-tag.module').then(m => m.EnjexpRatingTagModule)
      },
      {
        path: 'promocode',
        loadChildren: () => import('./promocode/promocode.module').then(m => m.EnjexpPromocodeModule)
      },
      {
        path: 'promocode-sale',
        loadChildren: () => import('./promocode-sale/promocode-sale.module').then(m => m.EnjexpPromocodeSaleModule)
      },
      {
        path: 'advert',
        loadChildren: () => import('./advert/advert.module').then(m => m.EnjexpAdvertModule)
      },
      {
        path: 'notice',
        loadChildren: () => import('./notice/notice.module').then(m => m.EnjexpNoticeModule)
      },
      {
        path: 'notice-receiver',
        loadChildren: () => import('./notice-receiver/notice-receiver.module').then(m => m.EnjexpNoticeReceiverModule)
      },
      {
        path: 'feedback',
        loadChildren: () => import('./feedback/feedback.module').then(m => m.EnjexpFeedbackModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class EnjexpEntityModule {}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INotice } from 'app/shared/model/notice.model';
import { NoticeService } from './notice.service';

@Component({
  selector: 'jhi-notice-delete-dialog',
  templateUrl: './notice-delete-dialog.component.html'
})
export class NoticeDeleteDialogComponent {
  notice: INotice;

  constructor(protected noticeService: NoticeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.noticeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'noticeListModification',
        content: 'Deleted an notice'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-notice-delete-popup',
  template: ''
})
export class NoticeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ notice }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(NoticeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.notice = notice;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/notice', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/notice', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { INotice, Notice } from 'app/shared/model/notice.model';
import { NoticeService } from './notice.service';

@Component({
  selector: 'jhi-notice-update',
  templateUrl: './notice-update.component.html'
})
export class NoticeUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    title: [],
    content: [],
    smsContent: [],
    flagPush: [],
    flagSms: [],
    flagEmail: [],
    receiverType: [],
    createDate: [],
    modifyDate: [],
    containerClass: [],
    containerId: [],
    noticeType: [],
    flagSent: [],
    sentTime: [],
    payloadObjectUrl: [],
    payloadObjectId: [],
    flagDeleted: []
  });

  constructor(protected noticeService: NoticeService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ notice }) => {
      this.updateForm(notice);
    });
  }

  updateForm(notice: INotice) {
    this.editForm.patchValue({
      id: notice.id,
      title: notice.title,
      content: notice.content,
      smsContent: notice.smsContent,
      flagPush: notice.flagPush,
      flagSms: notice.flagSms,
      flagEmail: notice.flagEmail,
      receiverType: notice.receiverType,
      createDate: notice.createDate != null ? notice.createDate.format(DATE_TIME_FORMAT) : null,
      modifyDate: notice.modifyDate != null ? notice.modifyDate.format(DATE_TIME_FORMAT) : null,
      containerClass: notice.containerClass,
      containerId: notice.containerId,
      noticeType: notice.noticeType,
      flagSent: notice.flagSent,
      sentTime: notice.sentTime != null ? notice.sentTime.format(DATE_TIME_FORMAT) : null,
      payloadObjectUrl: notice.payloadObjectUrl,
      payloadObjectId: notice.payloadObjectId,
      flagDeleted: notice.flagDeleted
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const notice = this.createFromForm();
    if (notice.id !== undefined) {
      this.subscribeToSaveResponse(this.noticeService.update(notice));
    } else {
      this.subscribeToSaveResponse(this.noticeService.create(notice));
    }
  }

  private createFromForm(): INotice {
    return {
      ...new Notice(),
      id: this.editForm.get(['id']).value,
      title: this.editForm.get(['title']).value,
      content: this.editForm.get(['content']).value,
      smsContent: this.editForm.get(['smsContent']).value,
      flagPush: this.editForm.get(['flagPush']).value,
      flagSms: this.editForm.get(['flagSms']).value,
      flagEmail: this.editForm.get(['flagEmail']).value,
      receiverType: this.editForm.get(['receiverType']).value,
      createDate:
        this.editForm.get(['createDate']).value != null ? moment(this.editForm.get(['createDate']).value, DATE_TIME_FORMAT) : undefined,
      modifyDate:
        this.editForm.get(['modifyDate']).value != null ? moment(this.editForm.get(['modifyDate']).value, DATE_TIME_FORMAT) : undefined,
      containerClass: this.editForm.get(['containerClass']).value,
      containerId: this.editForm.get(['containerId']).value,
      noticeType: this.editForm.get(['noticeType']).value,
      flagSent: this.editForm.get(['flagSent']).value,
      sentTime: this.editForm.get(['sentTime']).value != null ? moment(this.editForm.get(['sentTime']).value, DATE_TIME_FORMAT) : undefined,
      payloadObjectUrl: this.editForm.get(['payloadObjectUrl']).value,
      payloadObjectId: this.editForm.get(['payloadObjectId']).value,
      flagDeleted: this.editForm.get(['flagDeleted']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INotice>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}

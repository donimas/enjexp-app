import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { INotice } from 'app/shared/model/notice.model';

type EntityResponseType = HttpResponse<INotice>;
type EntityArrayResponseType = HttpResponse<INotice[]>;

@Injectable({ providedIn: 'root' })
export class NoticeService {
  public resourceUrl = SERVER_API_URL + 'api/notices';

  constructor(protected http: HttpClient) {}

  create(notice: INotice): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notice);
    return this.http
      .post<INotice>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(notice: INotice): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(notice);
    return this.http
      .put<INotice>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<INotice>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<INotice[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(notice: INotice): INotice {
    const copy: INotice = Object.assign({}, notice, {
      createDate: notice.createDate != null && notice.createDate.isValid() ? notice.createDate.toJSON() : null,
      modifyDate: notice.modifyDate != null && notice.modifyDate.isValid() ? notice.modifyDate.toJSON() : null,
      sentTime: notice.sentTime != null && notice.sentTime.isValid() ? notice.sentTime.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createDate = res.body.createDate != null ? moment(res.body.createDate) : null;
      res.body.modifyDate = res.body.modifyDate != null ? moment(res.body.modifyDate) : null;
      res.body.sentTime = res.body.sentTime != null ? moment(res.body.sentTime) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((notice: INotice) => {
        notice.createDate = notice.createDate != null ? moment(notice.createDate) : null;
        notice.modifyDate = notice.modifyDate != null ? moment(notice.modifyDate) : null;
        notice.sentTime = notice.sentTime != null ? moment(notice.sentTime) : null;
      });
    }
    return res;
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { NoticeComponent } from './notice.component';
import { NoticeDetailComponent } from './notice-detail.component';
import { NoticeUpdateComponent } from './notice-update.component';
import { NoticeDeletePopupComponent, NoticeDeleteDialogComponent } from './notice-delete-dialog.component';
import { noticeRoute, noticePopupRoute } from './notice.route';

const ENTITY_STATES = [...noticeRoute, ...noticePopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [NoticeComponent, NoticeDetailComponent, NoticeUpdateComponent, NoticeDeleteDialogComponent, NoticeDeletePopupComponent],
  entryComponents: [NoticeDeleteDialogComponent]
})
export class EnjexpNoticeModule {}

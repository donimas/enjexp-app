import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Notice } from 'app/shared/model/notice.model';
import { NoticeService } from './notice.service';
import { NoticeComponent } from './notice.component';
import { NoticeDetailComponent } from './notice-detail.component';
import { NoticeUpdateComponent } from './notice-update.component';
import { NoticeDeletePopupComponent } from './notice-delete-dialog.component';
import { INotice } from 'app/shared/model/notice.model';

@Injectable({ providedIn: 'root' })
export class NoticeResolve implements Resolve<INotice> {
  constructor(private service: NoticeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INotice> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Notice>) => response.ok),
        map((notice: HttpResponse<Notice>) => notice.body)
      );
    }
    return of(new Notice());
  }
}

export const noticeRoute: Routes = [
  {
    path: '',
    component: NoticeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'enjexpApp.notice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: NoticeDetailComponent,
    resolve: {
      notice: NoticeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.notice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: NoticeUpdateComponent,
    resolve: {
      notice: NoticeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.notice.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: NoticeUpdateComponent,
    resolve: {
      notice: NoticeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.notice.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const noticePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: NoticeDeletePopupComponent,
    resolve: {
      notice: NoticeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.notice.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

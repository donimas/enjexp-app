import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IPromocode, Promocode } from 'app/shared/model/promocode.model';
import { PromocodeService } from './promocode.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';

@Component({
  selector: 'jhi-promocode-update',
  templateUrl: './promocode-update.component.html'
})
export class PromocodeUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  editForm = this.fb.group({
    id: [],
    code: [],
    flagActive: [],
    name: [],
    description: [],
    containerClass: [],
    containerId: [],
    createDate: [],
    user: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected promocodeService: PromocodeService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ promocode }) => {
      this.updateForm(promocode);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(promocode: IPromocode) {
    this.editForm.patchValue({
      id: promocode.id,
      code: promocode.code,
      flagActive: promocode.flagActive,
      name: promocode.name,
      description: promocode.description,
      containerClass: promocode.containerClass,
      containerId: promocode.containerId,
      createDate: promocode.createDate != null ? promocode.createDate.format(DATE_TIME_FORMAT) : null,
      user: promocode.user
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const promocode = this.createFromForm();
    if (promocode.id !== undefined) {
      this.subscribeToSaveResponse(this.promocodeService.update(promocode));
    } else {
      this.subscribeToSaveResponse(this.promocodeService.create(promocode));
    }
  }

  private createFromForm(): IPromocode {
    return {
      ...new Promocode(),
      id: this.editForm.get(['id']).value,
      code: this.editForm.get(['code']).value,
      flagActive: this.editForm.get(['flagActive']).value,
      name: this.editForm.get(['name']).value,
      description: this.editForm.get(['description']).value,
      containerClass: this.editForm.get(['containerClass']).value,
      containerId: this.editForm.get(['containerId']).value,
      createDate:
        this.editForm.get(['createDate']).value != null ? moment(this.editForm.get(['createDate']).value, DATE_TIME_FORMAT) : undefined,
      user: this.editForm.get(['user']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPromocode>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}

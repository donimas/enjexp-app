import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPromocode } from 'app/shared/model/promocode.model';

type EntityResponseType = HttpResponse<IPromocode>;
type EntityArrayResponseType = HttpResponse<IPromocode[]>;

@Injectable({ providedIn: 'root' })
export class PromocodeService {
  public resourceUrl = SERVER_API_URL + 'api/promocodes';

  constructor(protected http: HttpClient) {}

  create(promocode: IPromocode): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(promocode);
    return this.http
      .post<IPromocode>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(promocode: IPromocode): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(promocode);
    return this.http
      .put<IPromocode>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IPromocode>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IPromocode[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(promocode: IPromocode): IPromocode {
    const copy: IPromocode = Object.assign({}, promocode, {
      createDate: promocode.createDate != null && promocode.createDate.isValid() ? promocode.createDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createDate = res.body.createDate != null ? moment(res.body.createDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((promocode: IPromocode) => {
        promocode.createDate = promocode.createDate != null ? moment(promocode.createDate) : null;
      });
    }
    return res;
  }
}

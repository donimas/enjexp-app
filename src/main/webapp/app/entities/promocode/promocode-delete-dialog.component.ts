import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPromocode } from 'app/shared/model/promocode.model';
import { PromocodeService } from './promocode.service';

@Component({
  selector: 'jhi-promocode-delete-dialog',
  templateUrl: './promocode-delete-dialog.component.html'
})
export class PromocodeDeleteDialogComponent {
  promocode: IPromocode;

  constructor(protected promocodeService: PromocodeService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.promocodeService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'promocodeListModification',
        content: 'Deleted an promocode'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-promocode-delete-popup',
  template: ''
})
export class PromocodeDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ promocode }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PromocodeDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.promocode = promocode;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/promocode', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/promocode', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

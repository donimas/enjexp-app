import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { PromocodeComponent } from './promocode.component';
import { PromocodeDetailComponent } from './promocode-detail.component';
import { PromocodeUpdateComponent } from './promocode-update.component';
import { PromocodeDeletePopupComponent, PromocodeDeleteDialogComponent } from './promocode-delete-dialog.component';
import { promocodeRoute, promocodePopupRoute } from './promocode.route';

const ENTITY_STATES = [...promocodeRoute, ...promocodePopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PromocodeComponent,
    PromocodeDetailComponent,
    PromocodeUpdateComponent,
    PromocodeDeleteDialogComponent,
    PromocodeDeletePopupComponent
  ],
  entryComponents: [PromocodeDeleteDialogComponent]
})
export class EnjexpPromocodeModule {}

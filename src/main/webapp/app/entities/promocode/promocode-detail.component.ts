import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPromocode } from 'app/shared/model/promocode.model';

@Component({
  selector: 'jhi-promocode-detail',
  templateUrl: './promocode-detail.component.html'
})
export class PromocodeDetailComponent implements OnInit {
  promocode: IPromocode;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ promocode }) => {
      this.promocode = promocode;
    });
  }

  previousState() {
    window.history.back();
  }
}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Promocode } from 'app/shared/model/promocode.model';
import { PromocodeService } from './promocode.service';
import { PromocodeComponent } from './promocode.component';
import { PromocodeDetailComponent } from './promocode-detail.component';
import { PromocodeUpdateComponent } from './promocode-update.component';
import { PromocodeDeletePopupComponent } from './promocode-delete-dialog.component';
import { IPromocode } from 'app/shared/model/promocode.model';

@Injectable({ providedIn: 'root' })
export class PromocodeResolve implements Resolve<IPromocode> {
  constructor(private service: PromocodeService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPromocode> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Promocode>) => response.ok),
        map((promocode: HttpResponse<Promocode>) => promocode.body)
      );
    }
    return of(new Promocode());
  }
}

export const promocodeRoute: Routes = [
  {
    path: '',
    component: PromocodeComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'enjexpApp.promocode.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PromocodeDetailComponent,
    resolve: {
      promocode: PromocodeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.promocode.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PromocodeUpdateComponent,
    resolve: {
      promocode: PromocodeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.promocode.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PromocodeUpdateComponent,
    resolve: {
      promocode: PromocodeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.promocode.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const promocodePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PromocodeDeletePopupComponent,
    resolve: {
      promocode: PromocodeResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.promocode.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

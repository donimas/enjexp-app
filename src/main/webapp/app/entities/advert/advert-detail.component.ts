import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IAdvert } from 'app/shared/model/advert.model';

@Component({
  selector: 'jhi-advert-detail',
  templateUrl: './advert-detail.component.html'
})
export class AdvertDetailComponent implements OnInit {
  advert: IAdvert;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ advert }) => {
      this.advert = advert;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}

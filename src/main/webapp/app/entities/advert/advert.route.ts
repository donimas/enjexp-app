import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Advert } from 'app/shared/model/advert.model';
import { AdvertService } from './advert.service';
import { AdvertComponent } from './advert.component';
import { AdvertDetailComponent } from './advert-detail.component';
import { AdvertUpdateComponent } from './advert-update.component';
import { AdvertDeletePopupComponent } from './advert-delete-dialog.component';
import { IAdvert } from 'app/shared/model/advert.model';

@Injectable({ providedIn: 'root' })
export class AdvertResolve implements Resolve<IAdvert> {
  constructor(private service: AdvertService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IAdvert> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Advert>) => response.ok),
        map((advert: HttpResponse<Advert>) => advert.body)
      );
    }
    return of(new Advert());
  }
}

export const advertRoute: Routes = [
  {
    path: '',
    component: AdvertComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'enjexpApp.advert.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: AdvertDetailComponent,
    resolve: {
      advert: AdvertResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.advert.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: AdvertUpdateComponent,
    resolve: {
      advert: AdvertResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.advert.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: AdvertUpdateComponent,
    resolve: {
      advert: AdvertResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.advert.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const advertPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: AdvertDeletePopupComponent,
    resolve: {
      advert: AdvertResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.advert.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

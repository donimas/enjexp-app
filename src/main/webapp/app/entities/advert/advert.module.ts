import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { AdvertComponent } from './advert.component';
import { AdvertDetailComponent } from './advert-detail.component';
import { AdvertUpdateComponent } from './advert-update.component';
import { AdvertDeletePopupComponent, AdvertDeleteDialogComponent } from './advert-delete-dialog.component';
import { advertRoute, advertPopupRoute } from './advert.route';

const ENTITY_STATES = [...advertRoute, ...advertPopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [AdvertComponent, AdvertDetailComponent, AdvertUpdateComponent, AdvertDeleteDialogComponent, AdvertDeletePopupComponent],
  entryComponents: [AdvertDeleteDialogComponent]
})
export class EnjexpAdvertModule {}

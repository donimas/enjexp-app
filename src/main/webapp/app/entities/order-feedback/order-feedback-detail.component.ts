import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOrderFeedback } from 'app/shared/model/order-feedback.model';

@Component({
  selector: 'jhi-order-feedback-detail',
  templateUrl: './order-feedback-detail.component.html'
})
export class OrderFeedbackDetailComponent implements OnInit {
  orderFeedback: IOrderFeedback;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ orderFeedback }) => {
      this.orderFeedback = orderFeedback;
    });
  }

  previousState() {
    window.history.back();
  }
}

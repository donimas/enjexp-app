import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IOrderFeedback, OrderFeedback } from 'app/shared/model/order-feedback.model';
import { OrderFeedbackService } from './order-feedback.service';
import { IOrder } from 'app/shared/model/order.model';
import { OrderService } from 'app/entities/order/order.service';
import { IRatingTag } from 'app/shared/model/rating-tag.model';
import { RatingTagService } from 'app/entities/rating-tag/rating-tag.service';

@Component({
  selector: 'jhi-order-feedback-update',
  templateUrl: './order-feedback-update.component.html'
})
export class OrderFeedbackUpdateComponent implements OnInit {
  isSaving: boolean;

  orders: IOrder[];

  ratingtags: IRatingTag[];

  editForm = this.fb.group({
    id: [],
    rating: [],
    comment: [],
    createDate: [],
    order: [],
    ratingTags: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected orderFeedbackService: OrderFeedbackService,
    protected orderService: OrderService,
    protected ratingTagService: RatingTagService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ orderFeedback }) => {
      this.updateForm(orderFeedback);
    });
    this.orderService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IOrder[]>) => mayBeOk.ok),
        map((response: HttpResponse<IOrder[]>) => response.body)
      )
      .subscribe((res: IOrder[]) => (this.orders = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.ratingTagService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IRatingTag[]>) => mayBeOk.ok),
        map((response: HttpResponse<IRatingTag[]>) => response.body)
      )
      .subscribe((res: IRatingTag[]) => (this.ratingtags = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(orderFeedback: IOrderFeedback) {
    this.editForm.patchValue({
      id: orderFeedback.id,
      rating: orderFeedback.rating,
      comment: orderFeedback.comment,
      createDate: orderFeedback.createDate != null ? orderFeedback.createDate.format(DATE_TIME_FORMAT) : null,
      order: orderFeedback.order,
      ratingTags: orderFeedback.ratingTags
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const orderFeedback = this.createFromForm();
    if (orderFeedback.id !== undefined) {
      this.subscribeToSaveResponse(this.orderFeedbackService.update(orderFeedback));
    } else {
      this.subscribeToSaveResponse(this.orderFeedbackService.create(orderFeedback));
    }
  }

  private createFromForm(): IOrderFeedback {
    return {
      ...new OrderFeedback(),
      id: this.editForm.get(['id']).value,
      rating: this.editForm.get(['rating']).value,
      comment: this.editForm.get(['comment']).value,
      createDate:
        this.editForm.get(['createDate']).value != null ? moment(this.editForm.get(['createDate']).value, DATE_TIME_FORMAT) : undefined,
      order: this.editForm.get(['order']).value,
      ratingTags: this.editForm.get(['ratingTags']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderFeedback>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackOrderById(index: number, item: IOrder) {
    return item.id;
  }

  trackRatingTagById(index: number, item: IRatingTag) {
    return item.id;
  }

  getSelected(selectedVals: any[], option: any) {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}

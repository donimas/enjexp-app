import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { OrderFeedbackComponent } from './order-feedback.component';
import { OrderFeedbackDetailComponent } from './order-feedback-detail.component';
import { OrderFeedbackUpdateComponent } from './order-feedback-update.component';
import { OrderFeedbackDeletePopupComponent, OrderFeedbackDeleteDialogComponent } from './order-feedback-delete-dialog.component';
import { orderFeedbackRoute, orderFeedbackPopupRoute } from './order-feedback.route';

const ENTITY_STATES = [...orderFeedbackRoute, ...orderFeedbackPopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OrderFeedbackComponent,
    OrderFeedbackDetailComponent,
    OrderFeedbackUpdateComponent,
    OrderFeedbackDeleteDialogComponent,
    OrderFeedbackDeletePopupComponent
  ],
  entryComponents: [OrderFeedbackDeleteDialogComponent]
})
export class EnjexpOrderFeedbackModule {}

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { OrderFeedback } from 'app/shared/model/order-feedback.model';
import { OrderFeedbackService } from './order-feedback.service';
import { OrderFeedbackComponent } from './order-feedback.component';
import { OrderFeedbackDetailComponent } from './order-feedback-detail.component';
import { OrderFeedbackUpdateComponent } from './order-feedback-update.component';
import { OrderFeedbackDeletePopupComponent } from './order-feedback-delete-dialog.component';
import { IOrderFeedback } from 'app/shared/model/order-feedback.model';

@Injectable({ providedIn: 'root' })
export class OrderFeedbackResolve implements Resolve<IOrderFeedback> {
  constructor(private service: OrderFeedbackService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IOrderFeedback> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<OrderFeedback>) => response.ok),
        map((orderFeedback: HttpResponse<OrderFeedback>) => orderFeedback.body)
      );
    }
    return of(new OrderFeedback());
  }
}

export const orderFeedbackRoute: Routes = [
  {
    path: '',
    component: OrderFeedbackComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'enjexpApp.orderFeedback.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: OrderFeedbackDetailComponent,
    resolve: {
      orderFeedback: OrderFeedbackResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.orderFeedback.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: OrderFeedbackUpdateComponent,
    resolve: {
      orderFeedback: OrderFeedbackResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.orderFeedback.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: OrderFeedbackUpdateComponent,
    resolve: {
      orderFeedback: OrderFeedbackResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.orderFeedback.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const orderFeedbackPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: OrderFeedbackDeletePopupComponent,
    resolve: {
      orderFeedback: OrderFeedbackResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.orderFeedback.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

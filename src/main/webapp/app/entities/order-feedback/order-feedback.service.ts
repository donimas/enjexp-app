import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IOrderFeedback } from 'app/shared/model/order-feedback.model';

type EntityResponseType = HttpResponse<IOrderFeedback>;
type EntityArrayResponseType = HttpResponse<IOrderFeedback[]>;

@Injectable({ providedIn: 'root' })
export class OrderFeedbackService {
  public resourceUrl = SERVER_API_URL + 'api/order-feedbacks';

  constructor(protected http: HttpClient) {}

  create(orderFeedback: IOrderFeedback): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderFeedback);
    return this.http
      .post<IOrderFeedback>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(orderFeedback: IOrderFeedback): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(orderFeedback);
    return this.http
      .put<IOrderFeedback>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IOrderFeedback>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IOrderFeedback[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(orderFeedback: IOrderFeedback): IOrderFeedback {
    const copy: IOrderFeedback = Object.assign({}, orderFeedback, {
      createDate: orderFeedback.createDate != null && orderFeedback.createDate.isValid() ? orderFeedback.createDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createDate = res.body.createDate != null ? moment(res.body.createDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((orderFeedback: IOrderFeedback) => {
        orderFeedback.createDate = orderFeedback.createDate != null ? moment(orderFeedback.createDate) : null;
      });
    }
    return res;
  }
}

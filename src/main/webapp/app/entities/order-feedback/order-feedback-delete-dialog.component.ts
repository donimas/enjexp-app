import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IOrderFeedback } from 'app/shared/model/order-feedback.model';
import { OrderFeedbackService } from './order-feedback.service';

@Component({
  selector: 'jhi-order-feedback-delete-dialog',
  templateUrl: './order-feedback-delete-dialog.component.html'
})
export class OrderFeedbackDeleteDialogComponent {
  orderFeedback: IOrderFeedback;

  constructor(
    protected orderFeedbackService: OrderFeedbackService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.orderFeedbackService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'orderFeedbackListModification',
        content: 'Deleted an orderFeedback'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-order-feedback-delete-popup',
  template: ''
})
export class OrderFeedbackDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ orderFeedback }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(OrderFeedbackDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.orderFeedback = orderFeedback;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/order-feedback', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/order-feedback', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

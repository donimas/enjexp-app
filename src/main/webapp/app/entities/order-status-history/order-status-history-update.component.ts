import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IOrderStatusHistory, OrderStatusHistory } from 'app/shared/model/order-status-history.model';
import { OrderStatusHistoryService } from './order-status-history.service';
import { IOrder } from 'app/shared/model/order.model';
import { OrderService } from 'app/entities/order/order.service';

@Component({
  selector: 'jhi-order-status-history-update',
  templateUrl: './order-status-history-update.component.html'
})
export class OrderStatusHistoryUpdateComponent implements OnInit {
  isSaving: boolean;

  orders: IOrder[];

  editForm = this.fb.group({
    id: [],
    orderStatus: [],
    createDate: [],
    comment: [],
    order: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected orderStatusHistoryService: OrderStatusHistoryService,
    protected orderService: OrderService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ orderStatusHistory }) => {
      this.updateForm(orderStatusHistory);
    });
    this.orderService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IOrder[]>) => mayBeOk.ok),
        map((response: HttpResponse<IOrder[]>) => response.body)
      )
      .subscribe((res: IOrder[]) => (this.orders = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(orderStatusHistory: IOrderStatusHistory) {
    this.editForm.patchValue({
      id: orderStatusHistory.id,
      orderStatus: orderStatusHistory.orderStatus,
      createDate: orderStatusHistory.createDate != null ? orderStatusHistory.createDate.format(DATE_TIME_FORMAT) : null,
      comment: orderStatusHistory.comment,
      order: orderStatusHistory.order
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const orderStatusHistory = this.createFromForm();
    if (orderStatusHistory.id !== undefined) {
      this.subscribeToSaveResponse(this.orderStatusHistoryService.update(orderStatusHistory));
    } else {
      this.subscribeToSaveResponse(this.orderStatusHistoryService.create(orderStatusHistory));
    }
  }

  private createFromForm(): IOrderStatusHistory {
    return {
      ...new OrderStatusHistory(),
      id: this.editForm.get(['id']).value,
      orderStatus: this.editForm.get(['orderStatus']).value,
      createDate:
        this.editForm.get(['createDate']).value != null ? moment(this.editForm.get(['createDate']).value, DATE_TIME_FORMAT) : undefined,
      comment: this.editForm.get(['comment']).value,
      order: this.editForm.get(['order']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrderStatusHistory>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackOrderById(index: number, item: IOrder) {
    return item.id;
  }
}

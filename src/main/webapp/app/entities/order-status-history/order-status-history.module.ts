import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { OrderStatusHistoryComponent } from './order-status-history.component';
import { OrderStatusHistoryDetailComponent } from './order-status-history-detail.component';
import { OrderStatusHistoryUpdateComponent } from './order-status-history-update.component';
import {
  OrderStatusHistoryDeletePopupComponent,
  OrderStatusHistoryDeleteDialogComponent
} from './order-status-history-delete-dialog.component';
import { orderStatusHistoryRoute, orderStatusHistoryPopupRoute } from './order-status-history.route';

const ENTITY_STATES = [...orderStatusHistoryRoute, ...orderStatusHistoryPopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    OrderStatusHistoryComponent,
    OrderStatusHistoryDetailComponent,
    OrderStatusHistoryUpdateComponent,
    OrderStatusHistoryDeleteDialogComponent,
    OrderStatusHistoryDeletePopupComponent
  ],
  entryComponents: [OrderStatusHistoryDeleteDialogComponent]
})
export class EnjexpOrderStatusHistoryModule {}

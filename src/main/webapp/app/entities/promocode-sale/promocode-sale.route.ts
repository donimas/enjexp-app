import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PromocodeSale } from 'app/shared/model/promocode-sale.model';
import { PromocodeSaleService } from './promocode-sale.service';
import { PromocodeSaleComponent } from './promocode-sale.component';
import { PromocodeSaleDetailComponent } from './promocode-sale-detail.component';
import { PromocodeSaleUpdateComponent } from './promocode-sale-update.component';
import { PromocodeSaleDeletePopupComponent } from './promocode-sale-delete-dialog.component';
import { IPromocodeSale } from 'app/shared/model/promocode-sale.model';

@Injectable({ providedIn: 'root' })
export class PromocodeSaleResolve implements Resolve<IPromocodeSale> {
  constructor(private service: PromocodeSaleService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPromocodeSale> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PromocodeSale>) => response.ok),
        map((promocodeSale: HttpResponse<PromocodeSale>) => promocodeSale.body)
      );
    }
    return of(new PromocodeSale());
  }
}

export const promocodeSaleRoute: Routes = [
  {
    path: '',
    component: PromocodeSaleComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'enjexpApp.promocodeSale.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PromocodeSaleDetailComponent,
    resolve: {
      promocodeSale: PromocodeSaleResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.promocodeSale.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PromocodeSaleUpdateComponent,
    resolve: {
      promocodeSale: PromocodeSaleResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.promocodeSale.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PromocodeSaleUpdateComponent,
    resolve: {
      promocodeSale: PromocodeSaleResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.promocodeSale.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const promocodeSalePopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PromocodeSaleDeletePopupComponent,
    resolve: {
      promocodeSale: PromocodeSaleResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.promocodeSale.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

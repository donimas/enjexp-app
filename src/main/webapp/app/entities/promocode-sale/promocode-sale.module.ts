import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { PromocodeSaleComponent } from './promocode-sale.component';
import { PromocodeSaleDetailComponent } from './promocode-sale-detail.component';
import { PromocodeSaleUpdateComponent } from './promocode-sale-update.component';
import { PromocodeSaleDeletePopupComponent, PromocodeSaleDeleteDialogComponent } from './promocode-sale-delete-dialog.component';
import { promocodeSaleRoute, promocodeSalePopupRoute } from './promocode-sale.route';

const ENTITY_STATES = [...promocodeSaleRoute, ...promocodeSalePopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PromocodeSaleComponent,
    PromocodeSaleDetailComponent,
    PromocodeSaleUpdateComponent,
    PromocodeSaleDeleteDialogComponent,
    PromocodeSaleDeletePopupComponent
  ],
  entryComponents: [PromocodeSaleDeleteDialogComponent]
})
export class EnjexpPromocodeSaleModule {}

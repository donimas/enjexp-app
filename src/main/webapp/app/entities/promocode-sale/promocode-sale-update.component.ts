import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IPromocodeSale, PromocodeSale } from 'app/shared/model/promocode-sale.model';
import { PromocodeSaleService } from './promocode-sale.service';
import { IPromocode } from 'app/shared/model/promocode.model';
import { PromocodeService } from 'app/entities/promocode/promocode.service';

@Component({
  selector: 'jhi-promocode-sale-update',
  templateUrl: './promocode-sale-update.component.html'
})
export class PromocodeSaleUpdateComponent implements OnInit {
  isSaving: boolean;

  promocodes: IPromocode[];

  editForm = this.fb.group({
    id: [],
    sale: [],
    minPrice: [],
    maxPrice: [],
    minAmount: [],
    maxAmount: [],
    minShare: [],
    startDate: [],
    endDate: [],
    promocode: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected promocodeSaleService: PromocodeSaleService,
    protected promocodeService: PromocodeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ promocodeSale }) => {
      this.updateForm(promocodeSale);
    });
    this.promocodeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPromocode[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPromocode[]>) => response.body)
      )
      .subscribe((res: IPromocode[]) => (this.promocodes = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(promocodeSale: IPromocodeSale) {
    this.editForm.patchValue({
      id: promocodeSale.id,
      sale: promocodeSale.sale,
      minPrice: promocodeSale.minPrice,
      maxPrice: promocodeSale.maxPrice,
      minAmount: promocodeSale.minAmount,
      maxAmount: promocodeSale.maxAmount,
      minShare: promocodeSale.minShare,
      startDate: promocodeSale.startDate != null ? promocodeSale.startDate.format(DATE_TIME_FORMAT) : null,
      endDate: promocodeSale.endDate != null ? promocodeSale.endDate.format(DATE_TIME_FORMAT) : null,
      promocode: promocodeSale.promocode
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const promocodeSale = this.createFromForm();
    if (promocodeSale.id !== undefined) {
      this.subscribeToSaveResponse(this.promocodeSaleService.update(promocodeSale));
    } else {
      this.subscribeToSaveResponse(this.promocodeSaleService.create(promocodeSale));
    }
  }

  private createFromForm(): IPromocodeSale {
    return {
      ...new PromocodeSale(),
      id: this.editForm.get(['id']).value,
      sale: this.editForm.get(['sale']).value,
      minPrice: this.editForm.get(['minPrice']).value,
      maxPrice: this.editForm.get(['maxPrice']).value,
      minAmount: this.editForm.get(['minAmount']).value,
      maxAmount: this.editForm.get(['maxAmount']).value,
      minShare: this.editForm.get(['minShare']).value,
      startDate:
        this.editForm.get(['startDate']).value != null ? moment(this.editForm.get(['startDate']).value, DATE_TIME_FORMAT) : undefined,
      endDate: this.editForm.get(['endDate']).value != null ? moment(this.editForm.get(['endDate']).value, DATE_TIME_FORMAT) : undefined,
      promocode: this.editForm.get(['promocode']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPromocodeSale>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackPromocodeById(index: number, item: IPromocode) {
    return item.id;
  }
}

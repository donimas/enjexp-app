import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IPromocodeSale } from 'app/shared/model/promocode-sale.model';

@Component({
  selector: 'jhi-promocode-sale-detail',
  templateUrl: './promocode-sale-detail.component.html'
})
export class PromocodeSaleDetailComponent implements OnInit {
  promocodeSale: IPromocodeSale;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ promocodeSale }) => {
      this.promocodeSale = promocodeSale;
    });
  }

  previousState() {
    window.history.back();
  }
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPromocodeSale } from 'app/shared/model/promocode-sale.model';
import { PromocodeSaleService } from './promocode-sale.service';

@Component({
  selector: 'jhi-promocode-sale-delete-dialog',
  templateUrl: './promocode-sale-delete-dialog.component.html'
})
export class PromocodeSaleDeleteDialogComponent {
  promocodeSale: IPromocodeSale;

  constructor(
    protected promocodeSaleService: PromocodeSaleService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.promocodeSaleService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'promocodeSaleListModification',
        content: 'Deleted an promocodeSale'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-promocode-sale-delete-popup',
  template: ''
})
export class PromocodeSaleDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ promocodeSale }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PromocodeSaleDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.promocodeSale = promocodeSale;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/promocode-sale', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/promocode-sale', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

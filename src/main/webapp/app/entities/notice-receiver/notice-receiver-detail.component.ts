import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { INoticeReceiver } from 'app/shared/model/notice-receiver.model';

@Component({
  selector: 'jhi-notice-receiver-detail',
  templateUrl: './notice-receiver-detail.component.html'
})
export class NoticeReceiverDetailComponent implements OnInit {
  noticeReceiver: INoticeReceiver;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ noticeReceiver }) => {
      this.noticeReceiver = noticeReceiver;
    });
  }

  previousState() {
    window.history.back();
  }
}

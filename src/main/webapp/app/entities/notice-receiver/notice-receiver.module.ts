import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { NoticeReceiverComponent } from './notice-receiver.component';
import { NoticeReceiverDetailComponent } from './notice-receiver-detail.component';
import { NoticeReceiverUpdateComponent } from './notice-receiver-update.component';
import { NoticeReceiverDeletePopupComponent, NoticeReceiverDeleteDialogComponent } from './notice-receiver-delete-dialog.component';
import { noticeReceiverRoute, noticeReceiverPopupRoute } from './notice-receiver.route';

const ENTITY_STATES = [...noticeReceiverRoute, ...noticeReceiverPopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    NoticeReceiverComponent,
    NoticeReceiverDetailComponent,
    NoticeReceiverUpdateComponent,
    NoticeReceiverDeleteDialogComponent,
    NoticeReceiverDeletePopupComponent
  ],
  entryComponents: [NoticeReceiverDeleteDialogComponent]
})
export class EnjexpNoticeReceiverModule {}

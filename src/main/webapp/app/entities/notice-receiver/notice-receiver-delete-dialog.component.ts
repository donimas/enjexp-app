import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { INoticeReceiver } from 'app/shared/model/notice-receiver.model';
import { NoticeReceiverService } from './notice-receiver.service';

@Component({
  selector: 'jhi-notice-receiver-delete-dialog',
  templateUrl: './notice-receiver-delete-dialog.component.html'
})
export class NoticeReceiverDeleteDialogComponent {
  noticeReceiver: INoticeReceiver;

  constructor(
    protected noticeReceiverService: NoticeReceiverService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.noticeReceiverService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'noticeReceiverListModification',
        content: 'Deleted an noticeReceiver'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-notice-receiver-delete-popup',
  template: ''
})
export class NoticeReceiverDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ noticeReceiver }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(NoticeReceiverDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.noticeReceiver = noticeReceiver;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/notice-receiver', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/notice-receiver', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

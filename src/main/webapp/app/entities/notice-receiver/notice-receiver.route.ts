import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NoticeReceiver } from 'app/shared/model/notice-receiver.model';
import { NoticeReceiverService } from './notice-receiver.service';
import { NoticeReceiverComponent } from './notice-receiver.component';
import { NoticeReceiverDetailComponent } from './notice-receiver-detail.component';
import { NoticeReceiverUpdateComponent } from './notice-receiver-update.component';
import { NoticeReceiverDeletePopupComponent } from './notice-receiver-delete-dialog.component';
import { INoticeReceiver } from 'app/shared/model/notice-receiver.model';

@Injectable({ providedIn: 'root' })
export class NoticeReceiverResolve implements Resolve<INoticeReceiver> {
  constructor(private service: NoticeReceiverService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<INoticeReceiver> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<NoticeReceiver>) => response.ok),
        map((noticeReceiver: HttpResponse<NoticeReceiver>) => noticeReceiver.body)
      );
    }
    return of(new NoticeReceiver());
  }
}

export const noticeReceiverRoute: Routes = [
  {
    path: '',
    component: NoticeReceiverComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'enjexpApp.noticeReceiver.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: NoticeReceiverDetailComponent,
    resolve: {
      noticeReceiver: NoticeReceiverResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.noticeReceiver.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: NoticeReceiverUpdateComponent,
    resolve: {
      noticeReceiver: NoticeReceiverResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.noticeReceiver.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: NoticeReceiverUpdateComponent,
    resolve: {
      noticeReceiver: NoticeReceiverResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.noticeReceiver.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const noticeReceiverPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: NoticeReceiverDeletePopupComponent,
    resolve: {
      noticeReceiver: NoticeReceiverResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.noticeReceiver.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

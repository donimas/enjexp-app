import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { INoticeReceiver } from 'app/shared/model/notice-receiver.model';

type EntityResponseType = HttpResponse<INoticeReceiver>;
type EntityArrayResponseType = HttpResponse<INoticeReceiver[]>;

@Injectable({ providedIn: 'root' })
export class NoticeReceiverService {
  public resourceUrl = SERVER_API_URL + 'api/notice-receivers';

  constructor(protected http: HttpClient) {}

  create(noticeReceiver: INoticeReceiver): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(noticeReceiver);
    return this.http
      .post<INoticeReceiver>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(noticeReceiver: INoticeReceiver): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(noticeReceiver);
    return this.http
      .put<INoticeReceiver>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<INoticeReceiver>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<INoticeReceiver[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(noticeReceiver: INoticeReceiver): INoticeReceiver {
    const copy: INoticeReceiver = Object.assign({}, noticeReceiver, {
      readDate: noticeReceiver.readDate != null && noticeReceiver.readDate.isValid() ? noticeReceiver.readDate.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.readDate = res.body.readDate != null ? moment(res.body.readDate) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((noticeReceiver: INoticeReceiver) => {
        noticeReceiver.readDate = noticeReceiver.readDate != null ? moment(noticeReceiver.readDate) : null;
      });
    }
    return res;
  }
}

import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { INoticeReceiver, NoticeReceiver } from 'app/shared/model/notice-receiver.model';
import { NoticeReceiverService } from './notice-receiver.service';
import { INotice } from 'app/shared/model/notice.model';
import { NoticeService } from 'app/entities/notice/notice.service';

@Component({
  selector: 'jhi-notice-receiver-update',
  templateUrl: './notice-receiver-update.component.html'
})
export class NoticeReceiverUpdateComponent implements OnInit {
  isSaving: boolean;

  notices: INotice[];

  editForm = this.fb.group({
    id: [],
    toUserId: [],
    userLang: [],
    flagSmsSent: [],
    flagEmailSent: [],
    flagPushSent: [],
    flagRead: [],
    readDate: [],
    receiverFullName: [],
    receiverPhone: [],
    notice: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected noticeReceiverService: NoticeReceiverService,
    protected noticeService: NoticeService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ noticeReceiver }) => {
      this.updateForm(noticeReceiver);
    });
    this.noticeService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<INotice[]>) => mayBeOk.ok),
        map((response: HttpResponse<INotice[]>) => response.body)
      )
      .subscribe((res: INotice[]) => (this.notices = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(noticeReceiver: INoticeReceiver) {
    this.editForm.patchValue({
      id: noticeReceiver.id,
      toUserId: noticeReceiver.toUserId,
      userLang: noticeReceiver.userLang,
      flagSmsSent: noticeReceiver.flagSmsSent,
      flagEmailSent: noticeReceiver.flagEmailSent,
      flagPushSent: noticeReceiver.flagPushSent,
      flagRead: noticeReceiver.flagRead,
      readDate: noticeReceiver.readDate != null ? noticeReceiver.readDate.format(DATE_TIME_FORMAT) : null,
      receiverFullName: noticeReceiver.receiverFullName,
      receiverPhone: noticeReceiver.receiverPhone,
      notice: noticeReceiver.notice
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const noticeReceiver = this.createFromForm();
    if (noticeReceiver.id !== undefined) {
      this.subscribeToSaveResponse(this.noticeReceiverService.update(noticeReceiver));
    } else {
      this.subscribeToSaveResponse(this.noticeReceiverService.create(noticeReceiver));
    }
  }

  private createFromForm(): INoticeReceiver {
    return {
      ...new NoticeReceiver(),
      id: this.editForm.get(['id']).value,
      toUserId: this.editForm.get(['toUserId']).value,
      userLang: this.editForm.get(['userLang']).value,
      flagSmsSent: this.editForm.get(['flagSmsSent']).value,
      flagEmailSent: this.editForm.get(['flagEmailSent']).value,
      flagPushSent: this.editForm.get(['flagPushSent']).value,
      flagRead: this.editForm.get(['flagRead']).value,
      readDate: this.editForm.get(['readDate']).value != null ? moment(this.editForm.get(['readDate']).value, DATE_TIME_FORMAT) : undefined,
      receiverFullName: this.editForm.get(['receiverFullName']).value,
      receiverPhone: this.editForm.get(['receiverPhone']).value,
      notice: this.editForm.get(['notice']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<INoticeReceiver>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackNoticeById(index: number, item: INotice) {
    return item.id;
  }
}

import { Component, OnInit, ElementRef } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IMenu, Menu } from 'app/shared/model/menu.model';
import { MenuService } from './menu.service';
import { ICategory } from 'app/shared/model/category.model';
import { CategoryService } from 'app/entities/category/category.service';
import { IPriceHistory, PriceHistory } from 'app/shared/model/price-history.model';
import { PriceHistoryService } from 'app/entities/price-history/price-history.service';

@Component({
  selector: 'jhi-menu-update',
  templateUrl: './menu-update.component.html'
})
export class MenuUpdateComponent implements OnInit {
  isSaving: boolean;

  categories: ICategory[];

  pricehistories: IPriceHistory[];

  menus: IMenu[];

  editForm = this.fb.group({
    id: [],
    name: [],
    description: [],
    image: [],
    imageContentType: [],
    createDate: [],
    flagDeleted: [],
    onStop: [],
    spicy: [],
    content: [],
    prepDuration: [],
    category: [],
    price: [],
    combo: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected menuService: MenuService,
    protected categoryService: CategoryService,
    protected priceHistoryService: PriceHistoryService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ menu }) => {
      this.updateForm(menu);
    });
    this.categoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICategory[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICategory[]>) => response.body)
      )
      .subscribe((res: ICategory[]) => (this.categories = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.priceHistoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IPriceHistory[]>) => mayBeOk.ok),
        map((response: HttpResponse<IPriceHistory[]>) => response.body)
      )
      .subscribe((res: IPriceHistory[]) => (this.pricehistories = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.menuService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMenu[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMenu[]>) => response.body)
      )
      .subscribe((res: IMenu[]) => (this.menus = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(menu: IMenu) {
    this.editForm.patchValue({
      id: menu.id,
      name: menu.name,
      description: menu.description,
      image: menu.image,
      imageContentType: menu.imageContentType,
      createDate: menu.createDate != null ? menu.createDate.format(DATE_TIME_FORMAT) : null,
      flagDeleted: menu.flagDeleted,
      onStop: menu.onStop,
      spicy: menu.spicy,
      content: menu.content,
      prepDuration: menu.prepDuration,
      category: menu.category,
      price: menu.price ? menu.price.price : null,
      combo: menu.combo
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file: File = event.target.files[0];
        if (isImage && !file.type.startsWith('image/')) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      // eslint-disable-next-line no-console
      () => console.log('blob added'), // success
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const menu = this.createFromForm();
    if (!menu.price) {
      menu.price = new PriceHistory();
    }
    menu.price.price = this.editForm.get(['price']).value;
    if (menu.id !== undefined) {
      this.subscribeToSaveResponse(this.menuService.update(menu));
    } else {
      this.subscribeToSaveResponse(this.menuService.create(menu));
    }
  }

  private createFromForm(): IMenu {
    return {
      ...new Menu(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      description: this.editForm.get(['description']).value,
      imageContentType: this.editForm.get(['imageContentType']).value,
      image: this.editForm.get(['image']).value,
      createDate:
        this.editForm.get(['createDate']).value != null ? moment(this.editForm.get(['createDate']).value, DATE_TIME_FORMAT) : undefined,
      flagDeleted: this.editForm.get(['flagDeleted']).value,
      content: this.editForm.get(['content']).value,
      prepDuration: this.editForm.get(['prepDuration']).value,
      category: this.editForm.get(['category']).value,
      onStop: this.editForm.get(['onStop']).value,
      spicy: this.editForm.get(['spicy']).value,
      combo: this.editForm.get(['combo']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMenu>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCategoryById(index: number, item: ICategory) {
    return item.id;
  }

  trackPriceHistoryById(index: number, item: IPriceHistory) {
    return item.id;
  }

  trackMenuById(index: number, item: IMenu) {
    return item.id;
  }
}

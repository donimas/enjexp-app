import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IOrder, Order } from 'app/shared/model/order.model';
import { OrderService } from './order.service';
import { IUser } from 'app/core/user/user.model';
import { UserService } from 'app/core/user/user.service';
import { IOrderStatusHistory } from 'app/shared/model/order-status-history.model';
import { OrderStatusHistoryService } from 'app/entities/order-status-history/order-status-history.service';
import { ILocation } from 'app/shared/model/location.model';
import { LocationService } from 'app/entities/location/location.service';

@Component({
  selector: 'jhi-order-update',
  templateUrl: './order-update.component.html'
})
export class OrderUpdateComponent implements OnInit {
  isSaving: boolean;

  users: IUser[];

  orderstatushistories: IOrderStatusHistory[];

  locations: ILocation[];

  editForm = this.fb.group({
    id: [],
    promocode: [],
    totalPrice: [],
    change: [],
    paid: [],
    paymentType: [],
    paymentCode: [],
    flagPaid: [],
    serviceType: [],
    serviceTime: [],
    createDate: [],
    comment: [],
    locationType: [],
    deliveryDuration: [],
    user: [],
    status: [],
    location: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected orderService: OrderService,
    protected userService: UserService,
    protected orderStatusHistoryService: OrderStatusHistoryService,
    protected locationService: LocationService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ order }) => {
      this.updateForm(order);
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.orderStatusHistoryService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IOrderStatusHistory[]>) => mayBeOk.ok),
        map((response: HttpResponse<IOrderStatusHistory[]>) => response.body)
      )
      .subscribe((res: IOrderStatusHistory[]) => (this.orderstatushistories = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.locationService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ILocation[]>) => mayBeOk.ok),
        map((response: HttpResponse<ILocation[]>) => response.body)
      )
      .subscribe((res: ILocation[]) => (this.locations = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(order: IOrder) {
    this.editForm.patchValue({
      id: order.id,
      promocode: order.promocode,
      totalPrice: order.totalPrice,
      change: order.change,
      paid: order.paid,
      paymentType: order.paymentType,
      paymentCode: order.paymentCode,
      flagPaid: order.flagPaid,
      serviceType: order.serviceType,
      serviceTime: order.serviceTime != null ? order.serviceTime.format(DATE_TIME_FORMAT) : null,
      createDate: order.createDate != null ? order.createDate.format(DATE_TIME_FORMAT) : null,
      comment: order.comment,
      locationType: order.locationType,
      deliveryDuration: order.deliveryDuration,
      user: order.user,
      status: order.status,
      location: order.location
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const order = this.createFromForm();
    if (order.id !== undefined) {
      this.subscribeToSaveResponse(this.orderService.update(order));
    } else {
      this.subscribeToSaveResponse(this.orderService.create(order));
    }
  }

  private createFromForm(): IOrder {
    return {
      ...new Order(),
      id: this.editForm.get(['id']).value,
      promocode: this.editForm.get(['promocode']).value,
      totalPrice: this.editForm.get(['totalPrice']).value,
      change: this.editForm.get(['change']).value,
      paid: this.editForm.get(['paid']).value,
      paymentType: this.editForm.get(['paymentType']).value,
      paymentCode: this.editForm.get(['paymentCode']).value,
      flagPaid: this.editForm.get(['flagPaid']).value,
      serviceType: this.editForm.get(['serviceType']).value,
      serviceTime:
        this.editForm.get(['serviceTime']).value != null ? moment(this.editForm.get(['serviceTime']).value, DATE_TIME_FORMAT) : undefined,
      createDate:
        this.editForm.get(['createDate']).value != null ? moment(this.editForm.get(['createDate']).value, DATE_TIME_FORMAT) : undefined,
      comment: this.editForm.get(['comment']).value,
      locationType: this.editForm.get(['locationType']).value,
      deliveryDuration: this.editForm.get(['deliveryDuration']).value,
      user: this.editForm.get(['user']).value,
      status: this.editForm.get(['status']).value,
      location: this.editForm.get(['location']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IOrder>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  trackOrderStatusHistoryById(index: number, item: IOrderStatusHistory) {
    return item.id;
  }

  trackLocationById(index: number, item: ILocation) {
    return item.id;
  }
}

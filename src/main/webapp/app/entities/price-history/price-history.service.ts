import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IPriceHistory } from 'app/shared/model/price-history.model';

type EntityResponseType = HttpResponse<IPriceHistory>;
type EntityArrayResponseType = HttpResponse<IPriceHistory[]>;

@Injectable({ providedIn: 'root' })
export class PriceHistoryService {
  public resourceUrl = SERVER_API_URL + 'api/price-histories';

  constructor(protected http: HttpClient) {}

  create(priceHistory: IPriceHistory): Observable<EntityResponseType> {
    return this.http.post<IPriceHistory>(this.resourceUrl, priceHistory, { observe: 'response' });
  }

  update(priceHistory: IPriceHistory): Observable<EntityResponseType> {
    return this.http.put<IPriceHistory>(this.resourceUrl, priceHistory, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IPriceHistory>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IPriceHistory[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { PriceHistoryComponent } from './price-history.component';
import { PriceHistoryDetailComponent } from './price-history-detail.component';
import { PriceHistoryUpdateComponent } from './price-history-update.component';
import { PriceHistoryDeletePopupComponent, PriceHistoryDeleteDialogComponent } from './price-history-delete-dialog.component';
import { priceHistoryRoute, priceHistoryPopupRoute } from './price-history.route';

const ENTITY_STATES = [...priceHistoryRoute, ...priceHistoryPopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    PriceHistoryComponent,
    PriceHistoryDetailComponent,
    PriceHistoryUpdateComponent,
    PriceHistoryDeleteDialogComponent,
    PriceHistoryDeletePopupComponent
  ],
  entryComponents: [PriceHistoryDeleteDialogComponent]
})
export class EnjexpPriceHistoryModule {}

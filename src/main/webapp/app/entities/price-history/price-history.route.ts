import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { PriceHistory } from 'app/shared/model/price-history.model';
import { PriceHistoryService } from './price-history.service';
import { PriceHistoryComponent } from './price-history.component';
import { PriceHistoryDetailComponent } from './price-history-detail.component';
import { PriceHistoryUpdateComponent } from './price-history-update.component';
import { PriceHistoryDeletePopupComponent } from './price-history-delete-dialog.component';
import { IPriceHistory } from 'app/shared/model/price-history.model';

@Injectable({ providedIn: 'root' })
export class PriceHistoryResolve implements Resolve<IPriceHistory> {
  constructor(private service: PriceHistoryService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IPriceHistory> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<PriceHistory>) => response.ok),
        map((priceHistory: HttpResponse<PriceHistory>) => priceHistory.body)
      );
    }
    return of(new PriceHistory());
  }
}

export const priceHistoryRoute: Routes = [
  {
    path: '',
    component: PriceHistoryComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'enjexpApp.priceHistory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: PriceHistoryDetailComponent,
    resolve: {
      priceHistory: PriceHistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.priceHistory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: PriceHistoryUpdateComponent,
    resolve: {
      priceHistory: PriceHistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.priceHistory.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: PriceHistoryUpdateComponent,
    resolve: {
      priceHistory: PriceHistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.priceHistory.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const priceHistoryPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: PriceHistoryDeletePopupComponent,
    resolve: {
      priceHistory: PriceHistoryResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.priceHistory.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

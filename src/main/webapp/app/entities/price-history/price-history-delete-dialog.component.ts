import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IPriceHistory } from 'app/shared/model/price-history.model';
import { PriceHistoryService } from './price-history.service';

@Component({
  selector: 'jhi-price-history-delete-dialog',
  templateUrl: './price-history-delete-dialog.component.html'
})
export class PriceHistoryDeleteDialogComponent {
  priceHistory: IPriceHistory;

  constructor(
    protected priceHistoryService: PriceHistoryService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.priceHistoryService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'priceHistoryListModification',
        content: 'Deleted an priceHistory'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-price-history-delete-popup',
  template: ''
})
export class PriceHistoryDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ priceHistory }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(PriceHistoryDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.priceHistory = priceHistory;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/price-history', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/price-history', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

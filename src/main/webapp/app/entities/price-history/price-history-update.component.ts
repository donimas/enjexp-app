import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IPriceHistory, PriceHistory } from 'app/shared/model/price-history.model';
import { PriceHistoryService } from './price-history.service';
import { IMenu } from 'app/shared/model/menu.model';
import { MenuService } from 'app/entities/menu/menu.service';

@Component({
  selector: 'jhi-price-history-update',
  templateUrl: './price-history-update.component.html'
})
export class PriceHistoryUpdateComponent implements OnInit {
  isSaving: boolean;

  menus: IMenu[];

  editForm = this.fb.group({
    id: [],
    price: [null, [Validators.required]],
    currency: [],
    menu: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected priceHistoryService: PriceHistoryService,
    protected menuService: MenuService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ priceHistory }) => {
      this.updateForm(priceHistory);
    });
    this.menuService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMenu[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMenu[]>) => response.body)
      )
      .subscribe((res: IMenu[]) => (this.menus = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(priceHistory: IPriceHistory) {
    this.editForm.patchValue({
      id: priceHistory.id,
      price: priceHistory.price,
      currency: priceHistory.currency,
      menu: priceHistory.menu
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const priceHistory = this.createFromForm();
    if (priceHistory.id !== undefined) {
      this.subscribeToSaveResponse(this.priceHistoryService.update(priceHistory));
    } else {
      this.subscribeToSaveResponse(this.priceHistoryService.create(priceHistory));
    }
  }

  private createFromForm(): IPriceHistory {
    return {
      ...new PriceHistory(),
      id: this.editForm.get(['id']).value,
      price: this.editForm.get(['price']).value,
      currency: this.editForm.get(['currency']).value,
      menu: this.editForm.get(['menu']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPriceHistory>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMenuById(index: number, item: IMenu) {
    return item.id;
  }
}

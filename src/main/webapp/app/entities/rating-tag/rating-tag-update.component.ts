import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { IRatingTag, RatingTag } from 'app/shared/model/rating-tag.model';
import { RatingTagService } from './rating-tag.service';

@Component({
  selector: 'jhi-rating-tag-update',
  templateUrl: './rating-tag-update.component.html'
})
export class RatingTagUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    name: [],
    eq: [],
    code: []
  });

  constructor(protected ratingTagService: RatingTagService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ ratingTag }) => {
      this.updateForm(ratingTag);
    });
  }

  updateForm(ratingTag: IRatingTag) {
    this.editForm.patchValue({
      id: ratingTag.id,
      name: ratingTag.name,
      eq: ratingTag.eq,
      code: ratingTag.code
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const ratingTag = this.createFromForm();
    if (ratingTag.id !== undefined) {
      this.subscribeToSaveResponse(this.ratingTagService.update(ratingTag));
    } else {
      this.subscribeToSaveResponse(this.ratingTagService.create(ratingTag));
    }
  }

  private createFromForm(): IRatingTag {
    return {
      ...new RatingTag(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      eq: this.editForm.get(['eq']).value,
      code: this.editForm.get(['code']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRatingTag>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}

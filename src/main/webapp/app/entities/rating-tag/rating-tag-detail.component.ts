import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRatingTag } from 'app/shared/model/rating-tag.model';

@Component({
  selector: 'jhi-rating-tag-detail',
  templateUrl: './rating-tag-detail.component.html'
})
export class RatingTagDetailComponent implements OnInit {
  ratingTag: IRatingTag;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ ratingTag }) => {
      this.ratingTag = ratingTag;
    });
  }

  previousState() {
    window.history.back();
  }
}

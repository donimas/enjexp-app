import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { EnjexpSharedModule } from 'app/shared/shared.module';
import { RatingTagComponent } from './rating-tag.component';
import { RatingTagDetailComponent } from './rating-tag-detail.component';
import { RatingTagUpdateComponent } from './rating-tag-update.component';
import { RatingTagDeletePopupComponent, RatingTagDeleteDialogComponent } from './rating-tag-delete-dialog.component';
import { ratingTagRoute, ratingTagPopupRoute } from './rating-tag.route';

const ENTITY_STATES = [...ratingTagRoute, ...ratingTagPopupRoute];

@NgModule({
  imports: [EnjexpSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RatingTagComponent,
    RatingTagDetailComponent,
    RatingTagUpdateComponent,
    RatingTagDeleteDialogComponent,
    RatingTagDeletePopupComponent
  ],
  entryComponents: [RatingTagDeleteDialogComponent]
})
export class EnjexpRatingTagModule {}

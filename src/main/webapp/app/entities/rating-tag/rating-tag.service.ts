import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IRatingTag } from 'app/shared/model/rating-tag.model';

type EntityResponseType = HttpResponse<IRatingTag>;
type EntityArrayResponseType = HttpResponse<IRatingTag[]>;

@Injectable({ providedIn: 'root' })
export class RatingTagService {
  public resourceUrl = SERVER_API_URL + 'api/rating-tags';

  constructor(protected http: HttpClient) {}

  create(ratingTag: IRatingTag): Observable<EntityResponseType> {
    return this.http.post<IRatingTag>(this.resourceUrl, ratingTag, { observe: 'response' });
  }

  update(ratingTag: IRatingTag): Observable<EntityResponseType> {
    return this.http.put<IRatingTag>(this.resourceUrl, ratingTag, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRatingTag>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRatingTag[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}

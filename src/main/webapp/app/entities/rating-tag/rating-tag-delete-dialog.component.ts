import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRatingTag } from 'app/shared/model/rating-tag.model';
import { RatingTagService } from './rating-tag.service';

@Component({
  selector: 'jhi-rating-tag-delete-dialog',
  templateUrl: './rating-tag-delete-dialog.component.html'
})
export class RatingTagDeleteDialogComponent {
  ratingTag: IRatingTag;

  constructor(protected ratingTagService: RatingTagService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.ratingTagService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'ratingTagListModification',
        content: 'Deleted an ratingTag'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-rating-tag-delete-popup',
  template: ''
})
export class RatingTagDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ ratingTag }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RatingTagDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.ratingTag = ratingTag;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/rating-tag', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/rating-tag', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}

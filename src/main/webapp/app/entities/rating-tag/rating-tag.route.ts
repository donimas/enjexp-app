import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { RatingTag } from 'app/shared/model/rating-tag.model';
import { RatingTagService } from './rating-tag.service';
import { RatingTagComponent } from './rating-tag.component';
import { RatingTagDetailComponent } from './rating-tag-detail.component';
import { RatingTagUpdateComponent } from './rating-tag-update.component';
import { RatingTagDeletePopupComponent } from './rating-tag-delete-dialog.component';
import { IRatingTag } from 'app/shared/model/rating-tag.model';

@Injectable({ providedIn: 'root' })
export class RatingTagResolve implements Resolve<IRatingTag> {
  constructor(private service: RatingTagService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRatingTag> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<RatingTag>) => response.ok),
        map((ratingTag: HttpResponse<RatingTag>) => ratingTag.body)
      );
    }
    return of(new RatingTag());
  }
}

export const ratingTagRoute: Routes = [
  {
    path: '',
    component: RatingTagComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'enjexpApp.ratingTag.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RatingTagDetailComponent,
    resolve: {
      ratingTag: RatingTagResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.ratingTag.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RatingTagUpdateComponent,
    resolve: {
      ratingTag: RatingTagResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.ratingTag.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RatingTagUpdateComponent,
    resolve: {
      ratingTag: RatingTagResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.ratingTag.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const ratingTagPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RatingTagDeletePopupComponent,
    resolve: {
      ratingTag: RatingTagResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'enjexpApp.ratingTag.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];

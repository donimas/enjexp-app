export interface IRatingTag {
  id?: number;
  name?: string;
  eq?: number;
  code?: string;
}

export class RatingTag implements IRatingTag {
  constructor(public id?: number, public name?: string, public eq?: number, public code?: string) {}
}

import { Moment } from 'moment';
import { INotice } from 'app/shared/model/notice.model';

export interface INoticeReceiver {
  id?: number;
  toUserId?: number;
  userLang?: string;
  flagSmsSent?: boolean;
  flagEmailSent?: boolean;
  flagPushSent?: boolean;
  flagRead?: boolean;
  readDate?: Moment;
  receiverFullName?: string;
  receiverPhone?: string;
  notice?: INotice;
}

export class NoticeReceiver implements INoticeReceiver {
  constructor(
    public id?: number,
    public toUserId?: number,
    public userLang?: string,
    public flagSmsSent?: boolean,
    public flagEmailSent?: boolean,
    public flagPushSent?: boolean,
    public flagRead?: boolean,
    public readDate?: Moment,
    public receiverFullName?: string,
    public receiverPhone?: string,
    public notice?: INotice
  ) {
    this.flagSmsSent = this.flagSmsSent || false;
    this.flagEmailSent = this.flagEmailSent || false;
    this.flagPushSent = this.flagPushSent || false;
    this.flagRead = this.flagRead || false;
  }
}

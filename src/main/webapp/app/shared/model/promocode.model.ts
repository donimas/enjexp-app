import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';

export interface IPromocode {
  id?: number;
  code?: string;
  flagActive?: boolean;
  name?: string;
  description?: string;
  containerClass?: string;
  containerId?: number;
  createDate?: Moment;
  user?: IUser;
}

export class Promocode implements IPromocode {
  constructor(
    public id?: number,
    public code?: string,
    public flagActive?: boolean,
    public name?: string,
    public description?: string,
    public containerClass?: string,
    public containerId?: number,
    public createDate?: Moment,
    public user?: IUser
  ) {
    this.flagActive = this.flagActive || false;
  }
}

import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IOrderStatusHistory } from 'app/shared/model/order-status-history.model';
import { ILocation } from 'app/shared/model/location.model';
import { PaymentType } from 'app/shared/model/enumerations/payment-type.model';
import { ServiceType } from 'app/shared/model/enumerations/service-type.model';
import { LocationType } from 'app/shared/model/enumerations/location-type.model';

export interface IOrder {
  id?: number;
  promocode?: string;
  totalPrice?: number;
  change?: number;
  paid?: number;
  paymentType?: PaymentType;
  paymentCode?: string;
  flagPaid?: boolean;
  serviceType?: ServiceType;
  serviceTime?: Moment;
  createDate?: Moment;
  comment?: string;
  locationType?: LocationType;
  deliveryDuration?: number;
  user?: IUser;
  status?: IOrderStatusHistory;
  location?: ILocation;
}

export class Order implements IOrder {
  constructor(
    public id?: number,
    public promocode?: string,
    public totalPrice?: number,
    public change?: number,
    public paid?: number,
    public paymentType?: PaymentType,
    public paymentCode?: string,
    public flagPaid?: boolean,
    public serviceType?: ServiceType,
    public serviceTime?: Moment,
    public createDate?: Moment,
    public comment?: string,
    public locationType?: LocationType,
    public deliveryDuration?: number,
    public user?: IUser,
    public status?: IOrderStatusHistory,
    public location?: ILocation
  ) {
    this.flagPaid = this.flagPaid || false;
  }
}

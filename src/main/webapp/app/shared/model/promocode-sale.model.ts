import { Moment } from 'moment';
import { IPromocode } from 'app/shared/model/promocode.model';

export interface IPromocodeSale {
  id?: number;
  sale?: number;
  minPrice?: number;
  maxPrice?: number;
  minAmount?: number;
  maxAmount?: number;
  minShare?: number;
  startDate?: Moment;
  endDate?: Moment;
  promocode?: IPromocode;
}

export class PromocodeSale implements IPromocodeSale {
  constructor(
    public id?: number,
    public sale?: number,
    public minPrice?: number,
    public maxPrice?: number,
    public minAmount?: number,
    public maxAmount?: number,
    public minShare?: number,
    public startDate?: Moment,
    public endDate?: Moment,
    public promocode?: IPromocode
  ) {}
}

export interface ILocation {
  id?: number;
  fullAddress?: string;
  floor?: string;
  room?: string;
  lat?: string;
  lon?: string;
}

export class Location implements ILocation {
  constructor(
    public id?: number,
    public fullAddress?: string,
    public floor?: string,
    public room?: string,
    public lat?: string,
    public lon?: string
  ) {}
}

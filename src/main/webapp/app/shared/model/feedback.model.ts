export interface IFeedback {
  id?: number;
  email?: string;
  message?: string;
}

export class Feedback implements IFeedback {
  constructor(public id?: number, public email?: string, public message?: string) {}
}

import { Moment } from 'moment';
import { IOrder } from 'app/shared/model/order.model';
import { IRatingTag } from 'app/shared/model/rating-tag.model';

export interface IOrderFeedback {
  id?: number;
  rating?: number;
  comment?: string;
  createDate?: Moment;
  order?: IOrder;
  ratingTags?: IRatingTag[];
}

export class OrderFeedback implements IOrderFeedback {
  constructor(
    public id?: number,
    public rating?: number,
    public comment?: string,
    public createDate?: Moment,
    public order?: IOrder,
    public ratingTags?: IRatingTag[]
  ) {}
}

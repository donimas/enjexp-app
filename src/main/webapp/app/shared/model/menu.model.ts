import { Moment } from 'moment';
import { ICategory } from 'app/shared/model/category.model';
import { IPriceHistory } from 'app/shared/model/price-history.model';
import { IMenu } from 'app/shared/model/menu.model';

export interface IMenu {
  id?: number;
  name?: string;
  description?: string;
  imageContentType?: string;
  image?: any;
  createDate?: Moment;
  flagDeleted?: boolean;
  onStop?: boolean;
  spicy?: boolean;
  content?: string;
  prepDuration?: number;
  category?: ICategory;
  price?: IPriceHistory;
  combo?: IMenu;
}

export class Menu implements IMenu {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public imageContentType?: string,
    public image?: any,
    public createDate?: Moment,
    public flagDeleted?: boolean,
    public onStop?: boolean,
    public spicy?: boolean,
    public content?: string,
    public prepDuration?: number,
    public category?: ICategory,
    public price?: IPriceHistory,
    public combo?: IMenu
  ) {
    this.flagDeleted = this.flagDeleted || false;
    this.onStop = this.onStop || false;
    this.spicy = this.spicy || false;
  }
}

import { Moment } from 'moment';
import { IOrder } from 'app/shared/model/order.model';
import { OrderStatus } from 'app/shared/model/enumerations/order-status.model';

export interface IOrderStatusHistory {
  id?: number;
  orderStatus?: OrderStatus;
  createDate?: Moment;
  comment?: string;
  order?: IOrder;
}

export class OrderStatusHistory implements IOrderStatusHistory {
  constructor(
    public id?: number,
    public orderStatus?: OrderStatus,
    public createDate?: Moment,
    public comment?: string,
    public order?: IOrder
  ) {}
}

import { IMenu } from 'app/shared/model/menu.model';
import { PriceCurrency } from 'app/shared/model/enumerations/price-currency.model';

export interface IPriceHistory {
  id?: number;
  price?: number;
  currency?: PriceCurrency;
  menu?: IMenu;
}

export class PriceHistory implements IPriceHistory {
  constructor(public id?: number, public price?: number, public currency?: PriceCurrency, public menu?: IMenu) {}
}

import { IOrder } from 'app/shared/model/order.model';
import { IMenu } from 'app/shared/model/menu.model';
import { OrderItemType } from 'app/shared/model/enumerations/order-item-type.model';

export interface IOrderItem {
  id?: number;
  amount?: number;
  type?: OrderItemType;
  order?: IOrder;
  menu?: IMenu;
}

export class OrderItem implements IOrderItem {
  constructor(public id?: number, public amount?: number, public type?: OrderItemType, public order?: IOrder, public menu?: IMenu) {}
}

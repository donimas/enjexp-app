export interface IAdvert {
  id?: number;
  name?: string;
  description?: string;
  imageContentType?: string;
  image?: any;
}

export class Advert implements IAdvert {
  constructor(
    public id?: number,
    public name?: string,
    public description?: string,
    public imageContentType?: string,
    public image?: any
  ) {}
}

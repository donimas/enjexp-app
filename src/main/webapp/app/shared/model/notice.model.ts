import { Moment } from 'moment';
import { NoticeReceiverType } from 'app/shared/model/enumerations/notice-receiver-type.model';
import { NoticeType } from 'app/shared/model/enumerations/notice-type.model';

export interface INotice {
  id?: number;
  title?: string;
  content?: string;
  smsContent?: string;
  flagPush?: boolean;
  flagSms?: boolean;
  flagEmail?: boolean;
  receiverType?: NoticeReceiverType;
  createDate?: Moment;
  modifyDate?: Moment;
  containerClass?: string;
  containerId?: number;
  noticeType?: NoticeType;
  flagSent?: boolean;
  sentTime?: Moment;
  payloadObjectUrl?: string;
  payloadObjectId?: number;
  flagDeleted?: boolean;
}

export class Notice implements INotice {
  constructor(
    public id?: number,
    public title?: string,
    public content?: string,
    public smsContent?: string,
    public flagPush?: boolean,
    public flagSms?: boolean,
    public flagEmail?: boolean,
    public receiverType?: NoticeReceiverType,
    public createDate?: Moment,
    public modifyDate?: Moment,
    public containerClass?: string,
    public containerId?: number,
    public noticeType?: NoticeType,
    public flagSent?: boolean,
    public sentTime?: Moment,
    public payloadObjectUrl?: string,
    public payloadObjectId?: number,
    public flagDeleted?: boolean
  ) {
    this.flagPush = this.flagPush || false;
    this.flagSms = this.flagSms || false;
    this.flagEmail = this.flagEmail || false;
    this.flagSent = this.flagSent || false;
    this.flagDeleted = this.flagDeleted || false;
  }
}

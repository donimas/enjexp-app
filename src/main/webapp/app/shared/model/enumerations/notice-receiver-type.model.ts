export const enum NoticeReceiverType {
  ONE_USER = 'ONE_USER',
  GROUP = 'GROUP',
  ALL_USERS = 'ALL_USERS',
  ORDER = 'ORDER',
  STOCK = 'STOCK'
}

export const enum PriceCurrency {
  KZT = 'KZT',
  RU = 'RU',
  US = 'US'
}

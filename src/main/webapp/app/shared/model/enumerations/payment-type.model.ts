export const enum PaymentType {
  CASH = 'CASH',
  EPAY = 'EPAY',
  KASPI = 'KASPI'
}

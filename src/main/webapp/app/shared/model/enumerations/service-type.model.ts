export const enum ServiceType {
  PICK_UP = 'PICK_UP',
  DELIVERY = 'DELIVERY'
}
